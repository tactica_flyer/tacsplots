package jp.d77.plugin.tacsplots;

import jp.d77.plugin.tacsplots.command.CommandManager;
import jp.d77.plugin.tacsplots.config.ConfigMain;
import jp.d77.plugin.tacsplots.config.ConfigObjects;
import jp.d77.plugin.tacsplots.core.DynmapManager;
import jp.d77.plugin.tacsplots.core.TownManager;
import jp.d77.plugin.tacsplots.events.GeneralListener;
import jp.d77.plugin.tacsplots.events.GeneralRunner;
import jp.d77.plugin.tacsplots.events.GeneralRunner.RUNNER_TYPE;
import jp.d77.plugin.tputils.db.DBSqlite;
import jp.d77.plugin.tputils.message.Message;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * TacsPlots main
 * @author Tactica
 *
 */
public class TacsPlots extends JavaPlugin{
	public static TacsPlots plugin;

	public Message				msg;
	public DBSqlite				db;
	public ConfigMain			cfg;
	public ConfigObjects		obj;
	public TownManager			town;
	public DynmapManager		map;

	private GeneralListener		lsGeneral;
	private CommandManager		cmd;

	private	GeneralRunner		saveEvent;

	private boolean				debug = false;

	@Override
	public void onEnable() {
		// メンバ初期化
		plugin = this;

		cfg			= new ConfigMain();
		obj			= new ConfigObjects();
		msg			= new Message( this, cfg );
		cmd			= new CommandManager();
		town		= new TownManager();
		map			= new DynmapManager();

		lsGeneral	= new GeneralListener();
		getServer().getPluginManager().registerEvents( lsGeneral , this );

		getCommand("tacsplots").setExecutor(cmd);
		getCommand("towns").setExecutor(cmd);
		getCommand("plots").setExecutor(cmd);
		getCommand("worlds").setExecutor(cmd);

		this.Enabler();
	}

	@Override
	public void onDisable() {
		this.Disabler();
	}

	private void Enabler(){
		this.cfg.init();
		this.obj.init();
		this.msg.init();
		this.cmd.init();
		this.town.init();

		// init cfg
		this.cfg.setMessageClass( this.msg );
		this.cfg.save();
		if ( this.cfg.isDefined("debug") ){
			this.setDebug( this.cfg.getBoolean("debug") );
		}

		// DataLoad
		this.load();

		this.map.init();

		// init listener
		this.lsGeneral.init();

		// run event
		int	save_sec = 10 * 60 * 20;
		if ( this.cfg.isDefined("system.save_interval_min") ){
			save_sec = this.cfg.getInteger( "system.save_interval_min" ) * 60 * 20;
		}
		this.saveEvent		= new GeneralRunner( RUNNER_TYPE.AUTO_SAVE );
		this.saveEvent.init();
		this.saveEvent.runTaskTimer( this, save_sec, save_sec );

		getLogger().info("Enable TacsPlots");
	}

	private void Disabler(){
		this.saveEvent.cancel();
		this.saveEvent.destroy();

		this.lsGeneral.destroy();

		this.save();
		this.map.destroy();
		this.town.destroy();
		this.cmd.destroy();
		this.msg.destroy();
		this.obj.destroy();
		this.cfg.destroy();

		getLogger().info("Disable TacsPlots");
	}

	public void reload(){
		this.Disabler();
		this.Enabler();
		getLogger().info("Reloaded TacsPlots");
	}

	public void save(){
		this.town.save();
		getLogger().info("Save TacsPlots");
	}

	public void load(){
		this.obj.load();
		this.town.load();
		getLogger().info("Load TacsPlots");
	}

	public void setDebug( boolean debug ){
		this.debug = debug;

		if ( this.msg != null ) this.msg.setDebug( this.debug );
		if ( this.cfg != null ) this.cfg.setDebug( this.debug );
		if ( this.obj != null ) this.obj.setDebug( this.debug );
		if ( this.town.plot_data != null ) this.town.plot_data.setDebug( this.debug );

		if ( this.debug == true ){
			this.getLogger().warning( "Debug mode: ON" );
		}else{
			this.getLogger().warning( "Debug mode: OFF" );
		}
	}

	public boolean isDebug(){
		return this.debug;
	}
}
