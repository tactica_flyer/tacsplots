package jp.d77.plugin.tacsplots.core;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.ChunkLoc;
import jp.d77.plugin.tacsplots.datas.CmdOpts;
import jp.d77.plugin.tacsplots.datas.Consts.FRIEND_TYPE;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tacsplots.datas.PlotData;
import jp.d77.plugin.tacsplots.datas.TownData;
import jp.d77.plugin.tputils.Utils;

public class TCmdPlots {
	protected static TacsPlots core = TacsPlots.plugin;
	private	boolean	enable = false;

	public TCmdPlots(){
	}

	public boolean init(){
		this.enable = true;
		return true;
	}

	public boolean destroy(){
		this.enable = false;
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Plot method
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 土地の取得
	 * @param cmd
	 */
	public void plot_claim( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.plots.claim");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.CLAIM.HELP" );
			return;
		}

		String owner_name;
		ChunkLoc	cl			= new ChunkLoc( cmd.getPlayer().getLocation() );
		TownData	td;
		int			pp;

		// 取得可能ワールドか確認
		if ( ! TUtils.isEnableWorld( cmd.getPlayer() , cl.getWorldName() ) ) return;

		// 所有済みの土地確認
		if ( TUtils.getPlotData( cl ) != null ){
			// 所有済みの土地
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.CLAIM.NONE_OWNER" );
			core.msg.debug( this.getClass().getSimpleName(), "plot_claim", "claimed" );
			return;
		}

		owner_name = TUtils.getOwnerName( cmd.getSender(), cmd.getPlayer().getName(), cmd.getOptPlayer(), cmd.getOptTown() );
		if ( owner_name == null ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.CLAIM.HELP" );
			return;
		}

		if ( owner_name.equalsIgnoreCase( cmd.getPlayerName() ) ){
			// 町を作成(無ければ)
			core.town.cmdTowns.create_town( cmd.getPlayer() );
		}

		td = TUtils.getTownData( owner_name );

		if ( td == null ) {
			// 町情報が取得できない
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.CLAIM.HELP" );
			return;
		}

		// 近くに別の土地が無い
		if ( ! this.checkClaimRange( owner_name, cl ) ){
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.CLAIM.NEAR" );
			return;
		}

		if ( this.checkClaimNear( owner_name, cl ) ){
			// 近くに自分の土地がある
			pp = core.cfg.getInteger("claim.pp.claim.near");
		}else{
			// 近くに自分の土地がない
			pp = core.cfg.getInteger("claim.pp.claim.faraway");
		}
		core.msg.debug( this.getClass().getSimpleName(), "plot_claim", "need pp=" + pp );

		if ( TUtils.getTownPlots( td.getOwner() ) >= td.getMaxPlots() ){
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.CLAIM.MAXPLOTS", "" + pp );
			return;
		}

		if ( td.getPlotPoints() < pp ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.ADMIN.PP.SHORTAGE", "" + pp );
			return;
		}
		td.addPlotPoints( pp * (-1) );

		// plotを取得
		core.town.plots.put( cl.toKey() , new PlotData( cl, owner_name ) );
		td.update(true);

		TUtils.sendOwnersMsg(
				td.getOwner()
				, cmd.getSender()
				, "MESSAGE.PLOTS.CLAIM.DONE", "(" + cl.toString() + ")" );

		core.msg.debug( this.getClass().getSimpleName(), "plot_claim", "complete" );
		return;
	}

	public boolean checkClaimNear( String owner_name, ChunkLoc cl ){
		PlotData	pd;
		ChunkLoc	wcl;

		for ( int z = -1; z <= 1; z++ ){
			for ( int x = -1; x <= 1; x++ ){
				if ( x == 0 && z == 0 ) continue;
				wcl = new ChunkLoc( cl );
				wcl.addX(x);
				wcl.addZ(z);

				pd = TUtils.getPlotData(wcl);
				if ( pd == null ) continue;		// 空き地

				//pd.isFriend(player_name)
				if ( pd.getOwner().equalsIgnoreCase( owner_name ) ) {
					return true;
				}

			}
		}

		return false;

	}

	public boolean checkClaimRange( String claimer_name, ChunkLoc cl ){
		int	rengeNormal = core.cfg.getInteger( "claim.min_range.normal" );
		int	rengeFriend = core.cfg.getInteger( "claim.min_range.friend" );
		ChunkLoc	wcl;
		TownData	td;
		PlotData	pd;

		for ( int z = rengeNormal * (-1); z < rengeNormal; z++ ){
			for ( int x = rengeNormal * (-1); x <= rengeNormal; x++ ){
				wcl = new ChunkLoc( cl );
				wcl.addX(x);
				wcl.addZ(z);

				pd = TUtils.getPlotData(wcl);
				if ( pd == null ) continue;		// 空き地

				//pd.isFriend(player_name)
				if ( pd.getOwner().equalsIgnoreCase( claimer_name ) ) continue;	// 自分の土地

				// 以下、すでに誰かの土地の処置
				if ( Math.abs(x) <= rengeFriend && Math.abs(z) <= rengeFriend ){
					// friend range以下なので、この土地の取得は認めない
					core.msg.debug( this.getClass().getSimpleName(), "checkClaimRange", "adjacent " + Math.abs(x) + " <= " + rengeFriend + " && " + Math.abs(z)+ " <= " + rengeFriend );
					return false;
				}

				td = TUtils.getTownData( pd.getOwner() );
				if ( td == null ) continue;
				if ( ! td.isFriend( claimer_name ) ){
					// フレンドになってない
					if ( Math.abs(x) <= rengeNormal && Math.abs(z) <= rengeNormal ){
						// normal range以下なので、この土地の取得は認めない
						core.msg.debug( this.getClass().getSimpleName(), "checkClaimRange", "notfriend other_owner=" + td.getOwner() + " claimer=" + claimer_name + " " + Math.abs(x) + " <= " + rengeNormal + " && " + Math.abs(z)+ " <= " + rengeNormal );
						return false;
					}

				}

			}
		}

		return true;
	}

	public void plot_unclaim( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.plots.unclaim");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.UNCLAIM.HELP" );
			return;
		}

		//core.town.plot_unclaim( cmd.getPlayer() );

		ChunkLoc	cl = new ChunkLoc( cmd.getPlayer().getLocation() );

		if ( ! TUtils.isEnableWorld( cmd.getPlayer() , cl.getWorldName() ) ) return;

		// 空き地では無い?
		if ( TUtils.getClaimedPlot( cmd.getPlayer(), cl ) == null ) return;

		// オーナ権限が無い?

		if ( ! TUtils.isPlotOwner( cl, cmd.getPlayer().getName() ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.NOT_PERM" );
			return;
		}

		TUtils.TownUpdate( core.town.plots.get( cl.toKey() ).getOwner() );
		TUtils.sendOwnersMsg(
				core.town.plots.get( cl.toKey() ).getOwner()
				, cmd.getSender()
				, "MESSAGE.PLOTS.UNCLAIM.DONE" );
		//core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.UNCLAIM.DONE" );

		core.town.plots.remove( cl.toKey() );

		core.msg.debug( this.getClass().getSimpleName(), "plot_unclaim", "complete" );
		return;
	}

	public void plot_reset( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.plots.reset");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.RESET.HELP" );
			return;
		}

		ChunkLoc	cl = new ChunkLoc( cmd.getPlayer().getLocation() );

		if ( ! TUtils.isEnableWorld( cmd.getPlayer() , cl.getWorldName() ) ) return;

		// 空き地では無い?
		if ( TUtils.getClaimedPlot( cmd.getPlayer(), cl ) == null ) return;

		// オーナ権限が無い?

		if ( ! TUtils.isPlotOwner( cl, cmd.getPlayer().getName() ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.NOT_PERM" );
			return;
		}

		core.town.plots.get( cl.toKey() ).reset();

		TUtils.sendOwnersMsg(
				core.town.plots.get( cl.toKey() ).getOwner()
				, cmd.getPlayer(), "MESSAGE.PLOTS.RESET.DONE" );
		core.msg.debug( this.getClass().getSimpleName(), "plot_reset", "complete" );
		return;
	}

	/**
	 * Plot Infomation
	 * @param cmd
	 */
	public void plot_info( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.plots.info");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.INFO.HELP" );
			return;
		}

		//core.town.plot_info( cmd.getPlayer() );

		ChunkLoc	cl = new ChunkLoc( cmd.getPlayer().getLocation() );

		if ( ! TUtils.isEnableWorld( cmd.getPlayer(), cl.getWorldName() ) ) return;

		PlotData	pl = TUtils.getClaimedPlot( cmd.getPlayer(), cl);
		if ( pl == null ) return;

		core.msg.addMessage( cmd.getPlayer(), pl.info() );
		core.msg.debug( this.getClass().getSimpleName(), "plot_info", "complete" );
		return;
	}


	/**
	 * Plotの権限変更
	 * @param cmd
	 */
	public void plot_setperm( CmdOpts cmd ){
		if ( ! this.enable ) return;

		// /towns set perm
		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.plots.set.perm");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.SET.PERM.HELP" );
			return;
		}

		//core.town.town_setperm( cmd.getPlayer(), cmd.getOptPlayer(), cmd.getOptTown(), cmd.getOptCommands(2), cmd.getOptCommands(3) );

		ChunkLoc cl			= new ChunkLoc( cmd.getPlayer() );
		String owner_name;
		String perm			= cmd.getOptCommands(1);
		String setdata		= cmd.getOptCommands(2);
		TownData td;
		PlotData pd;

		PERM_TYPE	val_perm;
		Boolean		val_setdata;

		pd = TUtils.getClaimedPlot( cmd.getSender(), cl );
		if ( pd == null ) return;


		if ( ! TUtils.isPlotOwner( cl, cmd.getPlayerName() ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.NOT_PERM" );
			return;
		}

		owner_name = TUtils.getOwnerName( cmd.getPlayer() , cmd.getPlayer().getName(), pd.getOwner(), null );
		if ( owner_name == null ) return;
		td = TUtils.getTownData( owner_name );
		if ( td == null ) return;

		val_perm	= TUtils.str2PermType(perm);
		val_setdata	= TUtils.str2boolean(setdata);

//		core.msg.debug( this.getClass().getSimpleName(), "plot_info", "complete" );

		if ( val_perm == null || val_setdata == null ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.PLOTS.SET.PERM.HELP" );
			return;
		}

		pd.setPerm( val_perm, val_setdata );

		TUtils.sendOwnersMsg(
				td.getOwner()
				, cmd.getSender()
				, "MESSAGE.PLOTS.SET.PERM.DONE"
				, "(" + cl.getX() + "," + cl.getZ() + ")"
				, TUtils.PermType2Str( val_perm )
				, TUtils.boolean2str( val_setdata )
				);
		td.update(true);
		core.msg.debug( this.getClass().getSimpleName(), "plot_setperm", "complete" );

		return;
	}

	public void plot_friend( CmdOpts cmd ){
		if ( ! this.enable ) return;

		String	friend_name;
		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.plots.add.friend");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.ADDREMOVE.FRIEND.HELP" );
			return;
		}

		if ( cmd.getOptCommands(2) == null ){
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.ADDREMOVE.FRIEND.HELP" );
			return;
		}
		friend_name = cmd.getOptCommands(2);

		ChunkLoc	cl = new ChunkLoc( cmd.getPlayer().getLocation() );
		PlotData	pd = TUtils.getClaimedPlot( cmd.getSender(), cl );
		if ( pd == null ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.NOT_PERM" );
			return;
		}

		if ( ! TUtils.isPlotOwner( cl, cmd.getPlayerName() ) ) {
			// オーナでは無い

			if ( TUtils.isPerm( null, cl, PERM_TYPE.RENTAL ) ){
				core.msg.debug( this.getClass().getSimpleName(), "plot_friend", "rental check friend_name=" + friend_name );

				// レンタル区画
				if ( ! cmd.getPlayer().getName().equalsIgnoreCase( friend_name ) ){
					// 他人の名前を指定している
					core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.ADDREMOVE.FRIEND.RENTAL.TARGETERR" );
					return;
				}

				if ( cmd.getOptCommands(0).equalsIgnoreCase( "add" ) ){
					if ( pd.getFriends( FRIEND_TYPE.FRIEND ).size() > 0 ){
						// 既に貸し出し中
						core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.ADDREMOVE.FRIEND.RENTAL.RENTERR" );
						return;
					}
					if ( pd.isFriend( friend_name ) ){
						// 既に貸し出し中
						core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.ADD.FRIEND.RENTERR" );
						return;
					}
				}else if ( cmd.getOptCommands(0).equalsIgnoreCase( "remove" ) ){
				}

				//pd.isFriend(  )
			}else{
				core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.NOT_PERM" );
				return;
			}
		}else{
			core.msg.debug( this.getClass().getSimpleName(), "plot_friend", cmd.getPlayerName() + " is owner." );

		}

		if ( cmd.getOptCommands(0).equalsIgnoreCase( "add" ) ){
			pd.addFriend( friend_name, FRIEND_TYPE.FRIEND );

			TUtils.sendOwnersMsg(
					pd.getOwner()
					, cmd.getPlayer(), "MESSAGE.PLOTS.ADD.FRIEND.DONE", friend_name );
			TUtils.TownUpdate( pd.getOwner() );
		}else{
			if ( pd.removeFriend( friend_name ) ){
				TUtils.sendOwnersMsg(
						pd.getOwner()
						, cmd.getPlayer(), "MESSAGE.PLOTS.REMOVE.FRIEND.DONE", friend_name );
				TUtils.TownUpdate( pd.getOwner() );
			}else{
				core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.REMOVE.ERROR", friend_name );
			}
		}

		core.town.checkInvincibleMode( friend_name );
		return;
	}

	/**
	 * プロット名を設定
	 * @param cmd
	 * @return
	 */
	public void plot_title( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.plots.set.name");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.SET.NAME.HELP" );
			return;
		}

		String title;
		title = cmd.getOptCommands(2);

		ChunkLoc	cl = new ChunkLoc( cmd.getPlayer().getLocation() );
		if ( ! TUtils.isPlotOwner( cl, cmd.getPlayerName() ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.NOT_PERM" );
			return;
		}

		PlotData	pd = TUtils.getClaimedPlot( cmd.getSender(), cl );
		if ( pd == null ) return;

		pd.setTitle( title );
		TUtils.TownUpdate( pd.getOwner() );

		if ( pd.getTitle() == null ){
			TUtils.sendOwnersMsg(
					pd.getOwner()
					, cmd.getPlayer(), "MESSAGE.PLOTS.SET.NAME.DONE", pd.getChunk().toString(), core.msg.template( "MESSAGE.MAIN.NULL" ) );
		}else{
			TUtils.sendOwnersMsg(
					pd.getOwner()
					, cmd.getPlayer(), "MESSAGE.PLOTS.SET.NAME.DONE", pd.getChunk().toString(), pd.getTitle() );
		}
		core.msg.debug( this.getClass().getSimpleName(), "plot_title", "complete" );
		return;
	}

	/**
	 * PlotExp追加
	 * @param cmd
	 * @return
	 */
	public void plot_addpe( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.admin.plots.add.pe");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.ADD.PLOTEXP.HELP" );
			return;
		}

		Integer	pe;
		pe = Utils.StringToInt( cmd.getOptCommands(2) );
		if ( pe == null ){
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.ADD.PLOTEXP.HELP" );
			return;
		}

		ChunkLoc	cl = new ChunkLoc( cmd.getPlayer().getLocation() );

		PlotData	pd = TUtils.getClaimedPlot( cmd.getSender(), cl );
		if ( pd == null ) return;

		pd.addPlotExp( pe );
		TUtils.TownUpdate( pd.getOwner() );
			TUtils.sendOwnersMsg(
					pd.getOwner()
					, cmd.getPlayer(), "MESSAGE.PLOTS.ADD.PLOTEXP.DONE", pd.getOwner(), pd.getChunk().toString(), pe + "" );
		core.msg.debug( this.getClass().getSimpleName(), "plot_addpe", "complete" );
		return;
	}

}
