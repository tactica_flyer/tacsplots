package jp.d77.plugin.tacsplots.core;

import java.util.HashMap;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.events.GeneralRunner;
import jp.d77.plugin.tacsplots.events.GeneralRunner.RUNNER_TYPE;
import jp.d77.plugin.tputils.Utils;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class TTp {
	protected static TacsPlots core = TacsPlots.plugin;

	private boolean	enable		= false;
	private int		cooldown	= 5;
	private int		warmup		= 10;

	private HashMap<Player,Long>		spawn_stack;		// 開始時間スタック
	private HashMap<Player,Long>		spawn_stack_up;		// ウォームアップスタック
	private HashMap<Player,Location>	spawn_stack_loc;	// 現在地スタック
	private HashMap<Player,Location>	spawn_stack_locto;	// 行き先スタック

	private GeneralRunner	grunner;

	public TTp(){
	}

	public void init(){

		this.spawn_stack		= new HashMap<Player,Long>();
		this.spawn_stack_up		= new HashMap<Player,Long>();
		this.spawn_stack_loc	= new HashMap<Player,Location>();
		this.spawn_stack_locto	= new HashMap<Player,Location>();

		this.cooldown	= core.getConfig().getInt( "spawn.cooldown", 5 );
		this.warmup		= core.getConfig().getInt( "spawn.warmup", 10 );

		//this.grunner = new GeneralRunner( RUNNER_TYPE.SPAWN ).runTaskTimer( core, 20, 20 );
		this.grunner = new GeneralRunner( RUNNER_TYPE.SPAWN );
		this.grunner.runTaskTimer( core, 20, 20 );
		this.grunner.init();

		this.enable = true;
	}

	public void destroy(){
		this.enable = false;

		if ( grunner != null ){
			grunner.cancel();
			grunner.destroy();
			grunner = null;
		}
	}

	/**
	 * コマンド入力処理
	 * @param cmddata
	 */
	public void onCommand( Player p, Location loc ){
		if ( ! this.enable ) return;

		if ( p.getGameMode().equals( GameMode.CREATIVE ) ){
			// Creative -> Immediately run
			this.doSpawn( p, loc );
			return;
		}

		if ( this.checkWarmup( p ) ){
			// ウォームアップ待ち
			core.msg.debug( this.getClass().getSimpleName(), "onCommand", "warmup waiting" );
			return;
		}

		// spawn待ち開始(クールダウン開始)
		core.msg.addMessage( p, "MESSAGE.TP.COOLDOWN", "" + this.cooldown );
		this.addStack( p, loc );
	}

	/**
	 * クールダウン完了チェック
	 */
	public void checkSpawn(){
		if ( ! this.enable ) return;

		for( Player p: this.spawn_stack.keySet() ){
			if ( this.spawn_stack.get(p) + ( this.cooldown * 1000 ) < Utils.nowtime() ){
				// do spawn
				this.doSpawn( p, this.spawn_stack_locto.get(p) );
				this.setWarmup( p );
			}else{
				if ( core.isDebug() ){
					core.msg.debug( this.getClass().getSimpleName(), "checkSpawn", "p=" + p.getName()
							+ " casttime=" + Utils.formatTime( this.spawn_stack.get(p) )
							+ " schedule=" + Utils.formatTime( this.spawn_stack.get(p) + ( this.cooldown * 1000 ) )
							+ " now=" + Utils.nowtime()
							);
				}

			}
		}
	}

	/**
	 * ウォームアップ待ちチェック
	 * @param p
	 * @return true...ウォームアップ中
	 */
	public boolean checkWarmup( Player p ){
		if ( ! this.enable ) return false;

		// スタック有無確認
		if ( ! spawn_stack_up.containsKey( p ) ) return false;

		long time = spawn_stack_up.get( p ) + ( this.warmup * 1000 ) - Utils.nowtime();

		if ( time > 0 ){
			// まだ待ちメッセージ
			core.msg.addMessage( p, "MESSAGE.TP.WARMUP", "" + (time / 1000) );
			return true;
		}

		// ウォームアップ完了
		spawn_stack_up.remove( p );
		return false;
	}

	/**
	 * ウォームアップ設定
	 * @param p
	 */
	private void setWarmup( Player p ){
		this.spawn_stack_up.put( p, Utils.nowtime() );
		core.msg.debug( this.getClass().getSimpleName(), "setWarmup", "p=" + p.getName() + " nowtime " + Utils.formatTime( this.spawn_stack_up.get(p) ));
	}

	/**
	 * Spawn実行
	 * @param p
	 */
	private void doSpawn( Player p, Location loc ){
		this.removeStack( p );
		if ( ! p.isOnline() ) return;

		if ( loc == null ) return;

		core.msg.debug( this.getClass().getSimpleName(), "doSpawn", "p=" + p.getName() + " to " + Utils.formatLocation( loc ));
		Utils.safeTeleport( p, loc );
		//p.teleport( loc );
	}

	/**
	 * クールダウンスタック追加
	 * @param p
	 * @param moveto 行き先
	 */
	private void addStack( Player p, Location moveto ){
		this.spawn_stack.put( p, Utils.nowtime() );		// 開始時間
		this.spawn_stack_loc.put( p, p.getLocation() );	// 現在地
		this.spawn_stack_locto.put( p, moveto );		// 行き先
		core.msg.debug( this.getClass().getSimpleName(), "addStack", "start cooldown p=" + p.getName()
				+ " nowtime=" + Utils.formatTime( this.spawn_stack.get(p) )
				+ " nowloc=" + Utils.formatLocation( this.spawn_stack_loc.get(p) )
				+ " moveto=" + Utils.formatLocation( this.spawn_stack_locto.get(p) )
			);
	}

	/**
	 * クールダウンスタックから削除
	 * @param p
	 */
	private void removeStack( Player p ){
		if ( this.spawn_stack.containsKey( p ) ) this.spawn_stack.remove( p );
		if ( this.spawn_stack_loc.containsKey( p ) ) this.spawn_stack_loc.remove( p );
		if ( this.spawn_stack_locto.containsKey( p ) ) this.spawn_stack_locto.remove( p );
		core.msg.debug( this.getClass().getSimpleName(), "removeStack", "p=" + p.getName());
	}

	/**
	 * クールダウンスタック中?
	 * @param p
	 * @return
	 */
	private boolean isStack( Player p ){
		if ( spawn_stack.containsKey( p ) ) return true;
		return false;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// イベント系処理(主に動いたので中断)
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 動いた時の処理
	 * @param p
	 */
	public void willMove( Player p ){
		if ( ! this.enable ) return;

		if ( ! this.isStack( p ) ) return;

		// 中断処理
		this.removeStack( p );
		if ( ! p.isOnline() ) return;

		core.msg.debug( this.getClass().getSimpleName(), "willMove", "p=" + p.getName());
		core.msg.addMessage( p, "MESSAGE.TP.CANCEL" );
	}

	// cancel
	public void OtherMove( Player p ){
		if ( ! this.enable ) return;
		this.willMove( p );
	}

	/**
	 * PlayerMoveEvent
	 * @param event
	 */
	public void PlayerMove( PlayerMoveEvent event ){
		if ( ! this.enable ) return;
		if ( ! this.isStack( event.getPlayer() ) ) return;

		Location sl = this.spawn_stack_loc.get( event.getPlayer() );
		Location nl = event.getPlayer().getLocation();
		if ( sl.getBlockX() != nl.getBlockX()
				|| sl.getBlockY() != nl.getBlockY()
				|| sl.getBlockZ() != nl.getBlockZ()
				){
			this.willMove( event.getPlayer() );
		}
	}

	/**
	 * EntityDamageEvent
	 * @param event
	 */
	public void EntityDamage( EntityDamageEvent event ){
		if ( ! this.enable ) return;
		if ( ! event.getEntityType().equals( EntityType.PLAYER ) ) return;

		Player p = (Player) event.getEntity();
		this.willMove( p );
	}
}
