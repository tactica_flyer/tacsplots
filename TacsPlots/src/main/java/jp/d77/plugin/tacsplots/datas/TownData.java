package jp.d77.plugin.tacsplots.datas;

import java.util.ArrayList;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.core.TUtils;
import jp.d77.plugin.tacsplots.datas.Consts.FRIEND_TYPE;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tputils.Utils;
import jp.d77.plugin.tputils.kvs.KeyValue;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class TownData {
	protected static TacsPlots core = TacsPlots.plugin;

	protected String		title	= null;
	protected PlayerData	owner;
	protected long			dateCreate	= 0;
	protected long			dateUpdate	= 0;
	protected int			plotpoint	= 0;
	protected int			pp_bonus	= 0;
	protected Location		home;
	protected ChunkLoc		home_chunk;
	protected FriendData	friends = new FriendData();
	protected PermData	perm	= new PermData();

	public TownData( String owner_name ){
		this.owner = new PlayerData( owner_name );
		this.dateCreate = Utils.nowtime();
		this.dateUpdate = Utils.nowtime();
		this.plotpoint  = core.cfg.getInteger("claim.pp.startexp");
		this.pp_bonus	= core.cfg.getInteger("claim.pp.startbonus");
	}

	public TownData( KeyValue kvs, String root ){
		this.load(kvs, root);
	}

	protected TownData(){

	}

	public void reset(){
		this.setTitle(null);
		this.perm		= new PermData();
		this.friends	= new FriendData();
		this.home		= null;
		this.home_chunk	= null;
	}

	public void save( KeyValue kvs, String root ){
		core.msg.debug( this.getClass().getSimpleName(), "save", "root=" + root );

		if ( this.title != null ) kvs.setString( root + ".title", this.title);
		kvs.setLong( root + ".create", this.dateCreate);
		kvs.setLong( root + ".update", this.dateUpdate);
		kvs.setInteger( root + ".plotpoint", this.plotpoint);
		kvs.setInteger( root + ".pp_bonus", this.pp_bonus);

		if ( this.home != null ) {
			kvs.setString( root + ".home.world", this.home.getWorld().getName() );
			kvs.setInteger( root + ".home.x", this.home.getBlockX() );
			kvs.setInteger( root + ".home.y", this.home.getBlockY() );
			kvs.setInteger( root + ".home.z", this.home.getBlockZ() );
		}

		this.perm.save( kvs, root + ".perm" );
		this.owner.save( kvs, root + ".owner" );
		this.friends.save(kvs, root);
	}

	public void load( KeyValue kvs, String root ){

		core.msg.debug( this.getClass().getSimpleName(), "load", "root=" + root );

		if ( kvs.getString( root + ".title" ) != null ) this.setTitle( kvs.getString( root + ".title" ), false );
		if ( kvs.getLong( root + ".create" ) != null ) this.dateCreate = kvs.getLong( root + ".create" );
		if ( kvs.getLong( root + ".update" ) != null ) this.dateUpdate = kvs.getLong( root + ".update" );
		if ( kvs.getInteger( root + ".plotpoint" ) != null ) this.plotpoint = kvs.getInteger( root + ".plotpoint" );
		if ( kvs.getInteger( root + ".pp_bonus" ) != null ) this.pp_bonus = kvs.getInteger( root + ".pp_bonus" );

		if ( kvs.getString( root + ".home.world" ) != null
				&& kvs.getInteger( root + ".home.x" ) != null
				&& kvs.getInteger( root + ".home.y" ) != null
				&& kvs.getInteger( root + ".home.z" ) != null
				){
			if ( TUtils.validWorld(null, kvs.getString( root + ".home.world" )) ){
				this.setHome(
					new Location(
						Bukkit.getWorld( kvs.getString( root + ".home.world" ) )
						,kvs.getInteger( root + ".home.x" )
						,kvs.getInteger( root + ".home.y" )
						,kvs.getInteger( root + ".home.z" )
					)
					, false
				);
			}
		}

		this.perm.load( kvs, root + ".perm" );
		this.owner = new PlayerData( kvs, root + ".owner" );
		this.friends.load(kvs, root);
	}

	public String info(){
		PermData pd = TUtils.getTownPerm( this.getOwner() );

		String town_name,home_pos;

		if ( this.getTitle() == null ){
			town_name = core.cfg.getString( "MESSAGE.MAIN.NULL" );
		}else{
			town_name = this.getTitle();
		}
		if ( this.gethome() == null ){
			home_pos = core.cfg.getString( "MESSAGE.MAIN.NULL" );
		}else{
			home_pos = Utils.formatLocation( this.gethome() );
		}

		return core.msg.template( "MESSAGE.TOWNS.INFO.VIEW"
				, this.getOwner()
				, town_name
				, home_pos
				, "(now/max)=(" + TUtils.getTownPlots( this.getOwner() ) + "pp / " + this.getMaxPlots() + "pp)"
				, "" + this.plotpoint
				, this.friends.info()
				) + "\n"
				+ pd.info();
	}

	public String simple_info(){
		PermData pd = TUtils.getTownPerm( this.getOwner() );

		String town_name;

		if ( this.getTitle() == null ){
			town_name = core.cfg.getString( "MESSAGE.MAIN.NULL" );
		}else{
			town_name = this.getTitle();
		}

		return core.msg.template( "MESSAGE.TOWNS.INFO.SIMPLEVIEW"
				, this.getOwner()
				, town_name
				, "(" + TUtils.getTownPlots( this.getOwner() ) + "pp / " + this.getMaxPlots() + "pp)"
				, "" + this.plotpoint
				) + " " + pd.info();
	}

	public String dynmap_info(){
		String ret,work;
		int cnt;

		ret = core.cfg.getString( "dynmap.baloon" );
		work = "";
		cnt = 0;

		if ( this.getTitle() == null ){
			ret = ret.replace( "%name%", core.cfg.getString( "dynmap.lang.NULL" ) );
		}else{
			ret = ret.replace( "%name%", this.getTitle() );
		}
		ret = ret.replace( "%owner%", this.getOwner() );

		cnt = 0;
		work = "";
		for ( String s: this.getFriends( FRIEND_TYPE.OWNER ) ){
			cnt++;
			if ( cnt <= 10 ) work += s + "(SO) ";
			if ( cnt == 11 ) work += "...";
		}
		for ( String s: this.getFriends( FRIEND_TYPE.FRIEND ) ){
			cnt++;
			if ( cnt <= 10 ) work += s + " ";
			if ( cnt == 11 ) work += "...";
		}
		work += "(Total:" + cnt + ")";

		ret = ret.replace( "%friends%", work );
		ret = ret.replace( "%plots%", "" + TUtils.getTownPlots( this.getOwner() ) );

		PermData pd = TUtils.getTownPerm( this.getOwner() );
		work = core.msg.template( "dynmap.baloon_perm"
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.OPEN ) )
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.BUILD ) )
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.SWITCH ) )
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.FIRE ) )
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.PVP ) )
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.ANIMAL ) )
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.MOB ) )
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.EXPLOSION ) )
				, this.dynmap_boolean2str( pd.get( PERM_TYPE.STORAGE ) )
				);

		ret = ret.replace( "%perm%", work );

		return ret;
	}

	private String dynmap_boolean2str( Boolean bool ){
		if ( bool == null ){
			return core.msg.template("dynmap.lang.NULL");
		}
		if ( bool ){
			return core.msg.template("dynmap.lang.ON");
		}else{
			return core.msg.template("dynmap.lang.OFF");
		}
	}


	// ---------------------------------------------------------------------------------------------------------------
	// Gets
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * オーナ名を取得する
	 * @return
	 */
	public String getOwner(){
		return this.owner.getName();
	}

	/**
	 * フレンドのプレイヤー情報を取得する
	 * @param player_name
	 * @return
	 */
	public PlayerData getFriendData( String player_name ){
		return this.friends.getPlayerData(player_name);
	}

	/**
	 * プレイヤーのタイプを取得する
	 * @param player_name
	 * @return
	 */
	public FRIEND_TYPE getFriendType( String player_name ){
		return this.friends.getPlayerType(player_name);
	}

	public PermData getPerm(){
		return this.perm;
	}

	/**
	 * 名称を取得する
	 * @return
	 */
	public String getTitle(){
		return this.title;
	}

	public Location gethome(){
		return this.home;
	}

	public ChunkLoc gethomeChunk(){
		return new ChunkLoc( this.home );
	}

	public int getMaxPlots(){
		return TUtils.calcMaxPlotPoint( this.getOwner() ) + this.pp_bonus;
	}

	public int getPlotPoints(){
		return this.plotpoint;
	}

	public long getUpdate(){
		return this.dateUpdate;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Sets
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * 名称を設定する
	 * @param title
	 */
	public void setTitle( String title ){
		this.setTitle(title,true);
	}

	private void setTitle( String title, boolean update ){
		this.title = title;
		core.msg.debug( this.getClass().getSimpleName(), "setTitle", "title=" + title );
		this.update( update );
	}

	/**
	 * Home設定
	 * @param cl
	 */
	public void setHome( Location cl){
		this.setHome(cl, true);
	}

	private void setHome( Location loc, boolean update ){
		this.home = loc;
		this.home_chunk = new ChunkLoc( loc );
		core.msg.debug( this.getClass().getSimpleName(), "setHome", "setHome=" + Utils.formatLocation( loc ) );
		this.update( update );
	}

	public void setPerm( PERM_TYPE pt, boolean b ){
		this.setPerm(pt, b, true);
	}

	private void setPerm( PERM_TYPE pt, boolean b, boolean update ){
		this.perm.set(pt, b);
		this.update( update );
	}

	public void update( boolean update ){
		if ( update ) {
			core.msg.debug( this.getClass().getSimpleName(), "update", this.dateUpdate + "->" + Utils.nowtime() );

			this.dateUpdate = Utils.nowtime();
		}
	}

	public boolean addFriend( String player_name, FRIEND_TYPE pt ){
		this.update( true );
		this.friends.add(player_name, pt);
		return true;
	}

	public boolean removeFriend( String player_name ){
		this.update( true );
		return this.friends.remove(player_name);
	}

	public void setPlotPoints( int pp ){
		this.plotpoint = pp;
	}

	public void addPlotPoints( int pp ){
		if ( this.plotpoint + pp < 0 ) {
			this.plotpoint = 0;
		}else{
			this.plotpoint += pp;
		}
	}

	public ArrayList<String> getFriends( FRIEND_TYPE pt ){
		return this.friends.getFriends(pt);
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Checks
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * オーナか？
	 * @param player_name
	 * @return
	 */
	public boolean isOwner( String player_name ){
		if ( this.owner.getName().equalsIgnoreCase( player_name ) ) return true;
		return false;
	}

	/**
	 * オーナ、あるいはサブオーナか？
	 * @param player_name
	 * @return
	 */
	public boolean isSubOwner( String player_name ){
		if ( this.isOwner( player_name ) ) return true;
		if ( this.getFriendType( player_name ).equals( FRIEND_TYPE.OWNER ) ) return true;
		return false;
	}

	/**
	 * オーナ、あるいはフレンド以上か？
	 * @param player_name
	 * @return
	 */
	public boolean isFriend( String player_name ){
		if ( this.isSubOwner( player_name ) ) return true;
		if ( this.getFriendType( player_name ).equals( FRIEND_TYPE.FRIEND ) ) return true;
		return false;
	}

	public boolean isHome( ChunkLoc cl ){
		if ( this.home == null ) return false;
		if ( cl.equal( this.home_chunk ) ) return true;
		return false;
	}

}
