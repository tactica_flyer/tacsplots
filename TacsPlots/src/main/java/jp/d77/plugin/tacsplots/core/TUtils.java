package jp.d77.plugin.tacsplots.core;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.ChunkLoc;
import jp.d77.plugin.tacsplots.datas.Consts.FRIEND_TYPE;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tacsplots.datas.PermData;
import jp.d77.plugin.tacsplots.datas.PlotData;
import jp.d77.plugin.tacsplots.datas.TownData;
import jp.d77.plugin.tacsplots.datas.WorldData;
import jp.d77.plugin.tputils.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TUtils {
	protected static TacsPlots core = TacsPlots.plugin;

	public static PERM_TYPE str2PermType( String perm ){
		if ( perm == null ) return null;

		if ( perm.equalsIgnoreCase( "open" ) ){
			return  PERM_TYPE.OPEN;
		}else if ( perm.equalsIgnoreCase( "pvp" ) ){
			return  PERM_TYPE.PVP;
		}else if ( perm.equalsIgnoreCase( "build" ) ){
			return  PERM_TYPE.BUILD;
		}else if ( perm.equalsIgnoreCase( "fire" ) ){
			return  PERM_TYPE.FIRE;
		}else if ( perm.equalsIgnoreCase( "switch" ) ){
			return  PERM_TYPE.SWITCH;
		}else if ( perm.equalsIgnoreCase( "mob" ) ){
			return  PERM_TYPE.MOB;
		}else if ( perm.equalsIgnoreCase( "animal" ) ){
			return  PERM_TYPE.ANIMAL;
		}else if ( perm.equalsIgnoreCase( "explosion" ) ){
			return  PERM_TYPE.EXPLOSION;
		}else if ( perm.equalsIgnoreCase( "exp" ) ){
			return  PERM_TYPE.EXPLOSION;
		}else if ( perm.equalsIgnoreCase( "storage" ) ){
			return  PERM_TYPE.STORAGE;
		}else if ( perm.equalsIgnoreCase( "rental" ) ){
			return  PERM_TYPE.RENTAL;
		}else{
			return null;
		}
	}

	public static Boolean str2boolean( String bool ){
		if ( bool == null ) return null;
		if ( bool.equalsIgnoreCase("on") || bool.equalsIgnoreCase("true") || bool.equalsIgnoreCase("allow") || bool.equalsIgnoreCase("1") ){
			return true;
		}else if ( bool.equalsIgnoreCase("off") || bool.equalsIgnoreCase("false") || bool.equalsIgnoreCase("deny") || bool.equalsIgnoreCase("0") ){
			return false;
		}else{
			return null;
		}
	}

	public static String boolean2str( Boolean bool ){
		if ( bool == null ){
			return core.msg.template("MESSAGE.PERM.NULL");
		}
		if ( bool ){
			return core.msg.template("MESSAGE.PERM.ON");
		}else{
			return core.msg.template("MESSAGE.PERM.OFF");
		}
	}

	public static String PermType2Str( PERM_TYPE perm ){
		switch ( perm ){
		case OPEN:		return core.msg.template("MESSAGE.PERM.OPEN");
		case PVP:		return core.msg.template("MESSAGE.PERM.PVP");
		case BUILD:		return core.msg.template("MESSAGE.PERM.BUILD");
		case FIRE:		return core.msg.template("MESSAGE.PERM.FIRE");
		case SWITCH:	return core.msg.template("MESSAGE.PERM.SWITCH");
		case MOB:		return core.msg.template("MESSAGE.PERM.MOB");
		case ANIMAL:	return core.msg.template("MESSAGE.PERM.ANIMAL");
		case EXPLOSION:	return core.msg.template("MESSAGE.PERM.EXPLOSION");
		case STORAGE:	return core.msg.template("MESSAGE.PERM.STORAGE");
		case RENTAL:	return core.msg.template("MESSAGE.PERM.RENTAL");
		}
		return null;
	}

	public static boolean isBuildBlock( Material m ){
		if ( core.obj.blocks.containsKey( m ) ) return true;
		return false;
	}

	public static boolean isBuildEntity( String e ){
		if ( core.obj.builds.containsKey( e.toUpperCase() ) ) return true;
		return false;
	}

	public static boolean isMobEntity( String e ){
		if ( core.obj.mobs.containsKey( e.toUpperCase() ) ) return true;
		return false;
	}

	public static boolean isSwitchBlock( String e ){
		//if ( core.obj.switchs.containsKey( e.toUpperCase() ) ) return true;
		if ( core.obj.isSwitchBlock( e.toUpperCase() ) ) return true;
		return false;
	}

	public static boolean isStorage( String e ){
		if ( core.obj.isStorage( e.toUpperCase() ) ) return true;
		return false;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Server method
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * サーバ全体の権限設定を取得する
	 * @return
	 */
	public static PermData getDefaultPerm(){
		return core.town.def_perm;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// World method
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * World名からWorldDataを取得する
	 * @param world_name
	 * @return
	 */
	public static WorldData getWorldData( String world_name ){
		for ( WorldData wd: core.town.worlds ){
			if ( wd.getTitle().equalsIgnoreCase( world_name ) ) return wd;
		}
		return null;
	}

	/**
	 * ワールド権限を取得
	 * @param world_name
	 * @return
	 */
	public static PermData getWorldPerm( String world_name ){
		// デフォルト値
		PermData ret = new PermData( TUtils.getDefaultPerm() );

		// ワールド設定
		WorldData wd = TUtils.getWorldData( world_name );
		if ( wd != null ) {
			ret = PermData.over( ret, wd.getPerm() );
		}

		return ret;
	}

	/**
	 * ワールドが存在しているか確認
	 * @param cs
	 * @param world_name
	 * @return
	 */
	public static boolean validWorld( CommandSender cs, String world_name ){

		for ( World w: Bukkit.getWorlds() ){
			if ( w.getName().equalsIgnoreCase( world_name ) ){
				// 存在している
				return true;
			}
		}

		// 存在していない
		if ( cs != null ){
			core.msg.addMessage( cs, "MESSAGE.WORLD.NOT_FOUND" );
			core.msg.warning( "world not found" );
		}

		return false;
	}

	/**
	 * TacsPlotsを使用できるワールドか?
	 * @param cs
	 * @param world_name
	 * @return
	 */
	public static boolean isEnableWorld( CommandSender cs, String world_name ){

		// ワールド有無確認
		if ( ! TUtils.validWorld(cs, world_name) ) return false;

		// 管理下か確認
		for ( WorldData wd: core.town.worlds ){
			if ( wd.getTitle().equalsIgnoreCase( world_name ) ) return true;
		}

		// 管理下では無い
		if ( cs != null ){
			core.msg.addMessage( cs, "MESSAGE.WORLD.NOT_ENABLE" );
			core.msg.warning( "world not enable" );
		}

		return false;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Town method
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * オーナ名からTownDataを取得
	 * @param owner
	 * @return
	 */
	public static TownData getTownData( String owner ){
		for ( TownData td: core.town.towns ){
			if ( owner != null ){
				if ( td.getOwner().equalsIgnoreCase( owner ) ) return td;
			}
		}

		return null;
	}

	public static TownData getTownDataFromTown( String town_name ){
		if ( town_name == null ) return null;
		for ( TownData td: core.town.towns ){
			if ( town_name != null && td.getTitle() != null ){
				if ( td.getTitle().equalsIgnoreCase( town_name ) ) return td;
			}
		}

		return null;
	}

	/**
	 * 町の権限を取得。
	 * @param player_name
	 * @return
	 */
	public static PermData getTownPerm( String player_name ){
		// デフォルト値
		PermData ret = new PermData( TUtils.getDefaultPerm() );

		// ワールドの権限は干渉しない

		TownData td = TUtils.getTownData( player_name );
		if ( td != null ) {
			ret = PermData.over( ret, td.getPerm() );
		}

		return ret;
	}

	/**
	 * PlotPointを計算する
	 * @param player_name
	 * @return
	 */
	public static int calcMaxPlotPoint( String owner_name ){
		int	ret = 0;

		for ( PlotData pd: core.town.plots.values() ){
			if ( pd.getOwner().equalsIgnoreCase( owner_name ) ) ret += pd.getPlotPoints();
		}

		return ret;
	}

	/**
	 * 対象としたいオーナ名を取得する。町名が指定されていれば町名から、でなければオーナ名から。それすら無いなら他のプレイヤー名を返す。
	 * @param cs
	 * @param player_name
	 * @param owner_name
	 * @param town_name
	 * @return
	 */
	public static String getOwnerName( CommandSender cs, String player_name, String owner_name, String town_name ){
		if ( owner_name != null && town_name != null ){
			// 町とオーナ両方指定してしまってる
			if ( cs != null ) core.msg.addMessage( cs, "MESSAGE.ADMIN.OWNER_OR_TOWN" );
			core.msg.debug( "TUtils", "getOwnerName", "both owner and town. owner=" + owner_name + " town=" + town_name );
			return null;
		}

		if ( town_name != null ) {
			// 町名が指定されている
			TownData td = TUtils.getTownDataFromTown( town_name );
			if ( td == null ){
				// `指定した名前の町は無い
				if ( cs != null ) core.msg.addMessage( cs, "MESSAGE.TOWNS.NOT_FOUND" );
				core.msg.debug( "TUtils", "getOwnerName", "town not found owner=" + owner_name + " town=" + town_name );
				return null;
			}
		}

		if ( owner_name != null ) {
			core.msg.debug( "TUtils", "getOwnerName", "owner=" + owner_name );
			return owner_name;
		}

		if ( player_name == null ) {
			// 何も指定されていない
			if ( cs != null ) core.msg.addMessage( cs, "MESSAGE.ADMIN.OWNER_OR_TOWN" );
			core.msg.debug( "TUtils", "getOwnerName", "Unknown Player and Town name." );
			return null;
		}

		core.msg.debug( "TUtils", "getOwnerName", "owner=" + player_name );
		return player_name;
	}

	/**
	 * 町配下のプロット数を取得する
	 * @param owner_name
	 * @return
	 */
	public static int getTownPlots( String owner_name ){
		int cnt = 0;
		for ( PlotData pd: core.town.plots.values() ){
			if ( pd.getOwner().equalsIgnoreCase( owner_name ) ){
				cnt ++;
			}
		}
		return cnt;
	}

	/**
	 * 町データの更新日を更新する
	 * @param owner_name
	 */
	public static void TownUpdate( String owner_name ){
		TownData td = TUtils.getTownData( owner_name );
		if ( td != null ) td.update(true);
	}
	// ---------------------------------------------------------------------------------------------------------------
	// Plot method
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * PlotDataを取得
	 * @param c
	 * @return
	 */
	public static PlotData getPlotData( ChunkLoc c ){
		if ( ! core.town.plots.containsKey( c.toKey() ) ) return null;
		return core.town.plots.get( c.toKey() );
	}

	/**
	 * Plotの権限を取得
	 * @param c
	 * @return
	 */
	public static PermData getPlotPerm( ChunkLoc c ){
		PermData ret;

		PlotData pd = TUtils.getPlotData(c);
		if ( pd == null ){
			// 未取得の土地
			ret = TUtils.getWorldPerm( c.getWorldName() );

			ret.set( PERM_TYPE.OPEN, true );	// 未取得の土地はOPEN設定になる。
		}else{
			// 取得済みの土地
			ret = TUtils.getTownPerm( pd.getOwner() );
			ret = PermData.over( ret, pd.getPerm() );
		}

		return ret;
	}
	/**
	 * PlotDataを取得。できなければエラー。
	 * @param cs
	 * @param cl
	 * @return
	 */
	public static PlotData getClaimedPlot( CommandSender cs, ChunkLoc cl ){
		PlotData	pd = TUtils.getPlotData( cl );

		if ( pd == null ){
			// 所有されていない土地
			if ( cs != null ){
				core.msg.addMessage( cs, "MESSAGE.PLOTS.NO_CLAIM" );
				core.msg.warning( "not claimed plot" );
			}
			return null;
		}

		return pd;
	}

	/**
	 * 指定のプロットに対するプレイヤーの権限を取得
	 * @param cl
	 * @param player_name
	 * @return
	 */
	public static FRIEND_TYPE getPlotPlayerType( ChunkLoc cl, String player_name ){
		PlotData pd = TUtils.getPlotData(cl);

		// 誰の土地でもない
		if ( pd == null ) return FRIEND_TYPE.GUEST;

		if ( pd.getOwner().equalsIgnoreCase( player_name ) ){
			// プロットオーナ
			return FRIEND_TYPE.OWNER;
		}

		if ( pd.isSubOwner( player_name ) ){
			// プロットサブオーナ
			return FRIEND_TYPE.OWNER;
		}

		if ( pd.isFriend( player_name ) ){
			// プロットフレンド
			return FRIEND_TYPE.FRIEND;
		}

		TownData td = TUtils.getTownData( pd.getOwner() );

		// 町自体が存在しない
		if ( td == null ) return FRIEND_TYPE.GUEST;

		if ( td.isSubOwner( player_name ) ){
			// 町のサブオーナ
			return FRIEND_TYPE.OWNER;
		}

		if ( td.isFriend( player_name ) ){
			// 町のフレンド
			return FRIEND_TYPE.FRIEND;
		}

		return FRIEND_TYPE.GUEST;
	}

	public static boolean isPlotOwner( ChunkLoc cl, String player_name ){
		FRIEND_TYPE pt = TUtils.getPlotPlayerType(cl, player_name);
		if ( pt.equals( FRIEND_TYPE.OWNER ) ) return true;
		return false;
	}

	public static boolean isPlotFriend( ChunkLoc cl, String player_name ){
		FRIEND_TYPE pt = TUtils.getPlotPlayerType(cl, player_name);
		if ( pt.equals( FRIEND_TYPE.OWNER ) ) return true;
		if ( pt.equals( FRIEND_TYPE.FRIEND ) ) return true;
		return false;
	}

	/**
	 * 土地の指定した権限を取得する
	 * @param p
	 * @param cl
	 * @param pt
	 * @return true=権限を有している, false=有していない
	 */
	public static boolean isPerm( Player p, ChunkLoc cl, PERM_TYPE pt ){
		FRIEND_TYPE player_type;

		// ワールド確認
		if ( ! TUtils.isEnableWorld( null, cl.getWorldName() ) ) {
//			core.msg.debug( "TUtils" , "isPerm", "disable world: " + cl.getWorldName() );
			return true;
		}

		if ( p == null ){
			player_type = FRIEND_TYPE.FRIEND;
		}else{
			player_type = TUtils.getPlotPlayerType( cl, p.getName() );
		}
		//core.msg.debug( "TUtils" , "isPerm", "player_type=" + player_type.toString() );

		if ( player_type == FRIEND_TYPE.OWNER ) {
//			core.msg.debug( "TUtils" , "isPerm", "Plot Owner" );
			return true;	// オーナ
		}

		PermData pd = TUtils.getPlotPerm(cl);
		if ( pd == null ) {
			// データがおかしい
			core.msg.error( "TUtils:isPerm" );
			return false;
		}

		//core.msg.debug( "TUtils" , "isPerm", "perm_type:" + pt.toString() + "=" + pd.get( pt ) );

		if ( pd.get( PERM_TYPE.OPEN ) ){
			// オープン
//			core.msg.debug( "TUtils" , "isPerm", "Open Plot" );
			return pd.get( pt );
		}else{
			// クローズ
			if ( player_type == FRIEND_TYPE.GUEST ) {
//				core.msg.debug( "TUtils" , "isPerm", "Plot Guest" );
				return false;
			}
			return pd.get( pt );
		}
	}

	public static void sendOwnersMsg( String owner_name, CommandSender p, String Msg, String... args ){
		TownData	td = TUtils.getTownData(owner_name);

		if ( td != null ){
			TUtils.sendOwnerMsg( owner_name, p, Msg, args);
			for ( String sub_owner: td.getFriends( FRIEND_TYPE.OWNER ) ){
				TUtils.sendOwnerMsg( sub_owner, p, Msg, args);
			}
		}
		core.msg.addMessage( p, Msg, args );
	}

	public static void sendOwnerMsg( String owner_name, CommandSender p, String Msg, String... args ){
		Player owner = Utils.getOnlinePlayer( owner_name );
		if ( owner != null ){
			if ( ! owner.getName().equalsIgnoreCase( p.getName() ) ){
				String m = core.msg.template( Msg, args );
				core.msg.addMessage( owner, "&7" + p.getName() + ">&r" + m );
			}
		}
	}
}
