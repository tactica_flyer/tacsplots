package jp.d77.plugin.tacsplots.datas;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.core.TUtils;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tputils.kvs.KeyValue;

public class WorldData {
	protected static TacsPlots core = TacsPlots.plugin;

	protected String	title	= null;
	protected PermData	perm	= new PermData();

	public WorldData( String title ){
		this.title = title;
	}

	public WorldData( KeyValue kvs, String root ){
		this.load(kvs, root);
	}

	public void save( KeyValue kvs, String root ){
		core.msg.debug( this.getClass().getSimpleName(), "save", "root=" + root );

		if ( this.title != null ) kvs.setString( root + ".title", this.title);
		this.perm.save( kvs, root + ".perm" );
	}

	public void load( KeyValue kvs, String root ){
		core.msg.debug( this.getClass().getSimpleName(), "load", "root=" + root );

		if ( kvs.getString( root + ".title" ) != null ) this.title = kvs.getString( root + ".title" );
		this.perm.load( kvs, root + ".perm" );
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Gets
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 名称を取得する
	 * @return
	 */
	public String getTitle(){
		return this.title;
	}

	/**
	 * 権限を取得する
	 * @return
	 */
	public PermData getPerm(){
		return this.perm;
	}

	public void setPerm( PERM_TYPE pt, boolean b ){
		this.perm.set(pt, b);
	}

	public String info(){
		PermData pd = TUtils.getWorldPerm( this.title );

		return core.msg.template( "MESSAGE.WORLD.INFO.VIEW"
				, this.getTitle()
				) + "\n"
				+ pd.info();
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Sets
	// ---------------------------------------------------------------------------------------------------------------



}
