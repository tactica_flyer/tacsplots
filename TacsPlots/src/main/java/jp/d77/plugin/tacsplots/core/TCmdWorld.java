package jp.d77.plugin.tacsplots.core;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.CmdOpts;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tacsplots.datas.WorldData;
import jp.d77.plugin.tputils.Utils;

public class TCmdWorld {
	protected static TacsPlots core = TacsPlots.plugin;
	private	boolean	enable = false;

	public TCmdWorld(){
	}

	public boolean init(){
		this.enable = true;
		return true;
	}

	public boolean destroy(){
		this.enable = false;
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// World method
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * ワールドをプラグインの管理下に置く
	 * @param cmd
	 * @return
	 */
	public void enable_world( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.admin.world.enable");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.WORLD.HELP" );
			return;
		}

		//core.town.enable_world( cmd.getSender(), cmd.getOptWorld() );

		String world_name = cmd.getOptWorld();

		if ( world_name == null ){
			// ワールド名が指定されていない
			if ( Utils.isConsole( cmd.getSender() ) ){
				// コンソールから
				core.msg.addMessage( cmd.getSender(), "MESSAGE.WORLD.HELP" );
				core.msg.debug( this.getClass().getSimpleName(), "enable_world", "world not instruction" );
				return;
			}else{
				// プレイヤーから
				world_name = cmd.getPlayer().getLocation().getWorld().getName();
			}
		}

		// ワールド有無チェック
		if ( ! TUtils.validWorld( cmd.getSender(), world_name ) ) return;

		WorldData wd = TUtils.getWorldData( world_name );
		if ( wd == null ){
			core.town.worlds.add( new WorldData( world_name ) );
		}
		core.msg.addMessage( cmd.getSender(), "MESSAGE.WORLD.ENABLE" );
		core.msg.debug( this.getClass().getSimpleName(), "enable_world", "complete" );

		return;
	}

	/**
	 * ワールドをプラグインの管理対象外にする
	 * @param cmd
	 * @return
	 */
	public void disable_world( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.admin.world.enable");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.WORLD.HELP" );
			return;
		}

		//core.town.disable_world( cmd.getSender(), cmd.getOptWorld() );
		String world_name = cmd.getOptWorld();

		if ( world_name == null ){
			// ワールド名が指定されていない
			if ( Utils.isConsole( cmd.getSender() ) ){
				// コンソールから
				core.msg.addMessage( cmd.getSender(), "MESSAGE.WORLD.HELP" );
				core.msg.debug( this.getClass().getSimpleName(), "disable_world", "world not instruction" );
				return;
			}else{
				// プレイヤーから
				world_name = cmd.getPlayer().getLocation().getWorld().getName();
			}
		}

		// ワールド有無チェック
		if ( ! TUtils.validWorld( cmd.getSender(), world_name ) ) return;

		WorldData wd = TUtils.getWorldData( world_name );
		if ( wd != null ){
			core.town.worlds.remove( new WorldData( world_name ) );
		}
		core.msg.addMessage( cmd.getSender(), "MESSAGE.WORLD.DISABLE" );
		core.msg.debug( this.getClass().getSimpleName(), "disable_world", "complete" );

		return;
	}

	public void world_setperm( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.admin.world.set.perm");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.WORLD.SET.PERM.HELP" );
			return;
		}

		String world_name	= cmd.getOptWorld();
		String perm			= cmd.getOptCommands(2);
		String setdata		= cmd.getOptCommands(3);

		PERM_TYPE	val_perm;
		Boolean		val_setdata;

		if ( world_name == null ){
			// ワールド名が指定されていない
			if ( Utils.isConsole( cmd.getSender() ) ){
				// コンソールから
				core.msg.addMessage( cmd.getSender(), "MESSAGE.WORLD.NOT_FOUND" );
				core.msg.debug( this.getClass().getSimpleName(), "world_setperm", "world not instruction" );
				return;
			}else{
				// プレイヤーから
				world_name = cmd.getPlayer().getLocation().getWorld().getName();
			}
		}

		// ワールドチェック
		if ( ! TUtils.isEnableWorld( cmd.getSender() , world_name ) ) return;

		val_perm	= TUtils.str2PermType(perm);
		val_setdata	= TUtils.str2boolean(setdata);

		if ( val_perm == null || val_setdata == null ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.WORLD.SET.PERM.HELP" );
			return;
		}
		if ( val_perm.equals( PERM_TYPE.RENTAL ) ){
			// 貸し出し区画権限は設定不可
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.SET.PERM.HELP" );
			return;
		}

		WorldData wd = TUtils.getWorldData(world_name);
		if ( wd == null ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.WORLD.NOT_ENABLE" );
			return;
		}
		wd.setPerm( val_perm, val_setdata );
		core.msg.addMessage(
				cmd.getSender()
				, "MESSAGE.WORLD.SET.PERM.DONE"
				, world_name
				, TUtils.PermType2Str( val_perm )
				, TUtils.boolean2str( val_setdata )
				);
	}

	public void world_info( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.player.world.info");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.WORLD.INFO.HELP" );
			return;
		}

		String world_name = cmd.getOptWorld();

		if ( cmd.isConsole() ){
			if ( world_name == null ) {
				core.msg.addMessage( cmd.getSender(), "MESSAGE.WORLD.INFO.HELP" );
				return;
			}
		}else{
			if ( world_name == null ) {
				world_name = cmd.getPlayer().getLocation().getWorld().getName();
			}
		}

		WorldData wd = TUtils.getWorldData( world_name );
		if ( wd == null ){
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.WORLD.NOT_ENABLE" );
			return;
		}

		core.msg.addMessage( cmd.getSender(), wd.info() );
		core.msg.debug( this.getClass().getSimpleName(), "town_info", "complete" );

		return;
	}
}
