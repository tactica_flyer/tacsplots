package jp.d77.plugin.tacsplots.core;

import java.util.ArrayList;
import java.util.HashMap;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.ChunkLoc;
import jp.d77.plugin.tacsplots.datas.Consts.MOVE_MESSAGE_MODE;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tacsplots.datas.PlotData;
import jp.d77.plugin.tacsplots.datas.TownData;
import jp.d77.plugin.tputils.Utils;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityBreakDoorEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;

public class TEvents {
	protected static TacsPlots core = TacsPlots.plugin;
	protected boolean enable = false;


	protected HashMap<Player, ChunkLoc> player_chunkloc;	// プレイヤーが居た場所
	protected HashMap<Player, String> player_navmsg;		// プレイヤーへアナウンスした場所
	protected HashMap<String,String> event_cache;			// イベントキャッシュ
	protected HashMap<String,MOVE_MESSAGE_MODE> move_mode;	// 移動メッセージモード

	public TEvents(){
	}

	public boolean init(){
		this.player_chunkloc = new HashMap<Player, ChunkLoc>();
		this.player_navmsg = new HashMap<Player, String>();
		this.event_cache = new HashMap<String,String>();
		this.move_mode = new HashMap<String,MOVE_MESSAGE_MODE>();
		this.enable = true;
		return true;
	}

	public boolean destroy(){
		this.enable = false;
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	public void setMoveMessageMode( String player_name, MOVE_MESSAGE_MODE mode ){
		this.move_mode.put( player_name.toLowerCase(), mode );
	}
	public MOVE_MESSAGE_MODE getMoveMessageMode( String player_name ){
		if ( this.move_mode.containsKey( player_name.toLowerCase() ) ){
			return this.move_mode.get( player_name.toLowerCase() );
		}
		return MOVE_MESSAGE_MODE.NORMAL;
	}

	public void checkMoveMessage(){
		ChunkLoc	cl;

		for ( Player p : Bukkit.getOnlinePlayers() ) {

			if ( ! TUtils.isEnableWorld( null, p.getLocation().getWorld().getName() ) ) continue;	// 無関係なワールド

			cl = new ChunkLoc( p.getLocation() );

			if ( this.player_chunkloc.containsKey(p) ){
				if ( cl != this.player_chunkloc.get(p) ) this.castMoveMessage( p, cl );
			}else{
				// 初めて補足したプレイヤー
				this.castMoveMessage( p, cl );
			}

		}
	}

	/**
	 * 前と違うChunkへ移動した際のメッセージ処理
	 * @param p
	 * @param cl
	 */
	private void castMoveMessage( Player p, ChunkLoc cl ){
		PlotData pd = TUtils.getPlotData(cl);
		TownData td;
		String msg = "";

		if ( this.getMoveMessageMode( p.getName() ).equals( MOVE_MESSAGE_MODE.NONE ) ) return;

		if ( pd == null ){
			// 空き地
			msg += core.msg.template( "MESSAGE.LANG.WILD" );
		}else{

			// オーナ名
			msg += "&7" + core.msg.template( "MESSAGE.LANG.OF_LAND", pd.getOwner() ) + "&r> ";

			// 町名を確認
			td = TUtils.getTownData( pd.getOwner() );
			if ( td != null ){
				if ( td.getTitle() != null ){
					msg += core.msg.template( "MESSAGE.TOWNS.TITLE" ) + ": " + td.getTitle();
				}
				if ( td.isHome( cl ) ){
					msg += "(" + core.msg.template( "MESSAGE.TOWNS.HOME.HOME" ) + ")";
				}
			}

			// 区画名
			if ( pd.getTitle() != null ){
				// 土地に名前が付いてる
				msg += "(" + core.msg.template( "MESSAGE.PLOTS.TITLE" ) + ": " + pd.getTitle() + ")";
			}

			if ( this.getMoveMessageMode( p.getName() ).equals( MOVE_MESSAGE_MODE.VERBOSE ) ) {
				msg += "\n" + pd.simple_info();
			}
		}

		if ( ! this.player_navmsg.containsKey( p ) ){
			this.player_navmsg.put(p, msg);
			core.msg.addMessage(p, msg);
		}else if ( ! this.player_navmsg.get( p ).equals( msg ) ){
			this.player_navmsg.put(p, msg);
			core.msg.addMessage(p, msg);
		}
	}

	/**
	 * 破壊/建築禁止場所
	 * @param ename
	 * @param e
	 * @return true=破壊/建築キャンセル
	 */
	public boolean isBuildCancel( String ename, Event e ){
		if ( ! this.enable ) return false;
		Player		p;
		Location	loc;

		// イベントクラス解析
		if ( ename.equals("BlockBreakEvent") ){							// ブロック破壊
			p = ((BlockBreakEvent) e).getPlayer();
			loc = ((BlockBreakEvent) e).getBlock().getLocation();

		}else if ( ename.equals("BlockPlaceEvent") ){					// ブロック設置
			p = ((BlockPlaceEvent) e).getPlayer();
			loc = ((BlockPlaceEvent) e).getBlock().getLocation();

			if ( ((BlockPlaceEvent) e).getBlock().getType().equals( Material.FIRE ) ) return false;	// 着火

		}else if ( ename.equals("HangingBreakByEntityEvent") ){			// 壁掛け破壊
			if ( ! ((HangingBreakByEntityEvent) e).getRemover().getType().equals( EntityType.PLAYER ) ) return false;
			p = (Player)((HangingBreakByEntityEvent) e).getRemover();
			loc = ((HangingBreakByEntityEvent) e).getEntity().getLocation();

		}else if ( ename.equals("HangingPlaceEvent") ){					// 壁掛け設置
			p = ((HangingPlaceEvent) e).getPlayer();
			loc = ((HangingPlaceEvent) e).getBlock().getLocation();

		}else if ( ename.equals("PlayerInteractEntityEvent") ){			// 額縁のアイテム設置、額縁のアイテム回転

			if ( ! TUtils.isBuildEntity( ((PlayerInteractEntityEvent) e).getRightClicked().getType().toString() ) ) return false;

			p = ((PlayerInteractEntityEvent) e).getPlayer();
			loc = ((PlayerInteractEntityEvent) e).getRightClicked().getLocation();

		}else if ( ename.equals("EntityDamageByEntityEvent") ){			// 額縁のアイテム撤去
			if ( ! ((EntityDamageByEntityEvent) e).getDamager().getType().equals( EntityType.PLAYER ) ) return false;
			if ( ! TUtils.isBuildEntity( ((EntityDamageByEntityEvent) e).getEntity().getType().toString() ) ) return false;

			p = (Player)((EntityDamageByEntityEvent) e).getDamager();
			loc = ((EntityDamageByEntityEvent) e).getEntity().getLocation();

		}else if ( ename.equals("EntityChangeBlockEvent") ){			// プレイヤー以外がブロックを変更
			p = null;
			loc = ((EntityChangeBlockEvent) e).getBlock().getLocation();
			Block b = ((EntityChangeBlockEvent) e).getBlock();
			if ( b != null ){
				if ( b.getType().equals( Material.GRASS ) || b.getType().equals( Material.LONG_GRASS ) ) return false;
			}
			core.msg.debug( this.getClass().getSimpleName() , "isBuildCancel", "EntityChangeBlockEvent block=" + ((EntityChangeBlockEvent) e).getBlock().getType().toString() );

		}else if ( ename.equals("EntityBreakDoorEvent") ){				// ゾンビがドアを壊す時
			p = null;
			loc = ((EntityBreakDoorEvent) e).getBlock().getLocation();

		}else if ( ename.equals("PlayerBucketEmptyEvent") ){			// バケツの中身をぶちまけた
			p = (Player)((PlayerBucketEmptyEvent) e).getPlayer();
			loc = ((PlayerBucketEmptyEvent) e).getBlockClicked().getLocation();

		}else if ( ename.equals("PlayerBucketFillEvent") ){				// バケツの中身を満たした
			p = (Player)((PlayerBucketFillEvent) e).getPlayer();
			loc = ((PlayerBucketFillEvent) e).getBlockClicked().getLocation();

		}else if ( ename.equals("VehicleDamageEvent") ){				// 乗り物ダメージ
			if ( ((VehicleDamageEvent) e).getAttacker() == null ) return false;
			if ( ((VehicleDamageEvent) e).getAttacker().getType() == null ) return false;
			if ( ((VehicleDamageEvent) e).getVehicle() == null ) return false;
			if ( ! ((VehicleDamageEvent) e).getAttacker().getType().equals( EntityType.PLAYER ) ) return false;

			p = (Player)((VehicleDamageEvent) e).getAttacker();
			loc = ((VehicleDamageEvent) e).getVehicle().getLocation();

		}else if ( ename.equals("VehicleDestroyEvent") ){				// 乗り物消滅
			if ( ! ((VehicleDamageEvent) e).getAttacker().getType().equals( EntityType.PLAYER ) ) return false;

			p = (Player)((VehicleDestroyEvent) e).getAttacker();
			loc = ((VehicleDestroyEvent) e).getVehicle().getLocation();

		}else{
			core.msg.error( "TEvents.isBuildCancel not proc event: " + ename );
			return false;
		}

		//core.msg.debug( this.getClass().getSimpleName() , "isBuildCancel", "e=" + ename );

		if ( p != null ){
			if ( p.getGameMode().equals( GameMode.CREATIVE ) ) return false;	// クリエイティブ
		}

		if ( TUtils.isPerm( p, new ChunkLoc( loc ) , PERM_TYPE.BUILD ) ){
			// OK
			return false;
		}else{
			// NG -> build cancel;
			if ( p != null ) core.msg.addMessage(p, "MESSAGE.PLOTS.EVENT.BUILD.CANCEL");
			return true;
		}
	}

	/**
	 * PvP禁止
	 * @param ename
	 * @param e
	 * @return true=PvPキャンセル
	 */
	public boolean isPvPCancel( String ename, Event e ){
		if ( ! this.enable ) return false;
		Player		f_p = null;
		ArrayList<Player> t_p = new ArrayList<Player>();

		//core.msg.debug( this.getClass().getSimpleName() , "isPvPCancel", "e=" + ename );

		// イベントクラス解析
		if ( ename.equals("EntityDamageByEntityEvent") ){			// エンティティダメージ
			EntityDamageByEntityEvent edbee = (EntityDamageByEntityEvent)e;

			if ( edbee.getDamager().getType().equals( EntityType.PLAYER ) ){
				// Fromがプレイヤー
				f_p = (Player)(edbee.getDamager());
				core.msg.debug( this.getClass().getSimpleName() , "isPvPCancel", "[" + ename + "] pvp from:" + f_p.getName() );

			}else  if ( edbee.getDamager() instanceof Projectile && edbee.getEntityType().equals( EntityType.PLAYER ) ) {
				// Fromが飛翔物でToがプレイヤー
				Projectile proj = (Projectile) edbee.getDamager();
				if ( proj.getShooter() instanceof Player ){
					f_p = (Player)proj.getShooter();
					core.msg.debug( this.getClass().getSimpleName() , "isPvPCancel", "[" + ename + "] pvp shooter from:" + f_p.getName() );
				}
			}

			if ( edbee.getEntityType().equals( EntityType.PLAYER ) ){
				// Toがプレイヤー
				t_p.add( (Player)edbee.getEntity() );
				core.msg.debug( this.getClass().getSimpleName() , "isPvPCancel", "[" + ename + "] pvp to:" + ((Player)edbee.getEntity()).getName() );
			}

		}else if ( ename.equals("PotionSplashEvent") ){				// スプラッシュポーションが割れる時

			if ( ((PotionSplashEvent)e).getPotion().getShooter() instanceof Player ){
				f_p = (Player)((PotionSplashEvent)e).getPotion().getShooter();
				core.msg.debug( this.getClass().getSimpleName() , "isPvPCancel", "[" + ename + "] potion from:" + f_p.getName() );
			}else{
				return false;
			}

			for ( LivingEntity le: ((PotionSplashEvent)e).getAffectedEntities() ){
				if ( le.getType().equals( EntityType.PLAYER ) ) {
					t_p.add( (Player)le );
					core.msg.debug( this.getClass().getSimpleName() , "[" + ename + "] isPvPCancel", "potion to:" + ((Player)le).getName() );
				}
			}

		}else{
			core.msg.error( "TEvents.isPvPCancel not proc event: " + ename );
			return false;
		}

		// --- from
		// プレイヤーでは無い
		if ( f_p == null ) return false;

		// 攻撃元がプレイヤー
		if ( ! TUtils.isPerm( null, new ChunkLoc( f_p.getLocation() ) , PERM_TYPE.PVP ) ){
			// PvP権限なし
			core.msg.debug( this.getClass().getSimpleName() , "isPvPCancel", "[" + ename + "] block pvp from:" + f_p.getName() );
			if ( t_p.size() > 0 ) return true;	// 攻撃先に一人でもプレイヤーが居れば攻撃不可

			// --- to
			/*
			for ( Player p: t_p ){
				// NG -> PvP cancel;
				core.msg.debug( this.getClass().getSimpleName() , "isPvPCancel", "block pvp from:" + f_p.getName() + " to:" + p.getName() );
			}
			*/
		}

		// 攻撃先がプレイヤー(攻撃元がPvP許可エリアに居る場合)
		for ( Player p: t_p ){
			if ( ! TUtils.isPerm( null, new ChunkLoc( p.getLocation() ) , PERM_TYPE.PVP ) ){
				// 攻撃先がPvP不許可エリアにいる場合は攻撃不可
				// NG -> PvP cancel;
				core.msg.debug( this.getClass().getSimpleName() , "isPvPCancel", "[" + ename + "] block pvp from:" + f_p.getName() + " to:" + p.getName() );
				return true;
			}
		}

		return false;
	}

	/**
	 * Switch禁止
	 * @param ename
	 * @param e
	 * @return true=Switchキャンセル
	 */
	public boolean isSwitchCancel( String ename, Event e ){
		if ( ! this.enable ) return false;
		Player	p   = null;
		String	obj = null;
		Location	loc = null;

		//core.msg.debug( this.getClass().getSimpleName() , "isSwitchCancel", "e=" + ename );

		// イベントクラス解析
		if ( ename.equals("PlayerInteractEvent") ){			// クリック時・感圧版・トリップワイヤー・トロッコを置く?
			if ( ((PlayerInteractEvent)e).getClickedBlock() == null ){
				//core.msg.debug( this.getClass().getSimpleName() , "isSwitchCancel", "null1" );
				return false;
			}
			if ( ((PlayerInteractEvent)e).getClickedBlock().getType() == null ){
				//core.msg.debug( this.getClass().getSimpleName() , "isSwitchCancel", "null2" );
				return false;
			}
			obj = ((PlayerInteractEvent)e).getClickedBlock().getType().toString();
			loc = ((PlayerInteractEvent)e).getClickedBlock().getLocation();
			p   = ((PlayerInteractEvent)e).getPlayer();

		}else{
			core.msg.error( "TEvents.isSwitchCancel not proc event: " + ename );
			return false;
		}

		if ( p == null || obj == null ) return false;
		if ( p.getGameMode().equals( GameMode.CREATIVE ) ) return false;	// クリエイティブ

		if ( ! TUtils.isSwitchBlock( obj ) ) return false;

		if ( this.isCashObj( obj, loc, p.getName() ) ){
			// キャッシュ済み
			return true;
		}

		if ( ! TUtils.isPerm( p, new ChunkLoc( loc ), PERM_TYPE.SWITCH ) ){
			// NG -> Switch cancel;
			core.msg.addMessage(p, "MESSAGE.PLOTS.EVENT.SWITCH.CANCEL");
			this.setCashObj( obj, loc, p.getName() );
			return true;
		}
		return false;
	}

	/**
	 * Storageアクセス禁止
	 * @param ename
	 * @param e
	 * @return true=Storageキャンセル
	 */
	public boolean isStorageCancel( String ename, Event e ){
		if ( ! this.enable ) return false;
		Player	p   = null;
		String	obj = null;
		Location	loc = null;

		//core.msg.debug( this.getClass().getSimpleName() , "isSwitchCancel", "e=" + ename );

		// イベントクラス解析
		if ( ename.equals("PlayerInteractEvent") ){			// クリック時・感圧版・トリップワイヤー・トロッコを置く?
			if ( ((PlayerInteractEvent)e).getClickedBlock() == null ){
				//core.msg.debug( this.getClass().getSimpleName() , "isSwitchCancel", "null1" );
				return false;
			}
			if ( ((PlayerInteractEvent)e).getClickedBlock().getType() == null ){
				//core.msg.debug( this.getClass().getSimpleName() , "isSwitchCancel", "null2" );
				return false;
			}
			obj = ((PlayerInteractEvent)e).getClickedBlock().getType().toString();
			loc = ((PlayerInteractEvent)e).getClickedBlock().getLocation();
			p   = ((PlayerInteractEvent)e).getPlayer();

		}else if ( ename.equals("PlayerInteractEvent") ){			// クリック時・感圧版・トリップワイヤー・トロッコを置く?
			if ( ((PlayerInteractEvent)e).getClickedBlock() == null ){
				//core.msg.debug( this.getClass().getSimpleName() , "isSwitchCancel", "null1" );
				return false;
			}
			if ( ((PlayerInteractEvent)e).getClickedBlock().getType() == null ){
				//core.msg.debug( this.getClass().getSimpleName() , "isSwitchCancel", "null2" );
				return false;
			}
			//obj = ((InventoryOpenEvent)e).getInventory().getLocation().getBlock();
			loc = ((PlayerInteractEvent)e).getClickedBlock().getLocation();
			p   = ((PlayerInteractEvent)e).getPlayer();

		}else{
			core.msg.error( "TEvents.isStorageCancel not proc event: " + ename );
			return false;
		}

		if ( p == null || obj == null ) return false;
		if ( p.getGameMode().equals( GameMode.CREATIVE ) ) return false;	// クリエイティブ

		if ( ! TUtils.isStorage( obj ) ) return false;

		if ( ! TUtils.isPerm( p, new ChunkLoc( loc ), PERM_TYPE.STORAGE ) ){
			// NG -> Switch cancel;
			core.msg.addMessage(p, "MESSAGE.PLOTS.EVENT.STORAGE.CANCEL");
			return true;
		}
		return false;
	}

	/**
	 * 爆発禁止
	 * @param ename
	 * @param e
	 * @return true=爆発キャンセル
	 */
	public boolean isExplosionCancel( String ename, Event e ){
		if ( ! this.enable ) return false;
		Location	loc = null;

		//core.msg.debug( this.getClass().getSimpleName() , "isExplosionCancel", "e=" + ename );

		// イベントクラス解析
		if ( ename.equals("EntityExplodeEvent") ){

			for ( Block block: ((EntityExplodeEvent)e).blockList() ){
				loc = block.getLocation();
				if ( ! TUtils.isPerm( null, new ChunkLoc( loc ), PERM_TYPE.EXPLOSION ) ){
					// NG -> Switch cancel;
					core.msg.debug( this.getClass().getSimpleName() , "isExplosionCancel", "cancel fire p=" + e.getEventName() );
					return true;
				}
			}

		}else{
			core.msg.error( "TEvents.isExplosionCancel not proc event: " + ename );
			return false;
		}

		return false;
	}

	/**
	 * 着火禁止
	 * @param ename
	 * @param e
	 * @return true=着火キャンセル
	 */
	public boolean isFireCancel( String ename, Event e ){
		if ( ! this.enable ) return false;
		Player		p = null;
		Location	loc = null;

		//core.msg.debug( this.getClass().getSimpleName() , "isFireCancel", "e=" + ename );

		// イベントクラス解析
		if ( ename.equals("BlockIgniteEvent") ){	// 着火
			p = ((BlockIgniteEvent)e).getPlayer();
			if ( ((BlockIgniteEvent)e).getBlock() != null ){
				loc = ((BlockIgniteEvent)e).getBlock().getLocation();

			}else{
				return false;
			}

		}else if ( ename.equals("BlockSpreadEvent") ){	// 延焼
			//p = ((BlockSpreadEvent)e).getPlayer();
			if ( ((BlockSpreadEvent)e).getBlock() != null ){
				loc = ((BlockSpreadEvent)e).getBlock().getLocation();

			}else{
				return false;
			}

		}else if ( ename.equals("BlockBurnEvent") ){	// 焼失
			if ( ((BlockBurnEvent)e).getBlock() != null ){
				loc = ((BlockBurnEvent)e).getBlock().getLocation();

			}else{
				return false;
			}

		}else{
			core.msg.error( "TEvents.isFireCancel not proc event: " + ename );
			return false;
		}

		if ( loc == null ) return false;
		if ( p != null ) {
			if ( p.getGameMode().equals( GameMode.CREATIVE ) ) return false;	// クリエイティブ
		}

		if ( ! TUtils.isPerm( p, new ChunkLoc( loc ), PERM_TYPE.FIRE ) ){
			// NG -> Switch cancel;
			if ( p != null ) {
				core.msg.addMessage(p, "MESSAGE.PLOTS.EVENT.FIRE.CANCEL");
				core.msg.debug( this.getClass().getSimpleName() , "isFireCancel", "cancel fire p=" + p.getName() );
			}else{
				core.msg.debug( this.getClass().getSimpleName() , "isFireCancel", "cancel fire p=null" );
			}
			return true;
		}
		return false;
	}

	/**
	 * 出現禁止
	 * @param ename
	 * @param e
	 * @return true=出現キャンセル
	 */
	public boolean isSpawnCancel( String ename, Event e ){
		if ( ! this.enable ) return false;
		String		mob = null;
		Location	loc = null;
		ChunkLoc	cl;

		//core.msg.debug( this.getClass().getSimpleName() , "isSpawnCancel", "e=" + ename );

		// イベントクラス解析
		if ( ename.equals("CreatureSpawnEvent") ){	// 生物が発生
			loc = ((CreatureSpawnEvent)e).getLocation();
			mob = ((CreatureSpawnEvent)e).getEntityType().toString();
/*
		}else if ( ename.equals("EntitySpawnEvent") ){	// 生物が発生
			loc = ((CreatureSpawnEvent)e).getLocation();
			mob = ((CreatureSpawnEvent)e).getEntityType().toString();
*/
		}else{
			core.msg.error( "TEvents.isSpawnCancel not proc event: " + ename );
			return false;
		}

		if ( loc == null ) return false;
		cl = new ChunkLoc( loc );

		if ( ! TUtils.isPerm( null, cl, PERM_TYPE.ANIMAL ) ){
			// NG -> spawn cancel;
			core.msg.debug( this.getClass().getSimpleName() , "isSpawnCancel", "ANIMAL=" + mob + " loc=" + Utils.formatLocation(loc) + " chunk=" + cl.toString() );
			return true;
		}

		if ( TUtils.isMobEntity( mob ) ){
			if ( ! TUtils.isPerm( null, cl, PERM_TYPE.MOB ) ){
				core.msg.debug( this.getClass().getSimpleName() , "isSpawnCancel", "MOB=" + mob + " loc=" + Utils.formatLocation(loc) + " chunk=" + cl.toString() );
				return true;
			}
		}

		//core.msg.debug( this.getClass().getSimpleName() , "isSpawnCancel", "Spawn OK=" + mob );
		//core.msg.warning( this.getClass().getSimpleName() + "[isSpawnCancel]"+ "Spawn OK=" + mob + "cl=" + cl.toString() + " t=" + TUtils.getPlotData( cl ).getOwner() );
		return false;
	}

	public void checkPlotExp( String ename, Event e ){
		if ( ! this.enable ) return;

		Player		p;
		Location	loc;
		ChunkLoc	cl;
		int			pe;

		// イベントクラス解析
		if ( ename.equals("BlockBreakEvent") ){							// ブロック破壊

			if ( ! TUtils.isBuildBlock( ((BlockBreakEvent) e).getBlock().getType() ) ) return;
			p = ((BlockBreakEvent) e).getPlayer();
			loc = ((BlockBreakEvent) e).getBlock().getLocation();
			pe = -1;

		}else if ( ename.equals("BlockPlaceEvent") ){					// ブロック設置
			if ( ! TUtils.isBuildBlock( ((BlockPlaceEvent) e).getBlock().getType() ) ) return;

			p = ((BlockPlaceEvent) e).getPlayer();
			loc = ((BlockPlaceEvent) e).getBlock().getLocation();
			pe = 1;

		}else{
			core.msg.error( "TEvents.PlotExp not proc event: " + ename );
			return;
		}

		if ( ! p.getGameMode().equals( GameMode.SURVIVAL ) ) return;

		cl = new ChunkLoc( loc );
		if ( ! TUtils.isPlotFriend( cl, p.getName() ) ) return;
		PlotData pd = TUtils.getPlotData( cl );

		int pp = pd.getPlotPoints();

		pd.addPlotExp( pe );

		if ( pp < pd.getPlotPoints() ){
			// LvUp
			if ( p != null ) core.msg.addMessage(p, "MESSAGE.PLOTS.LVUP", pp + "->" + pd.getPlotPoints() );

		}else if ( pp > pd.getPlotPoints() ){
			// LvDown
			if ( p != null ) core.msg.addMessage(p, "MESSAGE.PLOTS.LVDOWN", pp + "->" + pd.getPlotPoints() );
		}

		return;
	}

	/**
	 * キャッシュ済みのオブジェクトを確認
	 * @param objct_name
	 * @param loc
	 * @param player_name
	 * @return
	 */
	private boolean isCashObj( String objct_name, Location loc, String player_name ){
		if ( this.event_cache.containsKey( player_name ) ){
			if ( this.event_cache.get( player_name ).equalsIgnoreCase( objct_name + "_" + Utils.formatLocation(loc) ) ){
				// 前回と同じ確認
				return true;
			}else{
				// 前回のキャッシュから移動していたらクリア
				this.event_cache.remove(player_name);
			}
		}
		return false;
	}

	/**
	 * チェック済みのオブジェクトをキャッシュ
	 * @param objct_name
	 * @param loc
	 * @param player_name
	 */
	private void setCashObj( String objct_name, Location loc, String player_name ){
		this.event_cache.put(player_name,  objct_name + "_" + Utils.formatLocation(loc) );
	}

}
