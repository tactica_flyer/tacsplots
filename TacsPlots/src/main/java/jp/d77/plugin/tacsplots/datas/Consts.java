package jp.d77.plugin.tacsplots.datas;

public class Consts {
	public static enum FRIEND_TYPE {
		OWNER,FRIEND,GUEST
	}

	public static enum PERM_TYPE {
		OPEN,			// 対ゲスト
		PVP,			// 対人戦の許可
		BUILD,			// 建築の許可
		FIRE,			// 着火の許可
		SWITCH,			// ドア、宝箱の開閉
		MOB,			// 敵対動物の出現
		ANIMAL,			// 全動物の出現
		EXPLOSION,		// 爆発による破壊の許可
		STORAGE,		// 保管庫の保護
		RENTAL,			// レンタル区画
	}

	public static enum MOVE_MESSAGE_MODE {
		NONE,NORMAL,VERBOSE
	}

}
