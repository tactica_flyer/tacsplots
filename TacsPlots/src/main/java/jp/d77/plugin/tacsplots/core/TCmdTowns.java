package jp.d77.plugin.tacsplots.core;

import java.util.ArrayList;
import java.util.HashMap;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.ChunkLoc;
import jp.d77.plugin.tacsplots.datas.CmdOpts;
import jp.d77.plugin.tacsplots.datas.Consts.FRIEND_TYPE;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tacsplots.datas.PlotData;
import jp.d77.plugin.tacsplots.datas.TownData;
import jp.d77.plugin.tputils.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TCmdTowns {
	protected static TacsPlots core = TacsPlots.plugin;
	private	boolean	enable = false;

	public TCmdTowns(){
	}

	public boolean init(){
		this.enable = true;
		return true;
	}

	public boolean destroy(){
		this.enable = false;
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Town method
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * 町データ追加
	 * @param p
	 */
	public void create_town( Player p ){
		TownData td = TUtils.getTownData( p.getName() );
		if ( td != null ) return;	// 町あり

		core.town.towns.add( new TownData( p.getName() ) );
		core.town.checkInvincibleMode( p );
		core.msg.debug( this.getClass().getSimpleName(), "create_town", "complete:" + p.getName() );
	}

	/**
	 * Town Infomation
	 * @param cmd
	 */
	public void town_info( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.player.towns.info");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.TOWNS.INFO.HELP" );
			return;
		}

		String owner_name;
		TownData	td;

		owner_name = TUtils.getOwnerName( cmd.getSender(), cmd.getPlayerName(), cmd.getOptPlayer(), cmd.getOptTown() );
		if ( owner_name == null ) return;

		td = TUtils.getTownData( owner_name );
		if ( td == null ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.NOT_FOUND" );
			return;
		}

		core.msg.addMessage( cmd.getSender(), td.info() );
		core.msg.debug( this.getClass().getSimpleName(), "town_info", "complete" );

		return;
	}

	/**
	 * TownPlot List
	 * @param cmd
	 */
	public void town_plots( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.player.towns.plots");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.UNCLAIM.HELP" );
			return;
		}

		TownData	td;
		String owner_name,w;
		HashMap<String,ArrayList<String>>	plots_work = new HashMap<String,ArrayList<String>>();

		owner_name = TUtils.getOwnerName( cmd.getSender(), cmd.getPlayerName(), cmd.getOptPlayer(), cmd.getOptTown() );
		if ( owner_name == null ) return;
		td = TUtils.getTownData( owner_name );
		if ( td == null ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.NOT_FOUND" );
			return;
		}

		// リスト作成
		for ( PlotData pd: core.town.plots.values() ){
			if ( pd.getOwner().equalsIgnoreCase( owner_name ) ){
				w = "(" + pd.getChunk().getX() + "," + pd.getChunk().getZ() + ")";

				if ( ! Utils.isBlank( pd.getTitle() ) ) w += "(" + pd.getTitle() + ")";
				if ( td.gethomeChunk().equal( pd.getChunk() ) ) w += "(" + core.msg.template("MESSAGE.TOWNS.HOME.HOME") + ")";

				if ( ! plots_work.containsKey( pd.getChunk().getWorldName() ) ){
					// 配列を宣言
					plots_work.put( pd.getChunk().getWorldName() , new ArrayList<String>() );
				}
				plots_work.get( pd.getChunk().getWorldName() ).add(w);
			}
		}

		w = "";
		for ( String k: plots_work.keySet() ){
			w += k + " " + core.cfg.getString( "MESSAGE.WORLD.WORLD" ) + ": "
					+ Utils.join(" ", plots_work.get(k)) + "\n";
		}

		core.msg.addMessage( cmd.getSender(), "-" + core.cfg.getString( "MESSAGE.PLOTS.PLOTS" ) + "-\n" + w );
		core.msg.debug( this.getClass().getSimpleName(), "town_plots", "complete" );

		return;

	}

	/**
	 * ホームを設定
	 * @param cmd
	 * @return
	 */
	public void town_sethome( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.towns.set.home");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.TOWNS.SET.HOME.HELP" );
			return;
		}

		TownData	td;
		PlotData	pd;
		String		owner_name;
		ChunkLoc	cl;

		cl = new ChunkLoc( cmd.getPlayer().getLocation() );
		if ( ! TUtils.isEnableWorld( cmd.getPlayer(), cl.getWorldName() ) ) return;

		owner_name = TUtils.getOwnerName( cmd.getSender(), cmd.getPlayerName(), cmd.getOptPlayer(), cmd.getOptTown() );
		if ( owner_name == null ) return;
		td = TUtils.getTownData( owner_name );
		if ( td == null ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.NOT_FOUND" );
			return;
		}

		pd = TUtils.getClaimedPlot( cmd.getSender(), cl );
		if ( pd == null ) return;
		if ( ! pd.getOwner().equalsIgnoreCase( owner_name ) ){
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.PLOTS.NOT_PERM" );
			return;
		}

		td.setHome( cmd.getPlayer().getLocation() );

		TUtils.sendOwnersMsg( td.getOwner(), cmd.getPlayer(), "MESSAGE.TOWNS.SET.HOME.DONE", Utils.formatLocation( cmd.getPlayer().getLocation() ) );
		core.msg.debug( this.getClass().getSimpleName(), "town_sethome", "complete" );
		return;
	}

	/**
	 * ホームへ移動
	 * @param cmd
	 * @return
	 */
	public void town_home( CmdOpts cmd ){
		if ( ! this.enable ) return;

		TownData td;
		String owner_name,res = null,res_to = null;
		Location loc;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(false);
		cmd.setPermission("tacsplots.player.towns.home");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.TOWNS.HOME.HELP" );
			return;
		}

		owner_name = TUtils.getOwnerName( cmd.getSender(), cmd.getPlayerName(), cmd.getOptPlayer(), cmd.getOptTown() );
		if ( owner_name == null ) return;
		td = TUtils.getTownData( owner_name );
		if ( td == null ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.TOWNS.HOME.FAIL" );
			return;
		}

		if ( td.gethome() == null ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.HOME.FAIL" );
			return;
		}

		if ( ! td.isFriend( cmd.getPlayerName() ) ){
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.TOWNS.PERM.FAIL" );
			return;
		}

		if ( cmd.getOptCommands(1) != null
				|| ( cmd.getOptCommands(0).equalsIgnoreCase( "shigen" )
						|| cmd.getOptCommands(0).equalsIgnoreCase( "sigen" )
						|| cmd.getOptCommands(0).equalsIgnoreCase( "res" )
						)
			){
			// 資源行き
			res = cmd.getOptCommands(1);

			if ( core.cfg.isDefined("spawn.toworld.shigen") ){
				for ( String w: core.cfg.getString("spawn.toworld.shigen").split(",") ){
					if ( res == null ) res = w;

					if ( res.equalsIgnoreCase( w ) ){
						res_to = res;
					}
				}
			}
			if ( res_to == null ) return;

			// 資源行き
			// ワールド有無チェック
			if ( ! TUtils.validWorld( cmd.getPlayer(), res_to ) ) return;
			loc = new Location(
					Bukkit.getWorld( res_to ),
					td.gethome().getX(),
					td.gethome().getY(),
					td.gethome().getZ()
					);
		}else{
			// Home行き
			// ワールド有無チェック
			if ( ! TUtils.validWorld( cmd.getPlayer(), td.gethome().getWorld().getName() ) ) return;
			loc = new Location(
					td.gethome().getWorld(),
					td.gethome().getX(),
					td.gethome().getY(),
					td.gethome().getZ()
					);
		}

		core.town.tp.onCommand( cmd.getPlayer(), loc );
		core.msg.debug( this.getClass().getSimpleName(), "town_home", "complete" );
		return;
	}

	/**
	 * 町の権限変更
	 * @param cmd
	 */
	public void town_setperm( CmdOpts cmd ){
		if ( ! this.enable ) return;

		// /towns set perm
		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.player.towns.set.perm");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getPlayer(), "MESSAGE.TOWNS.SET.PERM.HELP" );
			return;
		}

		//core.town.town_setperm( cmd.getPlayer(), cmd.getOptPlayer(), cmd.getOptTown(), cmd.getOptCommands(2), cmd.getOptCommands(3) );

		String owner_name;
		String perm			= cmd.getOptCommands(1);
		String setdata		= cmd.getOptCommands(2);

		PERM_TYPE	val_perm;
		Boolean		val_setdata;
		TownData td;

		owner_name = TUtils.getOwnerName( cmd.getSender(), cmd.getPlayerName(), cmd.getOptPlayer(), cmd.getOptTown() );
		if ( owner_name == null ) return;
		td = TUtils.getTownData( owner_name );
		if ( td == null ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.NOT_FOUND" );
			return;
		}

		val_perm	= TUtils.str2PermType(perm);
		val_setdata	= TUtils.str2boolean(setdata);

		if ( val_perm == null || val_setdata == null ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.SET.PERM.HELP" );
			return;
		}

		if ( val_perm.equals( PERM_TYPE.RENTAL ) ){
			// 貸し出し区画権限は設定不可
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.SET.PERM.HELP" );
			return;
		}

		td.setPerm( val_perm, val_setdata );

		TUtils.sendOwnersMsg(
				td.getOwner()
				, cmd.getSender()
				, "MESSAGE.TOWNS.SET.PERM.DONE"
				, TUtils.PermType2Str( val_perm )
				, TUtils.boolean2str( val_setdata )
				);

		core.msg.debug( this.getClass().getSimpleName(), "town_setperm", "complete" );

		return;
	}

	public void town_friend( CmdOpts cmd ){
		if ( ! this.enable ) return;

		String	friend_name;
		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);

		if ( cmd.getOptCommands(0).equalsIgnoreCase( "add" ) ){
			if ( cmd.getOptCommands(1).equalsIgnoreCase( "friend" ) ){
				cmd.setPermission("tacsplots.player.towns.add.friend");
			}else if ( cmd.getOptCommands(1).equalsIgnoreCase( "owner" ) ){
				cmd.setPermission("tacsplots.player.towns.add.owner");
			}

		}else if ( cmd.getOptCommands(0).equalsIgnoreCase( "remove" ) ){
			if ( cmd.getOptCommands(1).equalsIgnoreCase( "friend" ) ){
				cmd.setPermission("tacsplots.player.towns.add.friend");
			}else if ( cmd.getOptCommands(1).equalsIgnoreCase( "owner" ) ){
				cmd.setPermission("tacsplots.player.towns.add.owner");
			}

		}else{
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.ADDREMOVE.FRIEND.HELP" );
			return;
		}

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.ADDREMOVE.FRIEND.HELP" );
			return;
		}

		if ( cmd.getOptCommands(2) == null ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.ADDREMOVE.FRIEND.HELP" );
			return;
		}
		friend_name = cmd.getOptCommands(2);

		String owner_name;
		TownData td;
		owner_name = TUtils.getOwnerName( cmd.getSender(), cmd.getPlayerName(), cmd.getOptPlayer(), cmd.getOptTown() );
		if ( owner_name == null ) return;
		td = TUtils.getTownData( owner_name );
		if ( td == null ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.NOT_FOUND" );
			return;
		}

		if ( cmd.getOptCommands(0).equalsIgnoreCase( "add" ) ){
			if ( cmd.getOptCommands(1).equalsIgnoreCase( "friend" ) ){
				td.addFriend( friend_name, FRIEND_TYPE.FRIEND );

				TUtils.sendOwnersMsg(
						td.getOwner()
						, cmd.getSender()
						, "MESSAGE.TOWNS.ADD.FRIEND.DONE", friend_name, td.getOwner() );

			}else if ( cmd.getOptCommands(1).equalsIgnoreCase( "owner" ) ){
				td.addFriend( friend_name, FRIEND_TYPE.OWNER );

				TUtils.sendOwnersMsg(
						td.getOwner()
						, cmd.getSender()
						, "MESSAGE.TOWNS.ADD.OWNER.DONE", friend_name, td.getOwner() );
			}
		}else{
			if ( td.removeFriend( friend_name ) ){
				TUtils.sendOwnersMsg(
						td.getOwner()
						, cmd.getSender()
						, "MESSAGE.TOWNS.REMOVE.FRIEND.DONE", friend_name, td.getOwner() );
			}else{
				TUtils.sendOwnersMsg(
						td.getOwner()
						, cmd.getSender()
						, "MESSAGE.TOWNS.REMOVE.ERROR", friend_name );
			}
		}

		core.town.checkInvincibleMode( friend_name );
		return;
	}

	/**
	 * 町名を設定
	 * @param cmd
	 * @return
	 */
	public void town_title( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.player.towns.set.name");

		if ( ! cmd.checkExec( core.msg ) ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.SET.NAME.HELP" );
			return;
		}

		String owner_name;
		TownData td;
		String title;

		owner_name = TUtils.getOwnerName( cmd.getSender(), cmd.getPlayerName(), cmd.getOptPlayer(), cmd.getOptTown() );
		if ( owner_name == null ) return;
		td = TUtils.getTownData( owner_name );
		if ( td == null ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.NOT_FOUND" );
			return;
		}

		title = cmd.getOptCommands(2);
		td.setTitle( title );

		if ( td.getTitle() == null ){
			TUtils.sendOwnersMsg(
					td.getOwner()
					, cmd.getSender()
					, "MESSAGE.TOWNS.SET.NAME.DONE", core.msg.template( "MESSAGE.MAIN.NULL" ) );
		}else{
			TUtils.sendOwnersMsg(
					td.getOwner()
					, cmd.getSender()
					, "MESSAGE.TOWNS.SET.NAME.DONE", td.getTitle() );
		}
		core.msg.debug( this.getClass().getSimpleName(), "town_title", "complete" );
		return;
	}

	public void town_addpp( CmdOpts cmd ){
		if ( ! this.enable ) return;

		cmd.setPlayerCommand(true);
		cmd.setConsoleCommand(true);
		cmd.setPermission("tacsplots.admin.towns.add.pp");

		if ( ! cmd.checkExec( core.msg ) ) return;

		String		owner_name;
		TownData	td;
		int			pp;

		//       0   1  2     3
		// towns add pp owner point

		if ( cmd.getOptCommands(2) == null ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.ADD.PP.HELP" );
			return;
		}
		owner_name = cmd.getOptCommands(2);

		if ( cmd.getOptCommands(3) == null ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.ADD.PP.HELP" );
			return;
		}
		if ( ! Utils.isNumber( cmd.getOptCommands(3) ) ){
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.ADD.PP.HELP" );
			return;
		}
		pp = Utils.StringToInt( cmd.getOptCommands(3) );

		td = TUtils.getTownData( owner_name );
		if ( td == null ) {
			core.msg.addMessage( cmd.getSender(), "MESSAGE.TOWNS.NOT_FOUND" );
			return;
		}

		td.addPlotPoints(pp);
		TUtils.sendOwnersMsg(
				td.getOwner()
				, cmd.getSender()
				, "MESSAGE.TOWNS.ADD.PP.DONE" );

		core.msg.debug( this.getClass().getSimpleName(), "town_addpp", "complete:owner=" + owner_name + " pp=" + pp );
		return;
	}
}
