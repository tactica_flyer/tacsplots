package jp.d77.plugin.tacsplots.config;

import java.util.HashMap;
import java.util.Set;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tputils.Utils;
import jp.d77.plugin.tputils.config.ConfigBase;

import org.bukkit.Material;

public class ConfigObjects extends ConfigBase {
	protected static TacsPlots core = TacsPlots.plugin;

	public HashMap<Material,Boolean>	blocks	= new HashMap<Material,Boolean>();
	public HashMap<String,Boolean>	builds	= new HashMap<String,Boolean>();
	public HashMap<String,Boolean>	mobs	= new HashMap<String,Boolean>();
	public HashMap<String,Boolean>	switchs	= new HashMap<String,Boolean>();
	public HashMap<String,Boolean>	storages= new HashMap<String,Boolean>();

	public ConfigObjects(){
		super( core, "objects.yml" );
		this.blocks	= new HashMap<Material,Boolean>();;
		this.builds		= new HashMap<String,Boolean>();;
		this.mobs		= new HashMap<String,Boolean>();;
		this.switchs	= new HashMap<String,Boolean>();;
		this.storages	= new HashMap<String,Boolean>();;
	}

	@Override
	public void setDefaults(){
		super.setDefaultStrings( "build_blocks"
				,"WOOD"
				,"GLASS"
				,"LAPIS_BLOCK"
				,"DISPENSER"
				,"SANDSTONE"
				,"NOTE_BLOCK"
				,"BED_BLOCK"
				,"POWERED_RAIL"
				,"DETECTOR_RAIL"
				,"PISTON_BASE"
				,"WOOL"
				,"GOLD_BLOCK"
				,"IRON_BLOCK"
				,"DOUBLE_STEP"
				,"STEP"
				,"BRICK"
				,"BOOKSHELF"
				,"OBSIDIAN"
				,"WOOD_STAIRS"
				,"CHEST"
				,"DIAMOND_BLOCK"
				,"WORKBENCH"
				,"FURNACE"
				,"SIGN_POST"
				,"WOODEN_DOOR"
				,"LADDER"
				,"RAILS"
				,"COBBLESTONE_STAIRS"
				,"WALL_SIGN"
				,"LEVER"
				,"STONE_PLATE"
				,"IRON_DOOR_BLOCK"
				,"WOOD_PLATE"
				,"STONE_BUTTON"
				,"JUKEBOX"
				,"FENCE"
				,"GLOWSTONE"
				,"JACK_O_LANTERN"
				,"STAINED_GLASS"
				,"TRAP_DOOR"
				,"SMOOTH_BRICK"
				,"HUGE_MUSHROOM_1"
				,"HUGE_MUSHROOM_2"
				,"IRON_FENCE"
				,"THIN_GLASS"
				,"FENCE_GATE"
				,"BRICK_STAIRS"
				,"SMOOTH_STAIRS"
				,"ENCHANTMENT_TABLE"
				,"BREWING_STAND"
				,"CAULDRON"
				,"ENDER_STONE"
				,"REDSTONE_LAMP_OFF"
				,"REDSTONE_LAMP_ON"
				,"WOOD_DOUBLE_STEP"
				,"WOOD_STEP"
				,"SANDSTONE_STAIRS"
				,"ENDER_CHEST"
				,"EMERALD_BLOCK"
				,"SPRUCE_WOOD_STAIRS"
				,"BIRCH_WOOD_STAIRS"
				,"JUNGLE_WOOD_STAIRS"
				,"COBBLE_WALL"
				,"FLOWER_POT"
				,"ANVIL"
				,"TRAPPED_CHEST"
				,"GOLD_PLATE"
				,"IRON_PLATE"
				,"DAYLIGHT_DETECTOR"
				,"REDSTONE_BLOCK"
				,"HOPPER"
				,"HOPPER"
				,"QUARTZ_BLOCK"
				,"QUARTZ_STAIRS"
				,"ACTIVATOR_RAIL"
				,"DROPPER"
				,"STAINED_CLAY"
				,"STAINED_GLASS_PANE"
				,"ACACIA_STAIRS"
				,"DARK_OAK_STAIRS"
//				,"SLIME_BLOCK"
				,"COAL_BLOCK"
//				,"RED_SANDSTONE_STAIRS"
//				,"DOUBLE_STONE_SLAB2"
//				,"STONE_SLAB2"
				,"SIGN"
				,"WOOD_DOOR"
				,"IRON_DOOR"
				);
		super.setDefaultStrings( "build_entity"	,"PAINTING","ITEM_FRAME" );
		super.setDefaultStrings( "mob_entity"
				,"CREEPER"
				,"SKELETON"
				,"SPIDER"
				,"GIANT"
				,"ZOMBIE"
				,"SLIME"
				,"GHAST"
				,"PIG_ZOMBIE"
				,"ENDERMAN"
				,"CAVE_SPIDER"
				,"SILVERFISH"
				,"BLAZE"
				,"MAGMA_CUBE"
				,"ENDER_DRAGON"
				,"WITHER"
				,"WITCH"
				,"ENDERMITE"
				,"GUARDIAN"
				);

		super.setDefaultStrings( "switch_block"
				,"WOODEN_DOOR"
				,"WOOD_BUTTON"
				,"TRIPWIRE"
				,"LEVER"
				,"TRIPWIRE_HOOK"
				,"WOODEN_DOOR"
				,"STONE_PLATE"
				,"FURNACE"
				);

		super.setDefaultStrings( "storage_block"
				,"CHEST"
				,"TRAPPED_CHEST"
				,"HOPPER"
				,"DROPPER"
				,"BIBLIOCRAFT_BIBLIO*"
				,"RAILCRAFT_*"
				,"GARDENSTUFF_*"
				,"BETTERSTORAGE_*"
				);

	}

	@Override
	public void save(){

		core.msg.debug( this.getClass().getSimpleName(), "save", "build=" + this.getBuildItems() );
		core.msg.debug( this.getClass().getSimpleName(), "save", "mobs=" + this.getMobs() );
		core.msg.debug( this.getClass().getSimpleName(), "save", "switch=" + this.getSwitchItems() );
		core.msg.debug( this.getClass().getSimpleName(), "save", "storage=" + this.getStorages() );

		super.clear();

		this.setList( "build_blocks",	this.MaterialSet2Strings( this.blocks.keySet() ) );
		this.setList( "build_entity",	Utils.Set2Strings(this.builds.keySet()) );
		this.setList( "mob_entity",		Utils.Set2Strings(this.mobs.keySet()) );
		this.setList( "switch_block",	Utils.Set2Strings(this.switchs.keySet()) );
		this.setList( "storage_block",	Utils.Set2Strings(this.storages.keySet()) );

		super.save();
	}

	public String[] MaterialSet2Strings( Set<Material> a ){
		return a.toArray( new String[0] );
	}

	@Override
	public void load(){
		super.load();

		this.builds		= new HashMap<String,Boolean>();
		this.mobs		= new HashMap<String,Boolean>();
		this.switchs	= new HashMap<String,Boolean>();
		this.storages	= new HashMap<String,Boolean>();

		for ( String s: this.getStringList( "build_blocks" ) ){
			this.addBuildBlocks( s, false );
		}

		for ( String s: this.getStringList( "build_entity" ) ){
			this.addBuildItem( s, false );
		}
		for ( String s: this.getStringList( "mob_entity" ) ){
			this.addMob( s, false );
		}
		for ( String s: this.getStringList( "switch_block" ) ){
			this.addSwitchItem( s, false );
		}
		for ( String s: this.getStringList( "storage_block" ) ){
			this.addStorage( s, false );
		}
	}

	public String getBuildItems(){
		return Utils.join(",", this.builds.keySet() );
	}

	public String getMobs(){
		return Utils.join(",", this.mobs.keySet() );
	}

	public String getSwitchItems(){
		return Utils.join(",", this.switchs.keySet() );
	}

	public String getStorages(){
		return Utils.join(",", this.storages.keySet() );
	}

	public void addBuildBlocks( String s, boolean save ){
		core.msg.debug( this.getClass().getSimpleName(), "addBuildBlocks", s);

		Material m;
		if ( Material.matchMaterial( s ) != null ){
			m = Material.matchMaterial( s );
		}else{
			core.msg.error( "not found material: " + s );
			return;
		}

		this.blocks.put( m, true );
		if ( save ) this.save();
	}

	public void addBuildItem( String s, boolean save ){
		core.msg.debug( this.getClass().getSimpleName(), "addBuildItem", s);
		this.builds.put( s.toUpperCase(), true );
		if ( save ) this.save();
	}

	public void addMob( String s, boolean save ){
		core.msg.debug( this.getClass().getSimpleName(), "addMob", s);
		this.mobs.put( s.toUpperCase(), true );
		if ( save ) this.save();
	}

	public void addSwitchItem( String s, boolean save ){
		core.msg.debug( this.getClass().getSimpleName(), "addSwitchItem", s);
		this.switchs.put( s.toUpperCase(), true );
		if ( save ) this.save();
	}

	public void addStorage( String s, boolean save ){
		core.msg.debug( this.getClass().getSimpleName(), "addStorage", s);
		this.storages.put( s.toUpperCase(), true );
		if ( save ) this.save();
	}

	public void removeBuildBlocks( String s ){
		core.msg.debug( this.getClass().getSimpleName(), "removeBuildBlocks", s);

		Material m;
		if ( Material.matchMaterial( s ) != null ){
			m = Material.matchMaterial( s );
		}else{
			core.msg.error( "not found material: " + s );
			return;
		}

		if ( this.blocks.containsKey( m ) ) this.blocks.remove( m );
		this.save();
	}

	public void removeBuildItem( String s ){
		core.msg.debug( this.getClass().getSimpleName(), "removeBuildItem", s);
		if ( this.builds.containsKey( s.toUpperCase() ) ) this.builds.remove( s.toUpperCase() );
		this.save();
	}

	public void removeMob( String s ){
		core.msg.debug( this.getClass().getSimpleName(), "removeMob", s);
		if ( this.mobs.containsKey( s.toUpperCase() ) ) this.mobs.remove( s.toUpperCase() );
		this.save();
	}

	public void removeSwitchItem( String s ){
		core.msg.debug( this.getClass().getSimpleName(), "removeSwitchItem", s);
		if ( this.switchs.containsKey( s.toUpperCase() ) ) this.switchs.remove( s.toUpperCase() );
		this.save();
	}

	public void removeStorage( String s ){
		core.msg.debug( this.getClass().getSimpleName(), "removeStorage", s);
		if ( this.storages.containsKey( s.toUpperCase() ) ) this.storages.remove( s.toUpperCase() );
		this.save();
	}

	private boolean matchString( String def, String val ){
		// 01234
		// abcd*
		if ( def.substring( def.length() - 1 ).equals("*") ){
			// ワイルドカード有り
			if ( def.length() - 2 > val.length() ) return false;
			if ( def.substring( 0, def.length() - 2 ).equalsIgnoreCase( val.substring( 0, def.length() - 2 ) ) ){
				return true;
			}
		}else{
			return def.equalsIgnoreCase( val );
		}
		return false;
	}

	public boolean isSwitchBlock( String e ){
		for ( String s: this.switchs.keySet() ){
			if ( this.matchString( s, e) ) return true;
		}
		return false;
	}

	public boolean isStorage( String e ){
		for ( String s: this.storages.keySet() ){
			if ( this.matchString( s, e) ) return true;
		}
		return false;
	}
}
