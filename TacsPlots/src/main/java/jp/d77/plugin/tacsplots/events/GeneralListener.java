package jp.d77.plugin.tacsplots.events;

import jp.d77.plugin.tacsplots.TacsPlots;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityBreakDoorEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;

public class GeneralListener implements Listener{
	protected static TacsPlots	core = TacsPlots.plugin;
	private boolean enable = false;

	public boolean init(){
		enable = true;
		return true;
	}

	public boolean destroy(){
		enable = false;
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// ブロック関連のイベント (org.bukkit.event.block)
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * ブロックを破壊
	 * @param event
	 */
	@EventHandler
	public void onBlockEvent( BlockBreakEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "BlockBreakEvent" , event ) ) event.setCancelled(true);
			core.town.events.checkPlotExp( "BlockBreakEvent" , event );
		}

	}

	/**
	 * ブロックを設置
	 * @param event
	 */
	@EventHandler
	public void onBlockEvent( BlockPlaceEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "BlockPlaceEvent" , event ) ) event.setCancelled(true);
			core.town.events.checkPlotExp( "BlockPlaceEvent" , event );
		}
	}

	/**
	 * 着火
	 * @param event
	 */
	@EventHandler
	public void onBlockEvent( BlockIgniteEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 着火
			if ( core.town.events.isFireCancel( "BlockIgniteEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * 延焼
	 * @param event
	 */
	@EventHandler
	public void onBlockEvent( BlockSpreadEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			if ( core.town.events.isFireCancel( "BlockSpreadEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * 消失
	 * @param event
	 */
	@EventHandler
	public void onBlockEvent( BlockBurnEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			if ( core.town.events.isFireCancel( "BlockBurnEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * レッドストーンの状態が変化する時
	 * @param event
	 */
	@EventHandler
	public void onBlockEvent( BlockRedstoneEvent event ){
		if ( ! enable ) return;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// 壁掛けアイテム関連のイベント (org.bukkit.event.hanging)
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * 壁掛け破壊
	 * @param event
	 */
	@EventHandler
	public void onHangingEvent( HangingBreakByEntityEvent event ){
		if ( ! enable ) return;

		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "HangingBreakByEntityEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * 壁掛け設置
	 * @param event
	 */
	@EventHandler
	public void onHangingEvent( HangingPlaceEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "HangingPlaceEvent" , event ) ) event.setCancelled(true);
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// プレイヤー関連のイベント (org.bukkit.event.player)
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * ログインしたとき
	 * @param event
	 */
	@EventHandler
	public void onPlayerEvent( PlayerJoinEvent event ){
		if ( ! enable ) return;

		if ( core.town != null ) core.town.checkInvincibleMode( event.getPlayer() );
	}

	/**
	 * ログアウトしたとき
	 * @param event
	 */
	@EventHandler
	public void onPlayerEvent( PlayerQuitEvent event ){
		if ( ! enable ) return;
	}

	/**
	 * しゃべった時
	 * @param event
	 */
	@EventHandler
	public void onPlayerEvent( AsyncPlayerChatEvent event ){
		if ( ! enable ) return;
	}

	/**
	 * プレイヤーが移動
	 * @param event
	 */
	@EventHandler
	public void onPlayerEvent( PlayerMoveEvent event ){
		if ( ! enable ) return;

		if ( core.town != null ){
			if ( core.town.tp != null ) core.town.tp.PlayerMove(event);
			//if ( core.town.events != null ) core.town.events.MoveEvents( event.getEventName() , event );
		}
	}

	/**
	 * 壁掛けアイテム回転/設置
	 * @param event
	 */
	@EventHandler
	public void onPlayerEvent( PlayerInteractEntityEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ){
			if ( core.town.tp != null ) core.town.tp.OtherMove(event.getPlayer());
		}

		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "PlayerInteractEntityEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * バケツの中身をぶちまけた
	 * @param event
	 */
	@EventHandler
	public void onPlayerEvent( PlayerBucketEmptyEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "PlayerBucketEmptyEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * バケツの中身を満たした
	 * @param event
	 */
	@EventHandler
	public void onPlayerEvent( PlayerBucketFillEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "PlayerBucketFillEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * クリック時・感圧版・トリップワイヤー
	 * @param event
	 */
	@EventHandler
	public void onPlayerEvent( PlayerInteractEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// スイッチ操作
			if ( core.town.events.isSwitchCancel( "PlayerInteractEvent" , event ) ) event.setCancelled(true);
			// Storage
			//if ( core.town.events.isStorageCancel( "PlayerInteractEvent" , event ) ) event.setCancelled(true);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------
	// エンティティ関連のイベント (org.bukkit.event.entity)
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 壁掛けアイテム撤去/エンティティへダメージを与える
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( EntityDamageByEntityEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			if ( core.town.events.isBuildCancel( "EntityDamageByEntityEvent" , event ) ) {
				// 建築/破壊
				event.setCancelled(true);
			}
			if ( core.town.events.isPvPCancel( "EntityDamageByEntityEvent" , event ) ) {
				// PvP
				event.setCancelled(true);
			}
			if ( event.getDamager().getType().equals( EntityType.PLAYER ) ){
				if ( core.town.isInvincible( ((Player)event.getDamager()).getName() ) ) event.setCancelled(true);
			}
		}
	}

	/**
	 * プレイヤー以外がブロックを変更
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( EntityChangeBlockEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "EntityChangeBlockEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * ゾンビがドアを壊す時
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( EntityBreakDoorEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "EntityBreakDoorEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * ブロック爆破
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( EntityExplodeEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 爆破
			if ( core.town.events.isExplosionCancel( "EntityExplodeEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * 生物が発生
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( CreatureSpawnEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// spawn
			if ( core.town.events.isSpawnCancel( "CreatureSpawnEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * 生物が発生(1.7では動かない)
	 * @param event
	 */
/*
	@EventHandler
	public void onEntityEvent( EntitySpawnEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// spawn
			if ( core.town.events.isSpawnCancel( "EntitySpawnEvent" , event ) ) event.setCancelled(true);
		}
	}
*/
	/**
	 * ダメージを受ける
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( EntityDamageEvent event ){
		if ( ! enable ) return;

		if ( event.getEntity().getType().equals( EntityType.PLAYER ) ){
			if ( core.town != null ) {
				if ( core.town.isInvincible( ((Player)event.getEntity()).getName() ) ) {
					event.setCancelled(true);
					return;
				}
				if ( core.town.tp != null ) core.town.tp.EntityDamage(event);
			}
		}
	}

	/**
	 * エンティティに着火
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( EntityCombustEvent event ){
		if ( ! enable ) return;
	}

	/**
	 * スプラッシュポーションが割れる時
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( PotionSplashEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// PvP
			if ( core.town.events.isPvPCancel( "PotionSplashEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * エンティティからターゲットされた
	 * @param event
	 */
	@EventHandler
	public void onEntityEvent( EntityTargetLivingEntityEvent event ){
		if ( ! enable ) return;

		if ( event.getTarget() == null ) return;
		if ( event.getTarget().getType() == null ) return;

		if ( event.getTarget().getType().equals( EntityType.PLAYER ) ){
			if ( core.town != null ) {
				if ( core.town.isInvincible( ((Player)event.getTarget()).getName() ) ) {
					event.setCancelled(true);
				}
			}
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// インベントリ関連のイベント (org.bukkit.event.inventory)
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * インベントリをクリック
	 * @param event
	 */
	@EventHandler
	public void onInventoryEvent( InventoryClickEvent event ){
		if ( ! enable ) return;
	}

	/**
	 * インベントリをオープン
	 * @param event
	 */
	@EventHandler
	public void onInventoryEvent( InventoryOpenEvent event ){
		if ( ! enable ) return;

		if ( core.town != null ) {
			// Storage
			if ( core.town.events.isStorageCancel( "PlayerInteractEvent" , event ) ) event.setCancelled(true);
		}

	}

	// ---------------------------------------------------------------------------------------------------------------
	// 乗り物関連のイベント (org.bukkit.event.vehicle)
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 乗り物ダメージ
	 * @param event
	 */
	@EventHandler
	public void onVehicleEvent( VehicleDamageEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "VehicleDamageEvent" , event ) ) event.setCancelled(true);
		}
	}

	/**
	 * 乗り物消滅
	 * @param event
	 */
	@EventHandler
	public void onVehicleEvent( VehicleDestroyEvent event ){
		if ( ! enable ) return;
		if ( core.town != null ) {
			// 建築/破壊
			if ( core.town.events.isBuildCancel( "VehicleDestroyEvent" , event ) ) event.setCancelled(true);
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// ワールド関連のイベント (org.bukkit.event.world)
	// ---------------------------------------------------------------------------------------------------------------
	/*
	@EventHandler
	public void onWorldEvent( WorldSaveEvent event ){
		if ( ! enable ) return;
		core.getLogger().info( "[debug]save " + event.getWorld().getName() );
	}
	*/

}
