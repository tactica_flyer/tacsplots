package jp.d77.plugin.tacsplots.datas;

import java.util.HashMap;
import java.util.Set;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.core.TUtils;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tputils.kvs.KeyValue;

public class PermData {
	protected static TacsPlots core = TacsPlots.plugin;

	private HashMap<PERM_TYPE,Boolean> perm = new HashMap<PERM_TYPE,Boolean>();

	public PermData(){
	}

	public PermData( PermData p ){
		for ( PERM_TYPE type: p.getKeys() ){
			this.set( type, p.get(type) );
		}
	}

	public void save( KeyValue kvs, String root ){
		for( PERM_TYPE pt: this.perm.keySet() ){
			kvs.setBoolean( root + "." + pt.toString() , this.perm.get(pt) );
		}
	}

	public void load( KeyValue kvs, String root ){
		Boolean b;

		this.perm = new HashMap<PERM_TYPE,Boolean>();
		for( PERM_TYPE pt: PERM_TYPE.values() ){
			b = kvs.getBoolean( root + "." + pt.toString() );
			if ( b != null ) this.perm.put( pt, b);
		}
	}

	public void set( PERM_TYPE p, Boolean v ){
		this.perm.put( p, v );
	}

	public Boolean get( PERM_TYPE p ){
		if ( ! this.perm.containsKey( p ) ) return null;
		return this.perm.get( p );
	}

	public Set<PERM_TYPE> getKeys(){
		return this.perm.keySet();
	}

	public static PermData over( PermData base, PermData add ){
		PermData ret = new PermData( base );

		for ( PERM_TYPE type: add.getKeys() ){
			if ( add.get(type) != null ) ret.set( type, add.get(type) );
		}

		return ret;
	}

	public String info(){
		return this.info( false );
	}

	public String info( boolean rental_view ){
		String open;
		String ret;

		if ( this.perm.get( PERM_TYPE.OPEN ) ) open = core.cfg.getString( "MESSAGE.PERM.TRUE" );
		else open = core.cfg.getString( "MESSAGE.PERM.FALSE" );

		ret = core.msg.template( "MESSAGE.PERM.INFO.VIEW"
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.OPEN ) ) + "..." + open
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.BUILD ) )
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.SWITCH ) )
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.FIRE ) )
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.PVP ) )
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.ANIMAL ) )
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.MOB ) )
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.EXPLOSION ) )
				, TUtils.boolean2str( this.perm.get( PERM_TYPE.STORAGE ) )
				);

		if ( rental_view ){
			ret += core.msg.template( "MESSAGE.PERM.INFO.VIEW_RENTAL"
					, TUtils.boolean2str( this.perm.get( PERM_TYPE.RENTAL ) )
					);
		}
		return ret;
	}

	public String simple_info(){
		return this.simple_info( false );
	}

	public String simple_info( boolean rental_view ){
		String ret = "";

		ret += simple_perm( PERM_TYPE.OPEN, "O " );
		ret += simple_perm( PERM_TYPE.BUILD, "B " );
		ret += simple_perm( PERM_TYPE.FIRE, "F " );
		ret += simple_perm( PERM_TYPE.PVP, "P " );
		ret += simple_perm( PERM_TYPE.SWITCH, "Sw " );
		ret += simple_perm( PERM_TYPE.STORAGE, "St " );
		ret += simple_perm( PERM_TYPE.ANIMAL, "A " );
		ret += simple_perm( PERM_TYPE.MOB, "M " );
		ret += simple_perm( PERM_TYPE.EXPLOSION, "E " );
		if ( rental_view ){
			ret += simple_perm( PERM_TYPE.RENTAL, "R " );
		}
		ret = core.msg.template( "MESSAGE.PERM.INFO.SIMPLEVIEW", ret );

		return ret;
	}

	public String simple_perm( PERM_TYPE pt, String s ){
		boolean b = false;

		if ( this.perm.containsKey( pt  ) ){
			if ( this.perm.get( pt ) ) b = true;
		}

		if ( b ) {
			return s;
		}else{
			return "&7" + s + "&r";
		}
	}
}
