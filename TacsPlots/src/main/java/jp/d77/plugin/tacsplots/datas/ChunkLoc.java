package jp.d77.plugin.tacsplots.datas;

import jp.d77.plugin.tputils.kvs.KeyValue;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class ChunkLoc {
	private String				mWorldName;
	private int 				mX,mZ;

	public ChunkLoc( String world_name, int x, int z ){
		this.mWorldName = world_name;
		this.mX = x;
		this.mZ = z;
	}

	public ChunkLoc( Player p ){
		this( p.getLocation() );
	}

	public ChunkLoc( Location loc ){
		this( loc.getWorld().getName(), loc.getChunk().getX(), loc.getChunk().getZ() );
	}

	public ChunkLoc( ChunkLoc chunk ){
		this( chunk.getWorldName(), chunk.getX(), chunk.getZ() );
	}

	public ChunkLoc(){
	}

	public boolean equal( ChunkLoc chunk ){
		if ( chunk.getWorldName().equals( this.mWorldName )
				&& chunk.getX() == this.mX
				&& chunk.getZ() == this.mZ
				){
			return true;
		}
		return false;
	}

	public String toString(){
		return "world=" + this.mWorldName + " x=" + this.mX + " z=" + this.mZ;
	}

	public String toKey(){
		return this.mWorldName + "_" + this.mX + "_" + this.mZ;
	}

	public String toFixKey(){
		return this.mWorldName + "_" + String.format( "%05d",this.mX) + "_" + String.format( "%05d",this.mZ);
	}

	public void save( KeyValue kvs, String root ){
		kvs.setString( root + ".world", this.getWorldName() );
		kvs.setInteger( root + ".x", this.getX() );
		kvs.setInteger( root + ".z", this.getZ() );
	}

	public boolean load( KeyValue kvs, String root ){
		if ( kvs.getString( root + ".world" ) == null ) return false;
		if ( kvs.getString( root + ".x" ) == null ) return false;
		if ( kvs.getString( root + ".z" ) == null ) return false;
		this.mWorldName = kvs.getString( root + ".world" );
		this.mX = kvs.getInteger( root + ".x" );
		this.mZ = kvs.getInteger( root + ".z" );
		return true;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Gets
	// ---------------------------------------------------------------------------------------------------------------

	public String getWorldName(){
		return this.mWorldName;
	}
	public int getX(){
		return this.mX;
	}
	public int getZ(){
		return this.mZ;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Sets
	// ---------------------------------------------------------------------------------------------------------------
	public void setWorldName( String world_name ){
		this.mWorldName = world_name;
	}
	public void setX( int x ){
		this.mX = x;
	}
	public void setZ( int z ){
		this.mZ = z;
	}

	public void addX( int x ){
		this.mX += x;
	}
	public void addZ( int z ){
		this.mZ += z;
	}
}
