package jp.d77.plugin.tacsplots.datas;

import jp.d77.plugin.tacsplots.core.TUtils;
import jp.d77.plugin.tputils.kvs.KeyValue;


public class PlotData extends TownData {

	protected ChunkLoc				chunk;
	int	plotexp = 0;

	public PlotData( ChunkLoc chunk, String owner_name ){
		super( owner_name );
		this.setChunk( chunk );
		this.plotexp = 0;
	}

	public PlotData( KeyValue kvs, String root ){
		super();
		this.load(kvs, root);
	}

	public void save( KeyValue kvs, String root ){
		core.msg.debug( this.getClass().getSimpleName(), "save", "root=" + root );

		kvs.setInteger( root + ".plotexp", this.plotexp);

		this.chunk.save(kvs, root);
		super.save(kvs, root);
	}

	public void load( KeyValue kvs, String root ){
		core.msg.debug( this.getClass().getSimpleName(), "load", "root=" + root );

		super.load(kvs, root);
		this.chunk = new ChunkLoc();
		this.chunk.load(kvs, root);
		this.plotexp = kvs.getInteger( root + ".plotexp" );
	}

	public ChunkLoc getChunk(){
		return this.chunk;
	}

	public String info(){
		PermData pd = TUtils.getPlotPerm( this.chunk );

		return core.msg.template( "MESSAGE.PLOTS.INFO.VIEW"
				, this.chunk.getWorldName() + " (x,z)=(" + this.chunk.getX() + "," + this.chunk.getZ() + ")"
				, this.getOwner()
				, this.getPlotPoints() + "pp (" + this.getPlotExp() + "pe)"
				, this.friends.info()

				) + "\n"
				+ pd.info( true );
	}

	public String simple_info(){
		PermData pd = TUtils.getPlotPerm( this.chunk );

		return core.msg.template( "MESSAGE.PLOTS.INFO.SIMPLEVIEW"
				, this.chunk.getWorldName() + " " + this.chunk.getX() + "," + this.chunk.getZ()
				, this.getOwner()
				, this.getPlotPoints() + "pp (" + this.getPlotExp() + "pe)"
				) + " " + pd.simple_info( true );
	}

	public int getPlotExp(){
		return this.plotexp;
	}

	public int getPlotPoints(){
		if ( core.cfg.getInteger( "claim.pp.plot_bonus.increase_5" ) <= this.getPlotExp() ){
			return 5;
		}else if ( core.cfg.getInteger( "claim.pp.plot_bonus.increase_4" ) <= this.getPlotExp() ){
			return 4;
		}else if ( core.cfg.getInteger( "claim.pp.plot_bonus.increase_3" ) <= this.getPlotExp() ){
			return 3;
		}else if ( core.cfg.getInteger( "claim.pp.plot_bonus.increase_2" ) <= this.getPlotExp() ){
			return 2;
		}else if ( core.cfg.getInteger( "claim.pp.plot_bonus.increase_1" ) <= this.getPlotExp() ){
			return 1;
		}
		return 0;
	}

	public void setChunk( ChunkLoc chunk ){
		this.chunk = new ChunkLoc( chunk );
	}

	public void addPlotExp( int pe ){
		core.msg.debug( this.getClass().getSimpleName(), "addPlotExp", "Chunk=" + this.chunk.toString() + " pe=" + pe );

		this.plotexp += pe;
		if ( this.plotexp <= core.cfg.getInteger("claim.pp.minexp") ) this.plotexp = core.cfg.getInteger("claim.pp.minexp");
		if ( this.plotexp >= core.cfg.getInteger("claim.pp.maxexp") ) this.plotexp = core.cfg.getInteger("claim.pp.maxexp");

	}

	public void setPlotExp( int pe ){
		this.plotexp = pe;
	}

}
