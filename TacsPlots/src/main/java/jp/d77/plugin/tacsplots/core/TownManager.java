package jp.d77.plugin.tacsplots.core;

import java.util.ArrayList;
import java.util.HashMap;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.Consts.PERM_TYPE;
import jp.d77.plugin.tacsplots.datas.PermData;
import jp.d77.plugin.tacsplots.datas.PlotData;
import jp.d77.plugin.tacsplots.datas.TownData;
import jp.d77.plugin.tacsplots.datas.WorldData;
import jp.d77.plugin.tacsplots.events.GeneralRunner;
import jp.d77.plugin.tacsplots.events.GeneralRunner.RUNNER_TYPE;
import jp.d77.plugin.tputils.Utils;
import jp.d77.plugin.tputils.kvs.KVStoreImpl.KVSModeType;
import jp.d77.plugin.tputils.kvs.KeyValue;
import jp.d77.plugin.tputils.kvs.KeyValue.DB_TYPE;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TownManager {
	protected static TacsPlots core = TacsPlots.plugin;

	protected PermData					def_perm;

	protected ArrayList<WorldData>		worlds		= new ArrayList<WorldData>();
	protected ArrayList<TownData>		towns		= new ArrayList<TownData>();
	protected HashMap<String,PlotData>	plots		= new HashMap<String,PlotData>();
	protected HashMap<String,Boolean>	invincible  = new HashMap<String,Boolean>();

	public TCmdWorld		cmdWorld;
	public TCmdTowns		cmdTowns;
	public TCmdPlots		cmdPlots;
	public TEvents			events;
	public TUtils			utils;
	public TTp				tp;
	public KeyValue			plot_data;
	private	GeneralRunner	timeEvent;

	public TownManager(){
		this.cmdWorld	= new TCmdWorld();
		this.cmdTowns	= new TCmdTowns();
		this.cmdPlots	= new TCmdPlots();
		this.events		= new TEvents();
		this.utils		= new TUtils();
		this.tp			= new TTp();
		this.plot_data	= new KeyValue( DB_TYPE.YAML, "plots.yml", "plots_data" );
	}

	public boolean init(){
		this.worlds	= new ArrayList<WorldData>();
		this.towns	= new ArrayList<TownData>();
		this.plots	= new HashMap<String,PlotData>();

		this.plot_data.init( core );
		this.plot_data.setMode( KVSModeType.AUTO_BACKUP , true);

		this.events.init();
		this.cmdWorld.init();
		this.cmdTowns.init();
		this.cmdPlots.init();
		this.tp.init();

		this.timeEvent		= new GeneralRunner( RUNNER_TYPE.NAV_MSG );
		this.timeEvent.init();
		this.timeEvent.runTaskTimer( core, 20, 20 );

		//this.checkInvincibleMode();

		return true;
	}

	public boolean destroy(){
		this.tp.destroy();

		this.timeEvent.destroy();
		this.timeEvent.cancel();

		this.cmdWorld.destroy();
		this.cmdTowns.destroy();
		this.cmdPlots.destroy();

		this.events.destroy();

		this.plot_data.destroy();

		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	public void save(){

		this.plot_data.clear();

		for ( WorldData wd: this.worlds ){
			wd.save( this.plot_data, "worlds." + wd.getTitle() );
		}

		for ( TownData td: this.towns ){
			td.save( this.plot_data, "towns." + td.getOwner() );
		}

		for ( String ps: this.plots.keySet() ){
			this.plots.get(ps).save( this.plot_data, "plots." + ps );
		}

		//Utils.mkdir(path);
		this.plot_data.save();

		//core.msg.debug( this.getClass().getSimpleName(), "save", "root=" + root );
	}

	public void load(){
		this.def_perm = new PermData();

		this.def_perm.set( PERM_TYPE.OPEN,		core.cfg.data().getBoolean("world_default.open",		false ) );
		this.def_perm.set( PERM_TYPE.PVP,		core.cfg.data().getBoolean("world_default.pvp",	false ) );
		this.def_perm.set( PERM_TYPE.BUILD,		core.cfg.data().getBoolean("world_default.build",	false ) );
		this.def_perm.set( PERM_TYPE.FIRE,		core.cfg.data().getBoolean("world_default.fire",		false ) );
		this.def_perm.set( PERM_TYPE.SWITCH,	core.cfg.data().getBoolean("world_default.switch",	true  ) );
		this.def_perm.set( PERM_TYPE.MOB,		core.cfg.data().getBoolean("world_default.mob",		true) );
		this.def_perm.set( PERM_TYPE.ANIMAL,	core.cfg.data().getBoolean("world_default.animal",	true) );
		this.def_perm.set( PERM_TYPE.EXPLOSION,	core.cfg.data().getBoolean("world_default.explosion",false) );
		this.def_perm.set( PERM_TYPE.STORAGE,	core.cfg.data().getBoolean("world_default.storage",false) );
		this.def_perm.set( PERM_TYPE.RENTAL,	core.cfg.data().getBoolean("world_default.rental",false) );

		this.worlds	= new ArrayList<WorldData>();
		this.towns	= new ArrayList<TownData>();
		this.plots	= new HashMap<String,PlotData>();

		this.plot_data.load();

		for ( String s: this.plot_data.getKeys( "worlds") ){
			this.worlds.add( new WorldData( this.plot_data, "worlds." + s ) );
		}

		for ( String s: this.plot_data.getKeys( "towns") ){
			this.towns.add( new TownData( this.plot_data, "towns." + s ) );
		}

		for ( String s: this.plot_data.getKeys( "plots") ){
			this.plots.put( s, new PlotData( this.plot_data, "plots." + s ) );
		}

		this.checkInvincibleMode();
	}

	public PlotData[] getPlotDatas(){

		return (PlotData[]) this.plots.values().toArray( new PlotData[0] );
	}

	public TownData[] getTownDatas(){
		return (TownData[]) this.towns.toArray( new TownData[0] );
	}

	// ---------------------------------------------------------------------------------------------------------------
	// 無敵モード
	// ---------------------------------------------------------------------------------------------------------------
	public void checkInvincibleMode(){
		this.invincible  = new HashMap<String,Boolean>();

		for ( Player player : Bukkit.getOnlinePlayers() ) {
			this.checkInvincibleMode( player );
		}
	}

	//
	public void checkInvincibleMode( String player_name ){
		this.checkInvincibleMode( Utils.getExistPlayer( player_name ) );
	}

	public void checkInvincibleMode( Player player ){
		if ( player == null ) return;

		boolean mode = false;

		if ( TUtils.getTownData( player.getName() ) == null ){
			// 町データが無い
			mode = true;	// 無敵モードON
		}

		if ( mode ){
			// フレンド登録確認
			for ( TownData td: this.towns ){
				if ( td.getFriendData( player.getName() ) != null ){
					// フレンド登録されてる
					mode = false;	// 無敵モードOFF
				}
			}
			for ( PlotData pd: this.plots.values() ){
				if ( pd.getFriendData( player.getName() ) != null ){
					// フレンド登録されてる
					mode = false;	// 無敵モードOFF
				}
			}
		}

		this.setInvincible( player, mode );
	}

	public void setInvincible( Player p, boolean b ){
		if ( b ){
			// 無敵モード設定
			if ( this.invincible.containsKey( p.getName().toLowerCase() ) ){
				// 何もしない
			}else{
				// 設定
				this.invincible.put( p.getName().toLowerCase(), true );
				core.msg.addMessage(p, "MESSAGE.PLAYER.SET_INVINCIBLE");
			}
			core.msg.debug( this.getClass().getSimpleName() , "setInvincible", p.getName() + " is invincible." );
		}else{
			// 無敵モード解除
			if ( this.invincible.containsKey( p.getName().toLowerCase() ) ){
				this.invincible.remove( p.getName().toLowerCase() );
				core.msg.addMessage(p, "MESSAGE.PLAYER.RESET_INVINCIBLE");
			}else{
				// 何もしない
			}
			core.msg.debug( this.getClass().getSimpleName() , "setInvincible", p.getName() + " is not invincible." );
		}
	}

	public boolean isInvincible( String player_name ){
		if ( this.invincible.containsKey( player_name.toLowerCase() ) ) {
			core.msg.debug( this.getClass().getSimpleName() , "setInvincible", player_name + " is invincible." );
			return true;
		}
		return false;
	}
}
