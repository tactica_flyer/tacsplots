package jp.d77.plugin.tacsplots.datas;

import java.util.UUID;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tputils.Utils;
import jp.d77.plugin.tputils.kvs.KeyValue;

import org.bukkit.entity.Player;

public class PlayerData{
	protected static TacsPlots core = TacsPlots.plugin;

	public UUID		uuid = null;
	public String	name = null;

	public PlayerData( Player p ){
		this.setData( p.getName(), p.getUniqueId() );
	}

	public PlayerData( String name ){
		this.setData( name, null );
	}

	public PlayerData( UUID uuid ){
		this.setData( null, uuid );
	}

	public PlayerData( KeyValue kvs, String root ){
		this.load(kvs, root);
	}

	public void save( KeyValue kvs, String root ){
		core.msg.debug( this.getClass().getSimpleName(), "save", "root=" + root );
		if ( this.name != null ) kvs.setString( root + ".name", this.name );
		if ( this.uuid != null ) kvs.setString( root + ".uuid", this.uuid.toString() );
	}

	public boolean load( KeyValue kvs, String root ){
		this.name = null;
		this.uuid = null;

		core.msg.debug( this.getClass().getSimpleName(), "load", "root=" + root );
		if ( kvs.getString( root + ".name" ) != null ) this.name = kvs.getString( root + ".name" );
		if ( kvs.getString( root + ".uuid" ) != null ) this.uuid = UUID.fromString( kvs.getString( root + ".uuid" ) );

		if ( this.name == null && this.uuid == null ) return false;
		this.setData( this.name, this.uuid );
		return true;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Gets
	// ---------------------------------------------------------------------------------------------------------------
	public String getName(){
		return this.name;
	}

	public UUID getUniqueId(){
		return this.uuid;
	}

	public Player getPlayer(){
		if ( this.uuid == null ) {
			return Utils.getExistPlayer( this.name );
		}
		return Utils.getExistUUID( this.uuid );
	}

	private void setData( String name, UUID uuid ){
		this.name = name;
		this.uuid = uuid;

		if ( this.uuid == null ) {
			if ( this.name == null ) return;
			Player p = Utils.getExistPlayer( this.name );
			if ( p == null ) return;
			this.name = p.getName();
			this.uuid = p.getUniqueId();
		}else{
			Player p = Utils.getExistUUID( this.uuid );
			if ( p == null ) return;
			this.name = p.getName();
		}
		core.msg.debug( this.getClass().getSimpleName(), "setData", "name=" + this.name + ", uuid=" + this.uuid.toString() );
	}
}
