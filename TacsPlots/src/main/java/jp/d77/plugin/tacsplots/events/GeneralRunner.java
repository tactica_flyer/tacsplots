package jp.d77.plugin.tacsplots.events;

import jp.d77.plugin.tacsplots.TacsPlots;

import org.bukkit.scheduler.BukkitRunnable;

public class GeneralRunner extends BukkitRunnable{
	public static enum RUNNER_TYPE {
		SPAWN,
		NAV_MSG,
		AUTO_SAVE,
		DYNMAP,
		;
	}

	protected static TacsPlots core = TacsPlots.plugin;
	protected boolean enable = false;
	private	RUNNER_TYPE	runner_type;

	public boolean init(){
		this.enable = true;
		return true;
	}

	public boolean destroy(){
		this.enable = false;
		return true;
	}

	public GeneralRunner( RUNNER_TYPE rt ){
		core.msg.debug( this.getClass().getSimpleName(), "GeneralRunner", "declear: " + rt.toString() );
		this.runner_type = rt;
	}

	@Override
	public void run() {
		if ( ! this.enable ) return;

		if ( this.runner_type.equals( RUNNER_TYPE.NAV_MSG ) ){
			if ( core.town != null ) core.town.events.checkMoveMessage();

		}else if ( this.runner_type.equals( RUNNER_TYPE.SPAWN ) ){
			if ( core.town != null ) core.town.tp.checkSpawn();

		}else if ( this.runner_type.equals( RUNNER_TYPE.AUTO_SAVE ) ){
			core.save();

		}else if ( this.runner_type.equals( RUNNER_TYPE.DYNMAP ) ){
			if ( core.map != null ) core.map.updateArea();
		}
	}

}
