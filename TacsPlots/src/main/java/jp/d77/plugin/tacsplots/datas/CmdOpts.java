package jp.d77.plugin.tacsplots.datas;

import java.util.ArrayList;
import java.util.List;

import jp.d77.plugin.tputils.CmdData;
import jp.d77.plugin.tputils.Utils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class CmdOpts extends CmdData {
	private String	opt_world;	// -w
	private String	opt_player;	// -p
	private String	opt_town;	// -t
	private Boolean	opt_all;	// -a
	private ArrayList<String>	cmds = new ArrayList<String>();

	public CmdOpts( CommandSender sender, Command cmd, String label, String[] args ){
		super( sender, cmd, label, args);
		this.cmdAnalysis();
	}

	private void cmdAnalysis(){
		String w = null;

		for ( String s: this.getArgs() ){
			if ( s.equalsIgnoreCase("-w") ) w = s;
			else if ( s.equalsIgnoreCase("-p") ) w = s;
			else if ( s.equalsIgnoreCase("-t") ) w = s;
			else if ( s.equalsIgnoreCase("-a") ) {
				this.opt_all = true;
				w = null;
			} else {
				if ( w == null ){
					this.cmds.add( s );
				}else{
					if ( w.equalsIgnoreCase("-w") ) 		this.opt_world = s;
					else if ( w.equalsIgnoreCase("-p") )	this.opt_player = s;
					else if ( w.equalsIgnoreCase("-t") )	this.opt_town = s;
				}
				w = null;
			}
		}
	}

	public String getOptCommands( int i ){
		if ( this.cmds.size() < i+1 ) return null;
		return this.cmds.get(i);
	}

	public String[] getOptCommands(){
		return (String[])this.cmds.toArray( new String[ this.cmds.size() ] );
	}
	public static String[] ArrayList2String( List<String> s ){
		return (String[])s.toArray( new String[ s.size() ] );
	}

	public String getOptWorld(){
		return this.opt_world;
	}

	public String getOptPlayer(){
		return this.opt_player;
	}

	public String getOptTown(){
		return this.opt_town;
	}

	public Boolean isOptAll(){
		return this.opt_all;
	}

	public String debugString(){
		String ret = "";

		if ( this.isConsole() ){
			ret += "(Console) ";
		}else{
			ret += "(Player=" + this.getPlayerName() + ") ";
		}
		ret += "label=" + this.getLabel() + " ";

		if ( this.getOptWorld() != null )	ret += "world=" + this.getOptWorld() + " ";
		if ( this.getOptPlayer() != null )	ret += "player=" + this.getOptPlayer() + " ";
		if ( this.getOptTown() != null )	ret += "town=" + this.getOptTown() + " ";
		if ( this.isOptAll() != null )		ret += "all=true ";

		ret += "cmds=" + Utils.join(" ", this.cmds);

		return ret;
	}
}
