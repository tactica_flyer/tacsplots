package jp.d77.plugin.tacsplots.config;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tputils.config.ConfigBase;

public class ConfigMain extends ConfigBase {
	protected static TacsPlots core = TacsPlots.plugin;

	public ConfigMain(){
		super( core, "config.yml" );
	}

	@Override
	public void setDefaults(){

		super.setDefault( "dynmap.lang.NULL"	, "なし" );
		super.setDefault( "dynmap.lang.ON"		,"許可");
		super.setDefault( "dynmap.lang.OFF"		,"拒否");

		super.setDefault( "dynmap.baloon"
				, "<span style=\"font-weight:bold;\">地名:</span> %name%<br/>"
				+ "<span style=\"font-weight:bold;\">オーナ:</span> %owner%<br/>"
				+ "<span style=\"font-weight:bold;\">フレンド:</span> %friends%<br/>\n"
				+ "<span style=\"font-weight:bold;\">区画数:</span> %plots%<br/>"
				+ "<span style=\"font-weight:bold;\">権限...</span> <br/>%perm%"
				);

		super.setDefault( "dynmap.baloon_perm"
				, "ゲスト招待(OPEN) : $1<br/>"
				+ "建築/採掘(BUILD) : $2<br/>"
				+ "操作/解錠(SWITCH): $3<br/>"
				+ "着火(FIRE)       : $4<br/>"
				+ "対人戦(PvP)      : $5<br/>"
				+ "生物(ANIMAL)     : $6<br/>"
				+ "敵対生物(MOB)    : $7<br/>"
				+ "爆発(EXPLOSION)  : $8<br/>"
				+ "保管庫(STORAGE)  : $9"
				);

		super.setDefault( "dynmap.marker.home", "house");

		super.setDefault( "system.save_interval_min", 10);

		super.setDefault( "spawn.cooldown", 5);
		super.setDefault( "spawn.warmup", 30);
		super.setDefault( "spawn.toworld.shigen", "res1");

		super.setDefault( "claim.min_range.normal", 5);
		super.setDefault( "claim.min_range.friend", 1);

		super.setDefault( "claim.pp.startexp", 10);
		super.setDefault( "claim.pp.startbonus", 10);
		super.setDefault( "claim.pp.minexp", -256);
		super.setDefault( "claim.pp.maxexp", 999);
		super.setDefault( "claim.pp.claim.near", 1);
		super.setDefault( "claim.pp.claim.faraway", 5);

		super.setDefault( "claim.pp.plot_bonus.increase_1", 64);
		super.setDefault( "claim.pp.plot_bonus.increase_2", 128);
		super.setDefault( "claim.pp.plot_bonus.increase_3", 256);
		super.setDefault( "claim.pp.plot_bonus.increase_4", 512);
		super.setDefault( "claim.pp.plot_bonus.increase_5", 1024);

		super.setDefault( "world_default.open",		false );
		super.setDefault( "world_default.pvp",		false );
		super.setDefault( "world_default.build",	false );
		super.setDefault( "world_default.fire",		false );
		super.setDefault( "world_default.switch",	true );
		super.setDefault( "world_default.mob",		true );
		super.setDefault( "world_default.animal",	true );
		super.setDefault( "world_default.explosion",false );

		super.setDefault( "MESSAGE.MAIN.PREFIX", "&a[TacsPlots]&r " );
		super.setDefault( "MESSAGE.MAIN.TRUE", "はい  " );
		super.setDefault( "MESSAGE.MAIN.FALSE", "いいえ" );
		super.setDefault( "MESSAGE.MAIN.NULL", "なし" );

		super.setDefault("MESSAGE.TP.COOLDOWN",	"移動します。$1 秒動かないで待って下さい." );
		super.setDefault("MESSAGE.TP.WARMUP",	"&e次回の瞬間移動まで、$1秒待って下さい.&r" );
		super.setDefault("MESSAGE.TP.CANCEL",	"&e瞬間移動は中断されました。&r" );

		super.setDefault( "MESSAGE.LANG.OWNER","オーナ" );
		super.setDefault( "MESSAGE.LANG.SUBOWNER","サブオーナ" );
		super.setDefault( "MESSAGE.LANG.FRIEND","フレンド" );
		super.setDefault( "MESSAGE.LANG.WILD","空き地" );
		super.setDefault( "MESSAGE.LANG.OF_LAND","$1の土地" );
		super.setDefault( "MESSAGE.LANG.PERM_TO","$1→$2" );

		super.setDefault( "MESSAGE.HELP"			,"/tacsplots ～ ... 管理用コマンド.\n"
				+ "/towns ～ ...所有地全体関連コマンド\n"
				+ "/plots ～ ...プロット(区画)関連コマンド"
				);

		super.setDefault( "MESSAGE.MSGMODE.HELP","/plots msgmode [none/normal/verbose]～ 移動時表示のメッセージをnone=なし、normal=通常、vervose=詳細表示にする.\n");
		super.setDefault( "MESSAGE.MSGMODE.DONE","移動時表示のメッセージを $1 にしました.\n");

		super.setDefault( "MESSAGE.PLAYER.NONE_SUBOWNER","あなたはオーナ、あるいはサブオーナではありません." );
		super.setDefault( "MESSAGE.PLAYER.NONE_FRIEND"	,"あなたは$1さんのフレンドではありません." );
		super.setDefault( "MESSAGE.PLAYER.SET_INVINCIBLE"	,"あなたは無敵/観光モードです.土地を取得するまで、敵の攻撃を受けません。攻撃することもできません." );
		super.setDefault( "MESSAGE.PLAYER.RESET_INVINCIBLE"	,"無敵/観光モードが解除されました." );

		super.setDefault( "MESSAGE.OBJECTS.HELP"			,"/tacsplots add/remove build/mob/switch ブロック/エンティティ/mob名 ... 指定対象名を設定します." );
		super.setDefault( "MESSAGE.OBJECTS.LIST.HELP"		,"/tacsplots list build/mob/switch ... 指定対象アイテム一覧を見ます." );
		super.setDefault( "MESSAGE.OBJECTS.ADD.DONE"		,"$1を追加しました." );
		super.setDefault( "MESSAGE.OBJECTS.REMOVE.DONE"		,"$1を追加しました." );

		super.setDefault( "MESSAGE.ADMIN.SAVELOAD.HELP"		,"/tacsplots save/load ... 設定の保存、読み込み." );
		super.setDefault( "MESSAGE.ADMIN.RELOAD.HELP"		,"/tacsplots reload ... プラグインのリロード" );
		super.setDefault( "MESSAGE.ADMIN.OWNER_OR_TOWN"		,"オーナ名か土地名、どちらか一方を指定してください." );
		super.setDefault( "MESSAGE.ADMIN.PP.SHORTAGE"		,"PlotCoinが足りません. 「$1 pc」が必要です." );

		super.setDefault( "MESSAGE.WORLD.WORLD","ワールド" );
		super.setDefault( "MESSAGE.WORLD.HELP"			,"/tacsplots enable/disable [-w ワールド名] ... 指定したワールドを管理対象/対象外にします." );
		super.setDefault( "MESSAGE.WORLD.SET.PERM.HELP"	,"/tacsplots set perm [open|pvp|build|fire|switch|mob|animal|explosion] [on|off] [-w ワールド] ... ワールドの権限を設定する." );

		super.setDefault( "MESSAGE.WORLD.NOT_FOUND"		,"指定したワールドは存在しません." );
		super.setDefault( "MESSAGE.WORLD.ENABLE"		,"このワールドをTacsPlots管理に設定しました." );
		super.setDefault( "MESSAGE.WORLD.DISABLE"		,"このワールドをTacsPlots管理から除外しました." );
		super.setDefault( "MESSAGE.WORLD.NOT_ENABLE"	,"このワールドではこのコマンドは使用できません." );
		super.setDefault( "MESSAGE.WORLD.SET.PERM.DONE"	,"ワールド $1 の権限 $2 を $3 にしました." );

		super.setDefault( "MESSAGE.WORLD.INFO.HELP"		,"/tacsplots info [-w ワールド名]... 指定したワールドの情報を見ます." );

		super.setDefault( "MESSAGE.WORLD.INFO.VIEW"			,"---------- ワールドの情報 ----------\n"
				+ "&bワールド名:&r $1\n"
				);

		super.setDefault( "MESSAGE.TOWNS.PLOTS.HELP"	,"/towns plots [-p オーナ名] [-t 土地名] ... 指定した土地/プレイヤー、あるいは自分の土地のPlot一覧を見ます." );
		super.setDefault( "MESSAGE.TOWNS.ADDREMOVE.HELP"	,"/towns add/remove friend/subowner ～ ... 他の人に権限を付与(詳しくは個別コマンドにて確認のこと)" );
		super.setDefault( "MESSAGE.TOWNS.HELP"	,"/towns info/home/plots/set/add/remove ～ ... 土地関連コマンド(詳しくは個別コマンドにて確認のこと)" );

		super.setDefault( "MESSAGE.TOWNS.TOWNS","土地" );
		super.setDefault( "MESSAGE.TOWNS.TITLE","地名" );
		super.setDefault( "MESSAGE.TOWNS.NOT_FOUND"		,"土地情報がありません." );
		super.setDefault( "MESSAGE.TOWNS.PERM.FAIL","土地に対する権限がありません." );

		super.setDefault( "MESSAGE.TOWNS.HOME.HOME","ホーム" );
		super.setDefault( "MESSAGE.TOWNS.HOME.FAIL"		,"ホームが設定されていません." );
		super.setDefault( "MESSAGE.TOWNS.HOME.HELP"		,"/towns home [資源名] [-p オーナ名] [-t 土地名] ... 自分のホームか資源、あるいは指定したフレンド/土地のホームへ移動します." );

		super.setDefault( "MESSAGE.TOWNS.SET.HOME.DONE"	,"$1 をホームに設定しました." );
		super.setDefault( "MESSAGE.TOWNS.SET.HOME.HELP"	,"/towns set home [-p オーナ名] [-t 土地名] ... 指定した場所を土地のhomeに設定する." );
		super.setDefault( "MESSAGE.TOWNS.SET.PERM.DONE"	,"所有地全体の権限 $1 を $2 にしました." );
		super.setDefault( "MESSAGE.TOWNS.SET.PERM.HELP"	,"/towns set [open|pvp|build|fire|switch|mob|animal|explosion] [on|off] [-p オーナ名] [-t 土地名] ... 土地の権限を設定する." );
		super.setDefault( "MESSAGE.TOWNS.SET.NAME.DONE"	,"所有の土地の名称を $1 にしました." );
		super.setDefault( "MESSAGE.TOWNS.SET.NAME.HELP"	,"/towns set name [名称] [-p オーナ名] [-t 土地名] ... 土地の名前を設定する(無指定で削除)." );
		super.setDefault( "MESSAGE.TOWNS.SET.HELP"		,"/towns set perm/home/name/msg ～ ... 土地の設定(詳しくは個別コマンドにて確認のこと)" );

		super.setDefault( "MESSAGE.TOWNS.ADDREMOVE.HELP","/towns add/remove friend/owner ～ ... フレンド/サブオーナ追加/削除(詳しくは個別コマンドにて確認のこと)" );
		super.setDefault( "MESSAGE.TOWNS.ADDREMOVE.FRIEND.HELP","/towns add/remove friend/owner フレンド名 [-p オーナ名] [-t 土地名] ... フレンドやサブオーナを追加/削除" );
		super.setDefault( "MESSAGE.TOWNS.ADD.FRIEND.DONE","$1 さんを $2 さんのフレンド登録しました." );
		super.setDefault( "MESSAGE.TOWNS.ADD.OWNER.DONE","$1 さんを $2 さんのサブオーナ登録しました." );
		super.setDefault( "MESSAGE.TOWNS.REMOVE.FRIEND.DONE","$1 さんを削除しました." );
		super.setDefault( "MESSAGE.TOWNS.REMOVE.ERROR","$1 さんは未登録です." );
		super.setDefault( "MESSAGE.TOWNS.ADD.PP.HELP","/towns add pp オーナ名 ポイント...ppを追加する" );
		super.setDefault( "MESSAGE.TOWNS.ADD.PP.DONE","PlotCoinを追加しました." );

		super.setDefault( "MESSAGE.TOWNS.INFO.HELP"		,"/towns info [-p オーナ名] [-t 土地名] ... 指定した土地/プレイヤー、あるいは自分の土地の情報を見ます." );
		super.setDefault( "MESSAGE.TOWNS.INFO.VIEW"			,"---------- 所有地全体の情報 ----------\n"
				+ "&bオーナ:&r $1  &b地名:&r $2  &bホーム:&r $3\n"
				+ "&b所有区画数:&r $4  &bPlotCoin:&r $5pc\n"
				+ "&bフレンド:&r $6\n"
				);
		super.setDefault( "MESSAGE.TOWNS.INFO.SIMPLEVIEW"			,"&b土地 OWNER:&r$1 &b地名:&r$2 区画:&r$4 $5pc"
				);

		super.setDefault( "MESSAGE.PLOTS.PLOTS","区画" );
		super.setDefault( "MESSAGE.PLOTS.TITLE","区画名" );
		super.setDefault( "MESSAGE.PLOTS.HELP"				,"/plots info/claim/unclaim/set/add/remove ～ ... 区画関連コマンド(詳しくは個別コマンドにて確認のこと)" );

		super.setDefault( "MESSAGE.PLOTS.EVENT.BUILD.CANCEL","ここで建築・破壊はできません." );
		super.setDefault( "MESSAGE.PLOTS.EVENT.FIRE.CANCEL","ここで着火はできません." );
		super.setDefault( "MESSAGE.PLOTS.EVENT.SWITCH.CANCEL","操作/解錠はできません." );
		super.setDefault( "MESSAGE.PLOTS.EVENT.STORAGE.CANCEL","この収納は利用できません." );

		super.setDefault( "MESSAGE.PLOTS.ADDREMOVE.HELP"	,"/plots add/remove friend/subowner ～ ... 他の人に権限を付与(詳しくは個別コマンドにて確認のこと)" );

		super.setDefault( "MESSAGE.PLOTS.CLAIM.HELP"		,"/plots claim [-p オーナ名] [-t 土地名] ... 今いるチャンクを自分(あるいは指定した人、土地名)の区画として取得します" );
		super.setDefault( "MESSAGE.PLOTS.CLAIM.NONE_OWNER"	,"この区画はすでに所有者が居ます.詳細は/p infoで確認を." );
		super.setDefault( "MESSAGE.PLOTS.CLAIM.NEAR"		,"近くに別の人の区画があるため入手できません." );
		super.setDefault( "MESSAGE.PLOTS.CLAIM.DONE"		,"区画$1を取得しました." );
		super.setDefault( "MESSAGE.PLOTS.CLAIM.MAXPLOTS"	,"これ以上区画を取得できません.区画経験値を積んで上限を増やしてください." );

		super.setDefault( "MESSAGE.PLOTS.UNCLAIM.DONE"		,"この区画を手放しました." );
		super.setDefault( "MESSAGE.PLOTS.UNCLAIM.HELP"		,"/plots unclaim ... 今いる区画を手放します" );

		super.setDefault( "MESSAGE.PLOTS.RESET.DONE"		,"区画固有の設定を初期状態にしました." );
		super.setDefault( "MESSAGE.PLOTS.RESET.HELP"		,"/plots reset ... 今いる区画固有の名前・権限・フレンドを削除します." );

		super.setDefault( "MESSAGE.PLOTS.NO_CLAIM"			,"この区画は誰にも所有されていません." );
		super.setDefault( "MESSAGE.PLOTS.NOT_OWNER"			,"あなたは区画の所有者ではありません." );
		super.setDefault( "MESSAGE.PLOTS.NOT_PERM"			,"その区画に対する権限がありません." );
		super.setDefault( "MESSAGE.PLOTS.SET.PERM.HELP"		,"/plots set [open|pvp|build|fire|switch|mob|animal|explosion] [on|off] ... 区画の権限を設定する." );
		super.setDefault( "MESSAGE.PLOTS.SET.PERM.DONE"		,"この区画 $1 の権限 $2 を $3 にしました." );
		super.setDefault( "MESSAGE.PLOTS.SET.HELP"			,"/plots set perm/name/msg ～ ... 区画の設定(詳しくは個別コマンドにて確認のこと)" );
		super.setDefault( "MESSAGE.PLOTS.SET.NAME.HELP"		,"/plots set name [名称] [-p オーナ名] [-t 土地名] ... 区画の名前を設定する(無指定で削除)." );
		super.setDefault( "MESSAGE.PLOTS.SET.NAME.DONE"		,"$1 の区画の名称を $2 にしました." );

		super.setDefault( "MESSAGE.PLOTS.LVUP"				,"区画レベルが上がりました.($1)" );
		super.setDefault( "MESSAGE.PLOTS.LVDOWN"			,"区画レベルが下がりました.($1)" );

		super.setDefault( "MESSAGE.PLOTS.ADDREMOVE.HELP"	,"/plots add/remove friend ～ ... 区画を利用できるフレンド追加/削除(詳しくは個別コマンドにて確認のこと)" );
		super.setDefault( "MESSAGE.PLOTS.ADDREMOVE.FRIEND.HELP","/plots add/remove friend フレンド名 ... 区画を利用できるフレンドを追加/削除" );
		super.setDefault( "MESSAGE.PLOTS.ADDREMOVE.FRIEND.RENTAL.RENTERR","この区画は既に貸し出し中です." );
		super.setDefault( "MESSAGE.PLOTS.ADDREMOVE.FRIEND.RENTAL.TARGETERR","あなたの名前を指定してください." );
		super.setDefault( "MESSAGE.PLOTS.ADD.FRIEND.DONE"	,"$1 さんをこの区画のフレンドとして登録しました." );
		super.setDefault( "MESSAGE.PLOTS.ADD.FRIEND.ERROR"	,"既にフレンドとして登録済みです." );
		super.setDefault( "MESSAGE.PLOTS.ADD.PLOTEXP.DONE"	,"$1 さんの区画 $2 へPlotExpが変化しました( $3 pe )." );
		super.setDefault( "MESSAGE.PLOTS.ADD.PLOTEXP.HELP"	,"/plots add pe 値 ... 区画にPlotExpを追加" );
		super.setDefault( "MESSAGE.PLOTS.REMOVE.FRIEND.DONE","$1 さんをフレンドから削除しました." );
		super.setDefault( "MESSAGE.PLOTS.REMOVE.ERROR","$1 さんはフレンド未登録です." );

		super.setDefault( "MESSAGE.PLOTS.INFO.HELP"			,"/plots info ... 今いる区画の情報を見ます." );
		super.setDefault( "MESSAGE.PLOTS.INFO.VIEW","---------- 区画の情報 ----------\n"
				+ "&b座標(Chunk):&r $1  &bオーナ:&r $2  &bPlotPoint:&r $3\n"
				+ "&bフレンド:&r $4"
				);
		super.setDefault( "MESSAGE.PLOTS.INFO.SIMPLEVIEW","&b区画($1)&r &bOWNER:&r$2 $3");

		super.setDefault( "MESSAGE.PERM.NOPERM"		,"権限がありません.");
		super.setDefault( "MESSAGE.PERM.NOPERM_OPT"		,"権限がありません($1).");
		super.setDefault( "MESSAGE.PERM.OPEN"		,"オープン");
		super.setDefault( "MESSAGE.PERM.TRUE"	, "この権限はフレンド以外にも適用されます.");
		super.setDefault( "MESSAGE.PERM.FALSE"	, "この権限はフレンドに適用されます.");
		super.setDefault( "MESSAGE.PERM.PVP"		,"対人戦");
		super.setDefault( "MESSAGE.PERM.BUILD"		,"建築/破壊");
		super.setDefault( "MESSAGE.PERM.FIRE"		,"着火");
		super.setDefault( "MESSAGE.PERM.SWITCH"		,"ドア/スイッチ操作");
		super.setDefault( "MESSAGE.PERM.MOB"		,"敵対動物の出現");
		super.setDefault( "MESSAGE.PERM.ANIMAL"		,"全動物の出現");
		super.setDefault( "MESSAGE.PERM.EXPLOSION"	,"爆発による破壊");
		super.setDefault( "MESSAGE.PERM.STORAGE"	,"保管庫の利用");
		super.setDefault( "MESSAGE.PERM.RENTAL"		,"貸し出し区画");

		super.setDefault( "MESSAGE.PERM.ON"			,"許可");
		super.setDefault( "MESSAGE.PERM.OFF"		,"&7拒否&r");
		super.setDefault( "MESSAGE.PERM.INFO.VIEW"		, "- 権限 -\n"
				+ "&bゲスト招待(OPEN):&r $1\n"
				+ "&b建築/採掘(BUILD):&r $2  &b操作/解錠(SWITCH):&r $3\n"
				+ "&b着火(FIRE)      :&r $4  &b対人戦(PvP)      :&r $5\n"
				+ "&b生物(ANIMAL)    :&r $6  &b敵対生物(MOB)    :&r $7\n"
				+ "&b爆発(EXPLOSION) :&r $8  &b保管庫(STORAGE)  :&r $9\n"
				);
		super.setDefault( "MESSAGE.PERM.INFO.VIEW_RENTAL"
				, "&b貸し出し(RENTAL):&r $1\n"
				);
		super.setDefault( "MESSAGE.PERM.INFO.SIMPLEVIEW", "&b権限:&r $1" );

	}

	public String yesno( boolean b ){
		if ( b ) return this.getString( "MESSAGE.MAIN.TRUE" );
		return this.getString( "MESSAGE.MAIN.FALSE" );
	}
}
