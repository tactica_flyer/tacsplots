package jp.d77.plugin.tacsplots.command;

import java.util.HashMap;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.CmdOpts;
import jp.d77.plugin.tacsplots.datas.Consts.MOVE_MESSAGE_MODE;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author tactica
 *
 */
public class CommandManager implements CommandExecutor{
	/*
	 * ploat save/load/reload
	 * plots list -p <player> -a -w <world> <page>
	 * plots perm -p <owner> -t <town_name> -w <world> -a
	 * plots claim -p <owner> -t <town_name>
	 * plots unclaim
	 * plots set perm <PERM> <true/false/allow/deny/1/0> -p <owner> -t <town_name> -w <world> -a
	 * plots set home -p <owner> -t <town_name>
	 * plots set name <town name> -p <owner> -t <town_name>
	 * plots set msg <town msg> -p <owner> -t <town_name>
	 * plots set owner <name>
	 * plots set friend <name> -p <owner> -t <town_name>
	 * plots del <name> -p <owner> -t <town_name>
	 * plots clear perm -a -p <owner> -t <town_name>
	 * plots add exp <num>
	 * -a ... all
	 * -p / -t ... target town
	 */
	protected TacsPlots core = TacsPlots.plugin;

	private boolean enable = false;

	private HashMap<CommandSender,CmdOpts> saveCmddata = new HashMap<CommandSender,CmdOpts>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if ( ! enable ) return true;
		return onCommand( new CmdOpts( sender, cmd, label, args ) );
	}

	public boolean permMsg( CmdOpts cmddata, String perm, String msg ){
		if ( ! cmddata.isConsole() ){
			if ( ! cmddata.getPlayer().hasPermission( perm ) ) return false;
		}
		core.msg.addMessage( cmddata.getSender(), msg );
		return true;
	}

	public boolean onCommand( CmdOpts cmddata ) {
		if ( !this.enable ) return false;

		core.msg.debug( this.getClass().getSimpleName(), "onCommand", "sender name=" + cmddata.getSender().getName() );

		core.msg.debug( this.getClass().getSimpleName(), "onCommand", cmddata.debugString() );
/*
			if ( cmddata.getArgs()[0].equalsIgnoreCase("yes") ) return this.submitYesNo( cmddata );
			if ( cmddata.getArgs()[0].equalsIgnoreCase("no") ) return this.submitYesNo( cmddata );
			this.removeYesNo( cmddata );

 */
		if( cmddata.getCmd().getName().equalsIgnoreCase("tacsplots")){
			// ---------------------------------------------------------------------------------------------------------------
			// System Commands
			// ---------------------------------------------------------------------------------------------------------------
			if ( cmddata.getOptCommands(0) != null ){
				if ( cmddata.getArgs()[0].equalsIgnoreCase("debug") ) {
					if ( cmddata.isConsole() ){
						core.setDebug( ! core.isDebug() );
						core.msg.info( "debug=" + core.isDebug() );
					}else if ( cmddata.getPlayer().isOp() ){
						core.setDebug( ! core.isDebug() );
						core.msg.info( "debug=" + core.isDebug() );
					}
					return true;
				}

				// save
				if ( cmddata.getArgs()[0].equalsIgnoreCase("save") ) return this.save( cmddata );

				// load
				if ( cmddata.getArgs()[0].equalsIgnoreCase("load") ) return this.load( cmddata );

				// reload
				if ( cmddata.getArgs()[0].equalsIgnoreCase("reload") ) return this.reload( cmddata );

				// enable world
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "enable" ) ){
					core.town.cmdWorld.enable_world(cmddata);
					return true;
				}

				// disable world
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "disable" ) ){
					core.town.cmdWorld.disable_world(cmddata);
					return true;

				}

				// world permission
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "worldperm" ) ){
					core.town.cmdWorld.world_setperm(cmddata);
					return true;

				}

				// add object
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "add" ) ){
					this.setObject(true, cmddata);
					return true;

				}

				// remove object
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "remove" ) ){
					this.setObject(false, cmddata);
					return true;
				}

				// list Object
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "list" ) ){
					this.listObject( cmddata);
					return true;

				}
			}

			// Help message
			core.msg.addMessage( cmddata.getSender(), core.getName() );
			this.permMsg( cmddata, "tacsplots.admin.saveload", "MESSAGE.ADMIN.SAVELOAD.HELP" );
			this.permMsg( cmddata, "tacsplots.admin.reload", "MESSAGE.ADMIN.RELOAD.HELP" );
			this.permMsg( cmddata, "tacsplots.admin.objects", "MESSAGE.OBJECTS.HELP");
			this.permMsg( cmddata, "tacsplots.admin.world.set.perm", "MESSAGE.WORLD.SET.PERM.HELP");
			if ( ! this.permMsg( cmddata, "tacsplots.admin.world.enable", "MESSAGE.WORLD.HELP") ){
				this.permMsg( cmddata, "tacsplots.admin.world.disable", "MESSAGE.WORLD.HELP");
			}
			return true;

		}else if( cmddata.getCmd().getName().equalsIgnoreCase("worlds")){
			// ---------------------------------------------------------------------------------------------------------------
			// World Commands
			// ---------------------------------------------------------------------------------------------------------------
			if ( cmddata.getOptCommands(0) != null ){

				// world info
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "info" ) ){
					core.town.cmdWorld.world_info(cmddata);
					return true;
				}

				// world enable
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "enable" ) ){
					core.town.cmdWorld.enable_world(cmddata);
					return true;

				}

				// world disable
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "disable" ) ){
					core.town.cmdWorld.disable_world(cmddata);
					return true;

				}

				// world permission
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "set" ) ){
					if ( cmddata.getOptCommands(1) != null ){
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "perm" ) ){
							core.town.cmdWorld.world_setperm(cmddata);
						}
					}
					return true;

				}
			}

		}else if( cmddata.getCmd().getName().equalsIgnoreCase("towns")){
			// ---------------------------------------------------------------------------------------------------------------
			// Towns Commands
			// ---------------------------------------------------------------------------------------------------------------
			if ( cmddata.getOptCommands(0) != null ){

				// towns info
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "info" ) ){
					core.town.cmdTowns.town_info( cmddata );
					return true;
				}

				// towns/plot claim
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "claim" ) ){
					core.town.cmdPlots.plot_claim( cmddata );
					return true;
				}

				// claimed plots list
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "plots" ) ){
					core.town.cmdTowns.town_plots( cmddata );
					return true;
				}

				// jump home
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "home" ) || cmddata.getOptCommands(0).equalsIgnoreCase( "spawn" ) ){
					core.town.cmdTowns.town_home( cmddata );
					return true;
				}

				// jump resource world
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "shigen" ) || cmddata.getOptCommands(0).equalsIgnoreCase( "sigen" ) || cmddata.getOptCommands(0).equalsIgnoreCase( "res" ) ){
					core.town.cmdTowns.town_home( cmddata );
					return true;

				}

				// towns set commands
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "set" ) ){
					if ( cmddata.getOptCommands(1) != null ){

						// towns set permissions
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "open" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "build" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "fire" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "switch" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "pvp" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "mob" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "animal" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "explosion" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "exp" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "storage" )
								){
							core.town.cmdTowns.town_setperm( cmddata );
							return true;

						}

						// set home
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "home" ) ){
							core.town.cmdTowns.town_sethome( cmddata );
							return true;

						}

						// set town name
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "name" ) ){
							core.town.cmdTowns.town_title( cmddata );
							return true;

						// set town message
						}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "msg" ) ){
						}
					}

					// Towns set Help
					this.permMsg( cmddata, "tacsplots.player.towns.set", "MESSAGE.TOWNS.SET.HELP");
					return true;

				}

				// towns add commands
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "add" ) ){
					if ( cmddata.getOptCommands(1) != null ){
						// (admin command) add pp
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "pp" ) ){
							core.town.cmdTowns.town_addpp( cmddata );
							return true;

						}

						// add friend
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "friend" ) ){
							core.town.cmdTowns.town_friend( cmddata );
							return true;

						}

						// add sub owner
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "owner" ) ){
							core.town.cmdTowns.town_friend( cmddata );
							return true;
						}
					}

					// town add Help
					this.permMsg( cmddata, "tacsplots.player.towns.add", "MESSAGE.TOWNS.ADDREMOVE.HELP");
					return true;

				}

				// towns remove commands
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "remove" ) ){
					if ( cmddata.getOptCommands(1) != null ){

						// remove friend
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "friend" ) ){
							core.town.cmdTowns.town_friend( cmddata );
							return true;
						}

						// remove sub owner
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "owner" ) ){
							core.town.cmdTowns.town_friend( cmddata );
							return true;
						}
					}
					this.permMsg( cmddata, "tacsplots.player.towns.remove", "MESSAGE.TOWNS.ADDREMOVE.HELP");
					return true;
				}
			}
			this.permMsg( cmddata, "tacsplots.player.towns", "MESSAGE.TOWNS.HELP");
			return true;

		}else if( cmddata.getCmd().getName().equalsIgnoreCase("plots")){
			// ---------------------------------------------------------------------------------------------------------------
			// Plots Commands
			// ---------------------------------------------------------------------------------------------------------------
			if ( cmddata.getOptCommands(0) != null ){
				// plots info
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "msgmode" ) ){
					this.setMsgMode(cmddata);
					return true;
				}

				// plots info
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "info" ) ){
					core.town.cmdPlots.plot_info( cmddata );
					return true;
				}

				// plots claim
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "claim" ) ){
					core.town.cmdPlots.plot_claim( cmddata );
					return true;

				}

				// plots unclaim
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "unclaim" ) ){
					core.town.cmdPlots.plot_unclaim( cmddata );
					return true;

				}

				// plots set commands
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "set" ) ){
					if ( cmddata.getOptCommands(1) != null ){
						// set permissions
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "open" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "build" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "fire" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "switch" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "pvp" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "mob" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "animal" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "explosion" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "exp" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "storage" )
								|| cmddata.getOptCommands(1).equalsIgnoreCase( "rental" )
								){
							core.town.cmdPlots.plot_setperm( cmddata );
							return true;

						}

						// set name
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "name" ) ){
							core.town.cmdPlots.plot_title( cmddata );
							return true;

						}

						// set message
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "msg" ) ){
						}
					}
					this.permMsg( cmddata, "tacsplots.player.plots.add", "MESSAGE.PLOTS.SET.HELP");
					return true;

				}

				// permission reset
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "reset" ) ){
					core.town.cmdPlots.plot_reset( cmddata );
					return true;

				}

				// plots add commands
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "add" ) ){
					if ( cmddata.getOptCommands(1) != null ){
						// friend add
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "friend" ) ){
							core.town.cmdPlots.plot_friend( cmddata );
							return true;
						}

						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "pe" ) ){
							core.town.cmdPlots.plot_addpe( cmddata );
							return true;
						}

					}
					this.permMsg( cmddata, "tacsplots.player.plots.add", "MESSAGE.PLOTS.ADDREMOVE.HELP");
					return true;

				}

				// plots remove commands
				if ( cmddata.getOptCommands(0).equalsIgnoreCase( "remove" ) ){
					if ( cmddata.getOptCommands(1) != null ){
						if ( cmddata.getOptCommands(1).equalsIgnoreCase( "friend" ) ){
							core.town.cmdPlots.plot_friend( cmddata );
							return true;
						}
					}
					this.permMsg( cmddata, "tacsplots.player.plots.remove", "MESSAGE.PLOTS.ADDREMOVE.HELP");
					return true;
				}
			}
			this.permMsg( cmddata, "tacsplots.player.plots", "MESSAGE.PLOTS.HELP");
			return true;
		}
		core.msg.addMessage( cmddata.getSender(), core.getName() );
		core.msg.addMessage( cmddata.getSender(), "MESSAGE.HELP" );
		return true;
	}

	public boolean init(){
		this.saveCmddata = new HashMap<CommandSender,CmdOpts>();
		this.enable = true;
		return true;
	}

	public boolean destroy(){
		this.enable = false;
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Methods
	// ---------------------------------------------------------------------------------------------------------------
	public void setMsgMode( CmdOpts cmddata ){
		cmddata.setPlayerCommand(true);
		cmddata.setConsoleCommand(false);
		cmddata.setArgumentsLength( 1, 99 );
		cmddata.setPermission("tacsplots.player.msgmode" );

		if ( ! cmddata.checkExec( core.msg ) ) {
			return;
		}

		String	mode = cmddata.getOptCommands(1);
		if ( mode == null ){
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.MSGMODE.HELP" );
			return;
		}

		if ( mode.equalsIgnoreCase("none") ){
			core.town.events.setMoveMessageMode( cmddata.getPlayerName(), MOVE_MESSAGE_MODE.NONE );
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.MSGMODE.DONE","NONE" );
		}else if ( mode.equalsIgnoreCase("normal") ){
			core.town.events.setMoveMessageMode( cmddata.getPlayerName(), MOVE_MESSAGE_MODE.NORMAL );
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.MSGMODE.DONE","NORMAL" );
		}else if ( mode.equalsIgnoreCase("verbose") ){
			core.town.events.setMoveMessageMode( cmddata.getPlayerName(), MOVE_MESSAGE_MODE.VERBOSE );
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.MSGMODE.DONE","VERBOSE" );
		}else{
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.MSGMODE.HELP" );
		}
		return;
	}

	public void setObject( boolean add, CmdOpts cmddata ){
		cmddata.setPlayerCommand(true);
		cmddata.setConsoleCommand(true);
		cmddata.setArgumentsLength( 3, 99 );
		String	obj;

		if ( cmddata.getOptCommands(1) == null || cmddata.getOptCommands(2) == null ){
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.OBJECTS.HELP" );
			return;
		}

		cmddata.setPermission("tacsplots.admin.objects." + cmddata.getOptCommands(1) );
		obj = cmddata.getOptCommands(2);
		if ( ! cmddata.checkExec( core.msg ) ) return;

		if ( add ){
			if ( cmddata.getOptCommands(1).equalsIgnoreCase( "build" ) ){
				core.obj.addBuildItem( obj, true );
			}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "mob" ) ){
				core.obj.addMob( obj, true );
			}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "switch" ) ){
				core.obj.addSwitchItem( obj, true );
			}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "block" ) ){
				core.obj.addBuildBlocks( obj, true );
			}else{
				core.msg.addMessage( cmddata.getSender(), "MESSAGE.OBJECTS.HELP" );
				return;
			}
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.OBJECTS.ADD.DONE", obj );
			return;
		}else{
			if ( cmddata.getOptCommands(1).equalsIgnoreCase( "build" ) ){
				core.obj.removeBuildItem( obj );
			}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "mob" ) ){
				core.obj.removeMob( obj );
			}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "switch" ) ){
				core.obj.removeSwitchItem( obj );
			}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "block" ) ){
				core.obj.removeBuildBlocks( obj );
			}else{
				core.msg.addMessage( cmddata.getSender(), "MESSAGE.OBJECTS.HELP" );
				return;
			}
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.OBJECTS.REMOVE.DONE", obj );
			return;
		}
	}

	public void listObject( CmdOpts cmddata ){
		cmddata.setPlayerCommand(true);
		cmddata.setConsoleCommand(true);
		cmddata.setArgumentsLength( 2, 99 );

		cmddata.setPermission( "tacsplots.admin.objects." + cmddata.getOptCommands(1) );
		if ( ! cmddata.checkExec( core.msg ) ) return;

		if ( cmddata.getOptCommands(1).equalsIgnoreCase( "build" ) ){
			core.msg.addMessage( cmddata.getSender(), core.obj.getBuildItems() );

		}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "mob" ) ){
			core.msg.addMessage( cmddata.getSender(), core.obj.getMobs() );

		}else if ( cmddata.getOptCommands(1).equalsIgnoreCase( "switch" ) ){
			core.msg.addMessage( cmddata.getSender(), core.obj.getSwitchItems() );

		}else{
			core.msg.addMessage( cmddata.getSender(), "MESSAGE.OBJECTS.LIST.HELP" );
		}
		return;
	}

	public void setYesNo( CmdOpts cmddata ){
		core.msg.addMessage( cmddata.getSender(), "COMMANDS.YESNO" );
		this.saveCmddata.put( cmddata.getSender(), cmddata );
	}

	public CmdOpts removeYesNo( CmdOpts cmddata ){
		if ( this.saveCmddata.containsKey( cmddata.getSender() ) ) {
			return this.saveCmddata.remove( cmddata.getSender() );
		}
		return null;
	}

	public boolean submitYesNo( CmdOpts cmddata ){
		CmdOpts cmd = this.removeYesNo( cmddata );

		if ( cmd == null ) {
			if ( cmddata.getArgs()[0].equalsIgnoreCase( "yes" ) ){
				core.msg.addMessage( cmddata.getSender(), "COMMANDS.FAIL_YES" );
			}else{
				core.msg.addMessage( cmddata.getSender(), "COMMANDS.FAIL_NO" );
			}
			return true;
		}

		if ( cmddata.getArgs()[0].equalsIgnoreCase( "yes" ) ){
			core.msg.addMessage( cmddata.getSender(), "*YES*" );
		}else{
			core.msg.addMessage( cmddata.getSender(), "*NO*" );
		}

		if ( cmddata.getArgs()[0].equalsIgnoreCase( "yes" ) ){
			cmd.setYesNoFlag(true);
			return this.onCommand( cmd );
		}
		return true;
	}

	private boolean save( CmdOpts cmddata ){
		cmddata.setPermission( "tacsplots.admin.save" );
		if ( ! cmddata.checkExec( core.msg ) ) return false;
		core.save();
		core.msg.addMessage( cmddata.getSender(), "saved");
		return true;
	}

	private boolean load( CmdOpts cmddata ){
		cmddata.setPermission( "tacsplots.admin.load" );
		if ( ! cmddata.checkExec( core.msg ) ) return false;
		core.load();
		core.msg.addMessage( cmddata.getSender(), "loaded");
		return true;
	}

	private boolean reload( CmdOpts cmddata ){
		cmddata.setPermission( "tacsplots.admin.reload" );
		if ( ! cmddata.checkExec( core.msg ) ) return false;
		core.reload();
		core.msg.addMessage( cmddata.getSender(), "reloaded");
		return true;
	}

}
