package jp.d77.plugin.tacsplots.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.ChunkLoc;
import jp.d77.plugin.tacsplots.datas.PlotData;
import jp.d77.plugin.tacsplots.datas.TownData;
import jp.d77.plugin.tacsplots.events.GeneralRunner;
import jp.d77.plugin.tacsplots.events.GeneralRunner.RUNNER_TYPE;
import jp.d77.plugin.tputils.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.dynmap.DynmapAPI;
import org.dynmap.markers.AreaMarker;
import org.dynmap.markers.Marker;
import org.dynmap.markers.MarkerAPI;
import org.dynmap.markers.MarkerIcon;
import org.dynmap.markers.MarkerSet;

public class DynmapManager {
	protected static TacsPlots core = TacsPlots.plugin;
	private boolean enable = false;

	// 描画用列挙データ
	private enum DMLPOS {
		RT,RB,LT,LB
	}

	// Dynmap Classes
	private Plugin dynmap;
	private DynmapAPI api;
	private MarkerAPI markerapi;
	private MarkerSet mset;

	// Dynmap params
	/*
	private boolean use3d;
	private String infowindow;
	private int maxdepth;
	private int updatesPerTick = 20;
	*/
	private Map<String, AreaMarker> resAreas = new HashMap<String, AreaMarker>();
	private Map<String, Marker> resMarker = new HashMap<String, Marker>();
	MarkerIcon homeMarker;

	// 枠線描画用データ
	private HashMap<String,PlotData> plots = new HashMap<String,PlotData>();				// plot識別子/plotデータ格納用
	private HashMap<String,Integer> plots_idx = new HashMap<String,Integer>();				// plot識別子/枠線グループ格納用
	private ArrayList<ArrayList<String>> plots_tbl = new ArrayList<ArrayList<String>>();	// 枠線グループ別→plot識別子二次元配列

	private ArrayList<ArrayList<Location>> points = new ArrayList<ArrayList<Location>>();	// 枠線グループ別→描画線座標二次元配列
	private HashMap<String,Boolean>	points_idx = new HashMap<String,Boolean>();				// 抽出済み座標識別子/ダミーデータ一時格納用
	private ArrayList<String> owner_name = new ArrayList<String>();

	private int updatesPerTick = 30 * 20;
	private long UpdateDate = 0;
	private	GeneralRunner	updateEvent;

	private double		fillOpacity	= 0.10d;
	private int			fillColor	= 0xFFFFFF;
	private double		lineOpacity	= 0.40d;
	private int			lineColor	= 0x00FF00;
	private int			lineWeight	= 2;
	private String		homeIcon	= "house";

	public DynmapManager(){
		this.enable = false;
	}

	public boolean init(){
		PluginManager pm = core.getServer().getPluginManager();
		dynmap = pm.getPlugin("dynmap");
		if(dynmap == null) {
			this.destroy();
			return false;
		}

		if ( dynmap.isEnabled() ){
			// set api
			this.api = (DynmapAPI)dynmap;

			// set markerapi
			this.markerapi = this.api.getMarkerAPI();
			if(markerapi == null) {
				this.destroy();
				return false;
			}

			// set markerset
			this.mset = markerapi.getMarkerSet( "tacsplots.markerset" );
			if( this.mset == null ){
				this.mset = markerapi.createMarkerSet( "tacsplots.markerset", core.cfg.getString("dynmap.layer.name", "TacsPlots"), null, false );
			}else{
				this.mset.setMarkerSetLabel( core.cfg.getString("dynmap.layer.name", "TacsPlots") );
			}

			if( this.mset == null) {
				this.destroy();
				return false;
			}

			int minzoom			= core.cfg.getInteger( "dynmap.layer.minzoom", 0 );
			if(minzoom > 0) this.mset.setMinZoom( minzoom );
			this.mset.setLayerPriority( core.cfg.getInteger("dynmap.layer.layerprio", 10) );
			this.mset.setHideByDefault( core.cfg.getBoolean("dynmap.layer.hidebydefault", false) );
			this.homeMarker = markerapi.getMarkerIcon( this.homeIcon );

			/*
			this.use3d			= core.cfg.getBoolean( "dynmap.use3dregions", false );
			// this.infowindow = core.cfg.getString( "infowindow", DEF_INFOWINDOW );
			this.maxdepth		= core.cfg.getInteger( "dynmap.maxdepth", 16 );
			this.updatesPerTick	= core.cfg.getInteger( "updates-per-tick", 20 );
			 */

			this.updateEvent		= new GeneralRunner( RUNNER_TYPE.DYNMAP );
			this.updateEvent.init();
			this.updateEvent.runTaskTimer( core, 20 * 10, this.getUpdatesPerTick() );

			core.msg.info( "dynmap bridge enabled." );
			this.enable = true;
		}
		return true;
	}

	public void destroy(){
		if ( this.mset != null ){
			this.mset.deleteMarkerSet();
			this.mset = null;
		}

		for( AreaMarker am: this.resAreas.values() ){
			am.deleteMarker();
		}
		this.resAreas.clear();

		for( Marker m: this.resMarker.values() ){
			m.deleteMarker();
		}
		this.resMarker.clear();

		if ( this.updateEvent != null ){
			this.updateEvent.cancel();
			this.updateEvent.destroy();
			this.updateEvent = null;
		}

		core.msg.warning( "dynmap bridge disabled." );
		this.enable = false;
		this.enable = false;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	public int getUpdatesPerTick(){
		return core.cfg.getInteger( "dynmap.updatesPerTick", this.updatesPerTick );
	}

	public void updateArea(){
		if ( ! this.enable ) return;

		this.plots.clear();
		this.plots_idx.clear();

		if ( ! this.checkPlotData() ) return;

		this.plots_tbl.clear();
		this.points.clear();
		this.points_idx.clear();
		this.owner_name.clear();

		core.msg.debug( this.getClass().getSimpleName(), "updateArea", "createEdges" );
		this.createEdges();

		core.msg.debug( this.getClass().getSimpleName(), "updateArea", "createArea" );
		this.createArea();

		core.msg.debug( this.getClass().getSimpleName(), "updateArea", "drawArea" );
		this.drawArea();

		core.msg.debug( this.getClass().getSimpleName(), "updateArea", "drawMarker" );
		this.drawMarker();
	}

	/**
	 * PlotDataを読み込みつつ、更新されたか判定
	 * @return true = 更新された
	 */
	private boolean checkPlotData(){
		long	update = 0;
		// 全PlotDataを取得
		for ( PlotData pw: core.town.getPlotDatas() ){
			if ( update < pw.getUpdate() ) update = pw.getUpdate();

			this.plots.put( pw.getChunk().toFixKey(), pw );
			this.plots_idx.put( pw.getChunk().toFixKey(), -1 );
		}

		// 全TownDataを確認
		for ( TownData tw: core.town.getTownDatas() ){
			if ( update < tw.getUpdate() ) update = tw.getUpdate();
		}

		core.msg.debug( this.getClass().getSimpleName(), "checkPlotData", "update: " + this.UpdateDate + "->" + update );

		if ( this.UpdateDate < update ) {
			this.UpdateDate = update;
			core.msg.debug( this.getClass().getSimpleName(), "checkPlotData", "update size=" + this.plots.size() );
			return true;
		}else{
			core.msg.debug( this.getClass().getSimpleName(), "checkPlotData", "not update");
			return false;
		}
	}

	private void drawArea(){
		String	markerid;
		String	name;
		double[] x = null;
		double[] z = null;
		World	w;
		AreaMarker m;
		TownData	td;

		for( AreaMarker am: this.resAreas.values() ){
			am.deleteMarker();
		}
		this.resAreas.clear();

		// Areaグループ毎に処理
		for ( int group = 0; group < this.points.size(); group++ ){
			x = new double[ this.points.get(group).size() ];
			z = new double[ this.points.get(group).size() ];
			w = null;

			for ( int i = 0; i < this.points.get(group).size(); i++ ){
				if ( w == null ) w = this.points.get(group).get(i).getWorld();
				x[i] = this.points.get(group).get(i).getBlockX();
				z[i] = this.points.get(group).get(i).getBlockZ();
			}

			markerid = w.getName() + "_" + group;
			name = markerid;
			//name = core.cfg.getString( "dynmap.baloon" );

			m = mset.createAreaMarker( markerid, markerid, false, w.getName(), x, z, false );
			if ( m != null ){
				td = TUtils.getTownData( this.owner_name.get(group) );
				if ( td != null ) name = td.dynmap_info();
				m.setDescription( name );
				m.setFillStyle( this.fillOpacity, this.fillColor );
				m.setLineStyle( this.lineWeight, this.lineOpacity, this.lineColor );
			}

			core.msg.debug( this.getClass().getSimpleName(), "drawArea", markerid + " " + this.points.get(group).size() );
			this.resAreas.put( markerid, m);
		}
	}

	private void drawMarker(){
		Location	loc;
		String		markid;

		for( Marker m: this.resMarker.values() ){
			m.deleteMarker();
		}
		this.resMarker.clear();

		for ( TownData td: core.town.getTownDatas() ){
			loc = td.gethome();
			markid = td.getOwner();

			if ( loc != null ){
				this.resMarker.put(
						markid + "_home"
						, mset.createMarker( markid + "_home", markid, loc.getWorld().getName(), loc.getX(), loc.getY(), loc.getZ(), this.homeMarker, false)
						);
			}
		}
	}

	private void createArea(){
		int			group;
		PlotData	pd;

		for ( group = 0; group < this.plots_tbl.size(); group++ ){
			// グループ毎

			// plots_tblを並び替え→ckey
			List<String> ckey = new ArrayList<String>( this.plots_tbl.get(group) );
			Collections.sort( ckey );

			// ポイントデータの器を作成
			this.points.add( new ArrayList<Location>() );

			// ポイント一時記録用indexを初期化
			this.points_idx.clear();

			// 初めのChunkを取得
			pd = this.plots.get( ckey.get( ckey.size() - 1 ) );
			if ( core.isDebug() ){
				core.msg.debug( this.getClass().getSimpleName(), "createArea", "fast=" + ckey.get( ckey.size() - 1 ) + " list=" +Utils.join( " ", ckey) );
			}

			this.owner_name.add( pd.getOwner() );

			if ( Bukkit.getWorld( pd.getChunk().getWorldName() ) != null ){
				this.createLines( group, Bukkit.getWorld( pd.getChunk().getWorldName() ), pd, DMLPOS.RB );
			}

			core.msg.debug( this.getClass().getSimpleName(), "createArea", "group=" + group + " size=" + this.points.get(group).size() );
		}
	}

	private void createLines( int group, World w, PlotData pd,DMLPOS pos ){
		ChunkLoc	cl;
		PlotData	wp,cp,plot;
		Location	loc;
		Location	loc_start = null;
		int			loop = 1000;

		plot = pd;

		//      -Z
		//      ↑
		// -X ←  → +X
		//      ↓
		//      +Z


		while ( loop > 0 ){
			cl = new ChunkLoc( plot.getChunk() );

			switch ( pos ){
			case RT:
				loc = new Location( w, cl.getX() * 16 + 16, 0, cl.getZ() * 16 );
				core.msg.debug( "group=" + group + " loop=" + loop + " chunk" + cl.toString() + " pos=" + pos.toString() + " loc=" + Utils.formatLocation(loc) );

				if ( loc.equals( loc_start ) ) {
					// 開始地点→ループから抜ける
					loop = -1;
					continue;
				}
				/*
				if ( this.points_idx.containsKey( this.fixLoc( loc ) ) ){
					// 次のポイントはすでに選択済み→ループから抜ける
					loop = -1;
					continue;
				}
				*/

				// ポイント登録
				this.points.get(group).add( loc );
				this.points_idx.put( this.fixLoc(loc), false );
				if ( loc_start == null ) loc_start = loc;

				// 次のポイントを探す(右)
				wp = this.existNearPlot( group, plot, 1, 0 );	// 右
				if ( wp == null ){ pos = DMLPOS.RB; continue; }	// ↓

				// 次のポイントを探す(右上)
				cp = wp;
				wp = this.existNearPlot( group, plot, 1, -1 );
				if ( wp == null ){ plot = cp; pos = DMLPOS.RT; continue; }					// →

				// 次のポイントを探す(上)
				cp = wp;
				wp = this.existNearPlot( group, plot, 0, -1 );
				if ( wp == null ){ plot = cp; pos = DMLPOS.LT; continue; }					// ↑

				// 行き止まり
				loop = -1;
				core.msg.debug( "-error?-" );
				break;

			case RB:
				loc = new Location( w, cl.getX() * 16 + 16, 0, cl.getZ() * 16 + 16 );
				core.msg.debug( "group=" + group + " loop=" + loop + " chunk" + cl.toString() + " pos=" + pos.toString() + " loc=" + Utils.formatLocation(loc) );

				if ( loc.equals( loc_start ) ) {
					// 開始地点→ループから抜ける
					loop = -1;
					continue;
				}
				/*
				if ( this.points_idx.containsKey( this.fixLoc( loc ) ) ){
					// 次のポイントはすでに選択済み→ループから抜ける
					loop = -1;
					continue;
				}
				*/

				// ポイント登録
				this.points.get(group).add( loc );
				this.points_idx.put( this.fixLoc(loc), false );
				if ( loc_start == null ) loc_start = loc;

				// 次のポイントを探す(下)
				wp = this.existNearPlot( group, plot, 0, 1 );	// 下
				if ( wp == null ){ pos = DMLPOS.LB; continue; }	// ←

				// 次のポイントを探す(右下)
				cp = wp;
				wp = this.existNearPlot( group, plot, 1, 1 );
				if ( wp == null ){ plot = cp; pos = DMLPOS.RB; continue; }					// ↓

				// 次のポイントを探す(右)
				cp = wp;
				wp = this.existNearPlot( group, plot, 1, 0 );
				if ( wp == null ){ plot = cp; pos = DMLPOS.RT; continue; }					// →

				// 行き止まり
				loop = -1;
				core.msg.debug( "-error?-" );
				break;

			case LB:
				loc = new Location( w, cl.getX() * 16, 0, cl.getZ() * 16 + 16 );
				core.msg.debug( "group=" + group + " loop=" + loop + " chunk" + cl.toString() + " pos=" + pos.toString() + " loc=" + Utils.formatLocation(loc) );

				if ( loc.equals( loc_start ) ) {
					// 開始地点→ループから抜ける
					loop = -1;
					continue;
				}
				/*
				if ( this.points_idx.containsKey( this.fixLoc( loc ) ) ){
					// 次のポイントはすでに選択済み→ループから抜ける
					loop = -1;
					continue;
				}
				*/

				// ポイント登録
				this.points.get(group).add( loc );
				this.points_idx.put( this.fixLoc(loc), false );
				if ( loc_start == null ) loc_start = loc;

				// 次のポイントを探す(左)
				wp = this.existNearPlot( group, plot, -1, 0 );	// 左
				if ( wp == null ){ pos = DMLPOS.LT; continue; }	// ↑

				// 次のポイントを探す(左下)
				cp = wp;
				wp = this.existNearPlot( group, plot, -1, 1 );	// 左下
				if ( wp == null ){ plot = cp; pos = DMLPOS.LB; continue; }					// ←

				// 次のポイントを探す(下)
				cp = wp;
				wp = this.existNearPlot( group, plot, 0, 1 );
				if ( wp == null ){ plot = cp; pos = DMLPOS.RB; continue; }					// ↓

				// 行き止まり
				loop = -1;
				core.msg.info( "-error?-" );
				break;

			case LT:
				loc = new Location( w, cl.getX() * 16, 0, cl.getZ() * 16 );
				core.msg.debug( "group=" + group + " loop=" + loop + " chunk" + cl.toString() + " pos=" + pos.toString() + " loc=" + Utils.formatLocation(loc) );

				if ( loc.equals( loc_start ) ) {
					// 開始地点→ループから抜ける
					loop = -1;
					continue;
				}
				/*
				if ( this.points_idx.containsKey( this.fixLoc( loc ) ) ){
					// 次のポイントはすでに選択済み→ループから抜ける
					loop = -1;
					continue;
				}
				 */
				// ポイント登録
				this.points.get(group).add( loc );
				this.points_idx.put( this.fixLoc(loc), false );
				if ( loc_start == null ) loc_start = loc;

				// 次のポイントを探す(上)
				wp = this.existNearPlot( group, plot, 0, -1 );	// 上
				if ( wp == null ){ pos = DMLPOS.RT; continue; }	// →

				// 次のポイントを探す(左上)
				cp = wp;
				wp = this.existNearPlot( group, plot, -1, -1 );	// 左上
				if ( wp == null ){ plot = cp; pos = DMLPOS.LT; continue; }					// ↑

				// 次のポイントを探す(左)
				cp = wp;
				wp = this.existNearPlot( group, plot, -1, 0 );
				if ( wp == null ){ plot = cp; pos = DMLPOS.LB; continue; }					// ←

				// 行き止まり
				loop = -1;
				core.msg.info( "-error?-" );
				break;

			}

			loop--;
		}
		core.msg.debug( "-end-" );
	}

	private String fixLoc( Location loc ){
		return String.format( "%06d", loc.getBlockX() ) + "_" + String.format( "%06d", loc.getBlockZ() );
	}

	private PlotData existNearPlot( int group, PlotData plot, int addx, int addz ){
		ChunkLoc	cl = new ChunkLoc( plot.getChunk() );

		cl.addX( addx );
		cl.addZ( addz );

		if ( this.plots_idx.get( cl.toFixKey() ) == null ) {
			//core.msg.info( " plots_idx:" +  cl.toFixKey() + " is null." );
			return null;
		}

		if ( this.plots_idx.get( cl.toFixKey() ) == group ){
			return this.plots.get( cl.toFixKey() );
		}
		//core.msg.info( " plots_idx:" +  this.plots_idx.get( cl.toFixKey() ) + " is other group." );
		return null;
	}

	private void createEdges(){
		int			group;

		// Plotsをグルーピング
		while( true ){
			// 未チェックポイントを抽出
			PlotData pbase = this.ges_getNewPlot();
			if ( pbase == null ) break;

			group = this.plots_tbl.size();
			this.plots_tbl.add( new ArrayList<String>() );
			this.ges_checkNearPlot( pbase, group, null );

			core.msg.debug( this.getClass().getSimpleName(), "createEdges", "group=" + group + " size=" + this.plots_tbl.get(group).size() );
			group++;
		}
	}

	private PlotData ges_getNewPlot(){
		List<String> ckey = new ArrayList<String>( this.plots.keySet() );
		Collections.sort( ckey );

		for ( String p: ckey ){
			if ( this.plots_idx.get( p ) == -1 ){
				return this.plots.get( p );
			}
		}
		return null;
	}

	private void ges_checkNearPlot( PlotData pd, int group, ChunkLoc cl ){
		PlotData	wp;
		ChunkLoc	wc;

		if ( cl != null ){
			// 隣接を確認
//			core.msg.info( "  check:" + cl.toString() );
			if ( ! this.plots_idx.containsKey( cl.toFixKey() ) ) return;			// 近くに無い
			if ( this.plots_idx.get( cl.toFixKey() ) != -1 ) return;					// 確認済み
			wp = this.plots.get( cl.toFixKey() );
			if ( ! pd.getOwner().equalsIgnoreCase( wp.getOwner() ) ) return;	// 別オーナ
		}else{
			// 登録だけ
			wp = this.plots.get( pd.getChunk().toFixKey() );
		}

		// 対象に追加
		this.plots_idx.put( wp.getChunk().toFixKey(), group );
		this.plots_tbl.get(group).add( wp.getChunk().toFixKey() );

		//core.msg.info( "o=" + wp.getOwner() + " g=" + group + " c:" + wp.getChunk() );

		// 対象の周りを確認
		wc = new ChunkLoc( wp.getChunk() );
		wc.addX( 1 );
		this.ges_checkNearPlot( wp, group, wc );

		wc = new ChunkLoc( wp.getChunk() );
		wc.addX( -1 );
		this.ges_checkNearPlot( wp, group, wc );

		wc = new ChunkLoc( wp.getChunk() );
		wc.addZ( 1 );
		this.ges_checkNearPlot( wp, group, wc );

		wc = new ChunkLoc( wp.getChunk() );
		wc.addZ( -1 );
		this.ges_checkNearPlot( wp, group, wc );
	}
}
