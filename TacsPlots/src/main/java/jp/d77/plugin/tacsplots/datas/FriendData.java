package jp.d77.plugin.tacsplots.datas;

import java.util.ArrayList;
import java.util.HashMap;

import jp.d77.plugin.tacsplots.TacsPlots;
import jp.d77.plugin.tacsplots.datas.Consts.FRIEND_TYPE;
import jp.d77.plugin.tputils.Utils;
import jp.d77.plugin.tputils.kvs.KeyValue;

public class FriendData {
	protected static TacsPlots core = TacsPlots.plugin;

	protected HashMap<PlayerData,FRIEND_TYPE>	friends			= new HashMap<PlayerData,FRIEND_TYPE>();
	public FriendData(){

	}

	/**
	 * 追加
	 * @param player_name
	 * @param pt
	 * @return
	 */
	public void add( String player_name, FRIEND_TYPE pt ){
		core.msg.debug( this.getClass().getSimpleName(), "add", player_name + " " + pt.toString()  );

		PlayerData pd = this.getPlayerData(player_name);

		if ( pd != null ) this.friends.put( pd, pt );
		else this.friends.put( new PlayerData( player_name ), pt );
	}

	/**
	 * 削除
	 * @param player_name
	 * @return
	 */
	public boolean remove( String player_name ){
		PlayerData pd = this.getPlayerData(player_name);
		if ( pd != null ) {
			core.msg.debug( this.getClass().getSimpleName(), "remove", player_name );
			this.friends.remove( pd );
			return true;
		}
		return false;
	}

	/**
	 * プレイヤーデータを取得
	 * @param player_name
	 * @return
	 */
	public PlayerData getPlayerData( String player_name ){
		for ( PlayerData pd: this.friends.keySet() ){
			if ( pd.getName().equalsIgnoreCase( player_name ) ){
				return pd;
			}
		}
		return null;
	}

	/**
	 * プレイヤータイプを取得
	 * @param player_name
	 * @return
	 */
	public FRIEND_TYPE getPlayerType( String player_name ){
		PlayerData pd = this.getPlayerData(player_name);
		if ( pd != null ) {
			return this.friends.get( pd );
		}
		return FRIEND_TYPE.GUEST;
	}

	public ArrayList<String> getFriends( FRIEND_TYPE pt ){
		ArrayList<String>	ret = new ArrayList<String>();

		for( PlayerData pd: this.friends.keySet() ){
			if ( pt == null ){
				ret.add( pd.getName() );
			}else if ( pt.equals( FRIEND_TYPE.OWNER ) && this.friends.get(pd).equals( FRIEND_TYPE.OWNER ) ){
				ret.add( pd.getName() );
			}else if ( pt.equals( FRIEND_TYPE.FRIEND ) && this.friends.get(pd).equals( FRIEND_TYPE.FRIEND ) ){
				ret.add( pd.getName() );
			}
		}
		return ret;
	}

	/**
	 * リスト
	 * @param pt
	 * @return
	 */
	public String info( FRIEND_TYPE pt ){
		ArrayList<String>	ret = new ArrayList<String>();

		for( PlayerData f: this.friends.keySet() ){
			if ( this.friends.get(f) == pt ) ret.add( f.name );
		}

		return Utils.join( ",", ret );
	}

	public String info(){
		ArrayList<String>	ret = new ArrayList<String>();

		for( PlayerData f: this.friends.keySet() ){
			if ( this.friends.get( f ).equals( FRIEND_TYPE.OWNER ) ){
				ret.add( f.name + "&e(" + core.msg.template("MESSAGE.LANG.SUBOWNER") + ")&r" );
			}else{
				ret.add( f.name + "&e(" + core.msg.template("MESSAGE.LANG.FRIEND") + ")&r" );
			}
		}

		return Utils.join( ",", ret );
	}

	/**
	 * 保存
	 * @param kvs
	 * @param root
	 */
	public void save( KeyValue kvs, String root ){
		int cnt = 1;
		for ( PlayerData pd: this.friends.keySet() ){
			pd.save( kvs, root + "." + this.friends.get(pd).toString() + "." + cnt );
			cnt ++;
		}
	}

	/**
	 * 読み込み
	 * @param kvs
	 * @param root
	 */
	public void load( KeyValue kvs, String root ){
		this.friends.clear();

		for ( String s: kvs.getKeys( root + ".OWNER" ) ){
			core.msg.debug( root + ".OWNER." + s );
			this.friends.put( new PlayerData( kvs, root + ".OWNER." + s ) , FRIEND_TYPE.OWNER);
		}

		for ( String s: kvs.getKeys( root + ".FRIEND" ) ){
			core.msg.debug( root + ".FRIEND." + s );
			this.friends.put( new PlayerData( kvs, root + ".FRIEND." + s ) , FRIEND_TYPE.FRIEND);
		}
	}
}

