package jp.d77.plugin.tputils.message;

import org.bukkit.scheduler.BukkitRunnable;

public class MessageRunner extends BukkitRunnable {

	protected boolean	enable = false;
	protected Message	msg;

	public boolean init( Message msg ){
		this.msg = msg;
		enable = true;
		return true;
	}

	public boolean destroy(){
		enable = false;
		return true;
	}

	public MessageRunner(){
	}

	@Override
	public void run() {
		if ( ! enable ) return;
		if ( this.msg != null ) this.msg.sendStackedMessage();
	}

}
