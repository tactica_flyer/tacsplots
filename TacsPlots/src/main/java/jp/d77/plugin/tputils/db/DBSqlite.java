package jp.d77.plugin.tputils.db;

public class DBSqlite extends DBCore{
	private String		mFilePath = "";

	public DBSqlite(){
		super();
	}

	/**
	 * Initializer
	 */
	public boolean init( String FilePath ){
		boolean ret = super.init();

		if ( !ret ) return ret;

		super.mDbType			= "sqlite";
		this.mFilePath			= FilePath;
		super.mConnectionStr	= "jdbc:sqlite:" + mFilePath;
		super.mDBClass			= "org.sqlite.JDBC";

		return ret;
	}

	/**
	 * Destractor
	 */
	public void destroy(){
		super.destroy();
	}

	/**
	 * DBとセッションを貼る
	 * @return
	 */
	public boolean dbopen(){
		boolean ret = false;

		ret = super.dbopen();
		return ret;
	}

	/**
	 * DBセッションを閉じる
	 */
	public void dbclose(){

	}

}
