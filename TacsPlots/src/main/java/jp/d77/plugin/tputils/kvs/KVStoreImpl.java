package jp.d77.plugin.tputils.kvs;

public interface KVStoreImpl {
	public enum KVSModeType{
		AUTO_BACKUP
	}

	public void setTable( String table );

	public void save();
	public void load();
	public void clear();

	public void setDebug( boolean b );

	public void setString( String key, String value );
	public void setInteger( String key, Integer value );
	public void setLong( String key, Long value );
	public void setFloat( String key, Float value );
	public void setDouble( String key, Double value );
	public void setBoolean( String key, Boolean value );

	public void setMode( KVSModeType mode, Boolean data );

	public String getString( String key );
	public Integer getInteger( String key );
	public Long getLong( String key );
	public Double getDouble( String key );
	public Boolean getBoolean( String key );
	public String[] getKeys( String key );
}
