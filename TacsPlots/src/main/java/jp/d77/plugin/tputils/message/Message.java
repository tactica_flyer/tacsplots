package jp.d77.plugin.tputils.message;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import jp.d77.plugin.tputils.CmdData;
import jp.d77.plugin.tputils.Utils;
import jp.d77.plugin.tputils.config.ConfigBase;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Message {
	public enum LOG_LEVEL {
		DEBUG, INFO, WARNING, ERROR;
	}
	public enum MESSAGE_TYPE {
		NORMAL, NO_DOBLING, GETSTRING, PAGING, THREW, BROADCAST
	}

	private	LinkedList<MessageData>		msgs;
	private	HashMap<Player,MessageData>	pmsgs;
	private	MessageRunner				grHelp;

	private	Logger						logger;
	private	ConfigBase					cfg;
	private	JavaPlugin					plugin;

	private	boolean mDebug	= false;

	public Message( JavaPlugin plugin, ConfigBase cfg ){
		this.plugin		= plugin;
		this.cfg		= cfg;
		this.logger		= plugin.getLogger();

		grHelp		= new MessageRunner();
		grHelp.runTaskTimer( plugin, 20, 4 );
	}

	public boolean init(){
		//this.cfg = this.plugin.c
		this.setDefault( "MESSAGE.WORD.NONE", "NULL" );
		this.setDefault( "MESSAGE.MAIN.PREFIX", "[" + plugin.getName() + "]" );
		this.setDefault( "MESSAGE.PAGING.ERROR", "No messages." );
		this.setDefault( "MESSAGE.PAGING.NEXT", "次へ" );
		this.setDefault( "MESSAGE.PAGING.PREVIOUS", "前へ" );
		this.setDefault( "MESSAGE.PAGING.NUMBER", "ページ番号" );
		new CmdData().setDefault(this);

		msgs = new LinkedList<MessageData>();
		grHelp.init( this );
		return true;
	}

	public boolean destroy(){
		grHelp.destroy();
		msgs = null;
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	public void setDefault( String k, String v ){
		this.cfg.setDefault( k, v );
	}

	protected void info( String method, String msg ) {
		this.info( "[" + this.getClass().getSimpleName() + "." + method + "] " + msg );
	}
	protected void debug( String method, String msg ) {
		this.debug( this.getClass().getSimpleName(), method,msg );
	}


	// ---------------------------------------------------------------------------------------------------------------
	// Methods
	// ---------------------------------------------------------------------------------------------------------------
	public String getConsoleMsg( String path, String... args ){
		if ( ! cfg.isDefined( "MESSAGE." + path ) ){
			//this.debug("getPlayerMsg", "Not defined: " + "MESSAGE." + path );
		}
		String msg = this.cfg.getStringJoin( "MESSAGE." + path );
		if ( args != null ) msg = this.template( msg, args );
		msg = this.changeColorCode( msg, true );
		return msg;
	}

	public String getPlayerMsg( String path, String... args ){
		if ( ! this.cfg.isDefined( "MESSAGE." + path ) ){
			//this.debug("getPlayerMsg", "Not defined: " + "MESSAGE." + path );
		}
		String msg = this.cfg.getStringJoin( "MESSAGE." + path );
		if ( args != null ) msg = this.template( msg, args );
		msg = this.changeColorCode( msg, false );
		return msg;
	}

	/**
	 * コンソールログ出力
	 * @param c message type ... Const.DEBUG,Const.Error,Const.WORN,Const.INFO
	 * @param s message string
	 */
	public void log( LOG_LEVEL level,String s) {
		switch ( level ){
		case DEBUG:		debug( s );
		case WARNING:	warning( s );
		case ERROR:		error( s );
		case INFO:		info( s ); break;
		}
	}

	/**
	 * output console log(Debug)
	 * @param class_name ... this.getClass().getSimpleName()
	 * @param method
	 * @param msg
	 */
	public void debug( String class_name, String method, String msg ) {
		setMessage( LOG_LEVEL.DEBUG, MESSAGE_TYPE.THREW, null, "[" + class_name + "." + method + "] " + msg );
	}
	public void debug( String msg ) {
		setMessage( LOG_LEVEL.DEBUG, MESSAGE_TYPE.THREW, null, msg );
	}

	/**
	 * output console log(Error)
	 * @param s message string
	 */
	public void error( String s ) {
		setMessage( LOG_LEVEL.ERROR, MESSAGE_TYPE.THREW, null, s );
	}

	/**
	 * output console log(Info)
	 * @param s message string
	 */
	public void info( String s ) {
		setMessage( LOG_LEVEL.INFO, MESSAGE_TYPE.THREW, null, s );
	}

	/**
	 * output console log(Info)
	 * @param s message string
	 */
	public void warning( String s ) {
		setMessage( LOG_LEVEL.WARNING, MESSAGE_TYPE.THREW, null, s );
	}

	public String null2None( String s ){
		if ( s == null ) return this.cfg.getString( "MESSAGE.WORD.NONE" );
		if ( s.equals("") ) return this.cfg.getString( "MESSAGE.WORD.NONE" );
		return s;
	}

	public String template( String msgs, String... args ){
		if ( this.cfg.isDefined( msgs ) ){
			msgs = this.cfg.getStringJoin( msgs );

		}else if ( this.cfg.isDefined( "MESSAGE." + msgs ) ){
			msgs = this.cfg.getStringJoin( "MESSAGE." + msgs );

		}else{
			// String
		}

		if ( args != null ){
			for ( int i = args.length - 1 ; i >= 0 ; i-- ){
				//core.msgs.debug("Messages", "template", msgs);
				if ( args[i] == null ){
					msgs = msgs.replace("$" + (i+1) , "null" );
				}else{
					msgs = msgs.replace("$" + (i+1) , args[i] );
				}
			}
		}
		return msgs;
	}

	public MCOLOR ColorCode2MCOLOR( String c ){
		switch ( c ){
		case "&0":	return MCOLOR.BLACK;
		case "&1":	return MCOLOR.DARK_BLUE;
		case "&2":	return MCOLOR.DARK_GREEN;
		case "&3":	return MCOLOR.DARK_AQUA;
		case "&4":	return MCOLOR.DARK_RED;
		case "&5":	return MCOLOR.DARK_PURPLE;
		case "&6":	return MCOLOR.GOLD;
		case "&7":	return MCOLOR.GRAY;
		case "&8":	return MCOLOR.DARK_GLAY;
		case "&9":	return MCOLOR.INDIGO;
		case "&a":	return MCOLOR.GREEN;
		case "&b":	return MCOLOR.AQUA;
		case "&c":	return MCOLOR.RED;
		case "&d":	return MCOLOR.PINK;
		case "&e":	return MCOLOR.YELLOW;
		case "&f":	return MCOLOR.WHITE;

		case "&k":	return MCOLOR.OBFUSCATED;
		case "&l":	return MCOLOR.BOLD;
		case "&m":	return MCOLOR.STRIKETHROUGH;
		case "&n":	return MCOLOR.UNDER;
		case "&o":	return MCOLOR.ITALIC;

		case "&r":	return MCOLOR.RESET;

		case "&BLACK&":			return MCOLOR.BLACK;
		case "&DARK_BLUE&":		return MCOLOR.DARK_BLUE;
		case "&DARK_GREEN&":	return MCOLOR.DARK_GREEN;
		case "&DARK_AQUA&":		return MCOLOR.DARK_AQUA;
		case "&DARK_CYAN&":		return MCOLOR.DARK_AQUA;
		case "&DARK_RED&":		return MCOLOR.DARK_RED;
		case "&DARK_PURPLE&":	return MCOLOR.DARK_PURPLE;
		case "&GOLD&":			return MCOLOR.GOLD;
		case "&ORANGE&":		return MCOLOR.GOLD;
		case "&GRAY&":			return MCOLOR.GRAY;
		case "&DARK_GLAY&":		return MCOLOR.DARK_GLAY;
		case "&INDIGO&":		return MCOLOR.INDIGO;
		case "&GREEN&":			return MCOLOR.GREEN;
		case "&AQUA&":			return MCOLOR.AQUA;
		case "&CYAN&":			return MCOLOR.AQUA;
		case "&RED&":			return MCOLOR.RED;
		case "&PINK&":			return MCOLOR.PINK;
		case "&YELLOW&":		return MCOLOR.YELLOW;
		case "&WHITE&":			return MCOLOR.WHITE;

		case "&OBFUSCATED&":	return MCOLOR.OBFUSCATED;
		case "&BOLD&":			return MCOLOR.BOLD;
		case "&STRIKETHROUGH&":	return MCOLOR.STRIKETHROUGH;
		case "&UNDER&":			return MCOLOR.UNDER;
		case "&ITALIC&":		return MCOLOR.ITALIC;

		case "&RESET&":			return MCOLOR.RESET;

		}
		return MCOLOR.RESET;
	}

	public String changeColorCode( String msgs, Boolean console ){
		String	args[] = {
				"&0"
				,"&1"
				,"&2"
				,"&3"
				,"&4"
				,"&5"
				,"&6"
				,"&7"
				,"&8"
				,"&9"
				,"&a"
				,"&b"
				,"&c"
				,"&d"
				,"&e"
				,"&f"
				,"&k"
				,"&l"
				,"&m"
				,"&n"
				,"&o"
				,"&r"
				,"&BLACK&"
				,"&DARK_BLUE&"
				,"&DARK_GREEN&"
				,"&DARK_AQUA&"
				,"&DARK_CYAN&"
				,"&DARK_RED&"
				,"&DARK_PURPLE&"
				,"&GOLD&"
				,"&ORANGE&"
				,"&GRAY&"
				,"&DARK_GLAY&"
				,"&INDIGO&"
				,"&GREEN&"
				,"&AQUA&"
				,"&CYAN&"
				,"&RED&"
				,"&PINK&"
				,"&YELLOW&"
				,"&WHITE&"

				,"&OBFUSCATED&"
				,"&BOLD&"
				,"&STRIKETHROUGH&"
				,"&UNDER&"
				,"&ITALIC&"

				,"&RESET&"

		};

		if ( msgs == null ) return null;
		for ( int i = 0 ; i < args.length ; i++ ){
			if ( console ){
				msgs = msgs.replace( args[i] , ColorCode2MCOLOR(args[i]).toConsole() );
			}else{
				msgs = msgs.replace( args[i] , ColorCode2MCOLOR(args[i]).toPlayer() );
			}
		}

		return msgs;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Setter
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * メッセージ表示
	 * @param level
	 * @param type
	 * @param s
	 * @param Message
	 * @param args
	 * @return
	 */
	public String setMessage( LOG_LEVEL level, MESSAGE_TYPE type, CommandSender s, String Message, String... args ){
		String msg = Message;
		if ( level == LOG_LEVEL.DEBUG && ! this.mDebug ){
			return "";
		}

		if ( this.cfg.isDefined( msg ) ){
			msg = this.cfg.getStringJoin( msg );

		}else if ( this.cfg.isDefined( "MESSAGE." + msg ) ){
			msg = this.cfg.getStringJoin( "MESSAGE." + msg );

		}else{
			// String
		}

		if ( msg == null ) msg = "";

		switch ( level ){
		case DEBUG:		msg = MCOLOR.AQUA.toColorCode() + "[DEBUG] " + msg; break;
		case WARNING:	msg = MCOLOR.YELLOW.toColorCode() + "[WARN] " + MCOLOR.RESET.toColorCode() + msg;break;
		case ERROR:		msg = MCOLOR.RED.toColorCode() + "[ERROR] " + MCOLOR.RESET.toColorCode() + msg;break;
		case INFO:break;
		}

		if ( args != null ) msg = this.template( msg, args );

		msg += MCOLOR.RESET.toColorCode();

		if ( type == MESSAGE_TYPE.BROADCAST ){
			msg = this.changeColorCode( MCOLOR.PINK.toColorCode() + msg, false );

			this.plugin.getServer().broadcastMessage( this.getPlayerMsg( "MAIN.PREFIX") + msg );

		}else if ( s == null || Utils.isConsole( s ) ){
			msg = this.changeColorCode( msg, true );

			if ( type == MESSAGE_TYPE.THREW ){
				Level l = Level.INFO;
				switch ( level ){
				case DEBUG:		l = Level.INFO; break;
				case WARNING:	l = Level.WARNING; break;
				case ERROR:		l = Level.SEVERE; break;
				case INFO:		l = Level.INFO; break;
				}
				this.logger.log( l, this.getConsoleMsg( "MAIN.PREFIX") + msg );
			}else if ( type == MESSAGE_TYPE.NORMAL ){
				msgs.offer( new MessageData( this.plugin.getServer().getConsoleSender(), level, this.getConsoleMsg( "MAIN.PREFIX") + msg ) );
			}

		}else{
			msg = this.changeColorCode( msg, false );

			if ( type == MESSAGE_TYPE.PAGING ){
				pmsgs.put( (Player)s , new MessageData( (Player)s, this.getPlayerMsg( "MAIN.PREFIX") + msg ) );
				pmsgs.get( (Player)s ).enablePaging();
				pmsgs.get( (Player)s ).sendPageMessage( this.cfg );

			}else if ( type == MESSAGE_TYPE.NO_DOBLING ){
				for ( MessageData md: msgs ){
					if ( md.getPlayer() != null ){
						if ( md.getPlayer().equals( (Player)s ) ){
							if ( md.getMessage().equals( this.getPlayerMsg( "MAIN.PREFIX") + msg ) ){
								return msg;
							}
						}
					}
				}
				msgs.offer( new MessageData( (Player)s , this.getPlayerMsg( "MAIN.PREFIX") + msg ) );

			}else if ( type == MESSAGE_TYPE.NORMAL ){
				msgs.offer( new MessageData( (Player)s , this.getPlayerMsg( "MAIN.PREFIX") + msg ) );

			}else if ( type == MESSAGE_TYPE.THREW ){
				((Player)s).sendMessage( this.getPlayerMsg( "MAIN.PREFIX") + msg );
			}
		}

		return msg;
	}

	public void setDebug( boolean debug ){
		this.mDebug = debug;
	}
	// ---------------------------------------------------------------------------------------------------------------
	// Getter
	// ---------------------------------------------------------------------------------------------------------------

	// ---------------------------------------------------------------------------------------------------------------
	// Adder
	// ---------------------------------------------------------------------------------------------------------------

	public void addMessage( UUID uuid, String Msg, String... args ){
		if ( Utils.getExistUUID( uuid ) == null ) return;
		setMessage( LOG_LEVEL.INFO, MESSAGE_TYPE.NORMAL, Utils.getExistUUID( uuid ) , Msg, args );
	}

	public void addMessage( Player p, String Msg, String... args ){
		setMessage( LOG_LEVEL.INFO, MESSAGE_TYPE.NORMAL, p, Msg, args );
	}

	public void addMessage( MESSAGE_TYPE type, Player p, String Msg, String... args ){
		setMessage( LOG_LEVEL.INFO, type, p, Msg, args );
	}

	public void addMessage( CommandSender s, String Msg, String... args ){
		setMessage( LOG_LEVEL.INFO, MESSAGE_TYPE.NORMAL, s, Msg, args );
	}

	/**
	 * メッセージ表示
	 */
	public void sendStackedMessage(){
		if ( msgs == null ) return;
		MessageData m = msgs.poll();
		if ( m == null ) return;
		m.sendMessage( this );
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Commands
	// ---------------------------------------------------------------------------------------------------------------
	public boolean cmdPaging( CmdData cmddata ){

		// 基本的なコマンドチェック
		cmddata.setPlayerCommand(true);
		if ( cmddata.getArgs()[0].equalsIgnoreCase("next") ){
			cmddata.setPermission("tacstowns.player.page.next");

		}else if ( cmddata.getArgs()[0].equalsIgnoreCase("previous") ){
			cmddata.setPermission("tacstowns.player.page.previous");

		}else if ( Utils.isNumber( cmddata.getArgs()[0] ) ){
			cmddata.setPermission("tacstowns.player.page.number");

		}else{
			return false;
		}

		if ( ! cmddata.checkExec( this ) ) {
			this.addMessage( cmddata.getPlayer().getUniqueId(), "MESSAGE.PAGING.ERROR" );
			return true;
		}

		if ( ! pmsgs.containsKey( cmddata.getPlayer() ) ){
			// 表示可能なメッセージが無い
			this.addMessage( cmddata.getPlayer().getUniqueId(), "MESSAGE.PAGING.ERROR" );
			return true;
		}

		if ( cmddata.getArgs()[0].equalsIgnoreCase("next") ){
			cmdPageNext( cmddata );

		}else if ( cmddata.getArgs()[0].equalsIgnoreCase("previous") ){
			cmdPagePrevious( cmddata );

		}else if ( Utils.isNumber( cmddata.getArgs()[0] ) ){
			cmdPageNo( cmddata );

		}
		return true;
	}

	public void cmdPageNext( CmdData cmddata ){
		pmsgs.get( cmddata.getPlayer() ).nextPage();
		pmsgs.get( cmddata.getPlayer() ).sendPageMessage( this.cfg );
	}

	public void cmdPagePrevious( CmdData cmddata ){
		pmsgs.get( cmddata.getPlayer() ).previousPage();
		pmsgs.get( cmddata.getPlayer() ).sendPageMessage( this.cfg );
	}

	public void cmdPageNo( CmdData cmddata ){
		pmsgs.get( cmddata.getPlayer() ).setPage( Integer.parseInt( cmddata.getArgs()[0] ) );
		pmsgs.get( cmddata.getPlayer() ).sendPageMessage( this.cfg );
	}

}
