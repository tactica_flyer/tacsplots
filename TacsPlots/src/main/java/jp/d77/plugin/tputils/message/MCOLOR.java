package jp.d77.plugin.tputils.message;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public enum MCOLOR {
	BLACK,
	DARK_BLUE,
	DARK_GREEN,
	DARK_AQUA,
	DARK_RED,
	DARK_PURPLE,
	GOLD,
	GRAY,
	DARK_GLAY,
	INDIGO,
	GREEN,
	AQUA,
	RED,
	PINK,
	YELLOW,
	WHITE,
	OBFUSCATED,
	BOLD,
	STRIKETHROUGH,
	UNDER,
	ITALIC,
	RESET;

	public String toSenderCase( CommandSender sender ){
		if ((sender instanceof ConsoleCommandSender)) {
			return toConsole();
		}else{
			return toPlayer();
		}
	}

	public String toConsole(){
		switch ( this ){
		case BLACK:			return "\u001B[30m";
		case DARK_BLUE:		return "\u001B[34m";
		case DARK_GREEN:	return "\u001B[32m";
		case DARK_AQUA:		return "\u001B[36m";
		case DARK_RED:		return "\u001B[31m";
		case DARK_PURPLE:	return "\u001B[35m";
		case GOLD:			return "\u001B[33m";
		case GRAY:			return "\u001B[37m";
		case DARK_GLAY:		return "\u001B[30m";
		case INDIGO:		return "\u001B[34m";
		case GREEN:			return "\u001B[32m";
		case AQUA:			return "\u001B[36m";
		case RED:			return "\u001B[31m";
		case PINK:			return "\u001B[35m";
		case YELLOW:		return "\u001B[33m";
		case WHITE:			return "\u001B[37m";

		case OBFUSCATED:	return "";
		case BOLD:			return "";
		case STRIKETHROUGH:	return "";
		case UNDER:			return "";
		case ITALIC:		return "";

		case RESET:			return "\u001B[0m";
		}
		return "";
	}

	public String toPlayer(){
		switch ( this ){
		case BLACK:			return "§0";
		case DARK_BLUE:		return "§1";
		case DARK_GREEN:	return "§2";
		case DARK_AQUA:		return "§3";
		case DARK_RED:		return "§4";
		case DARK_PURPLE:	return "§5";
		case GOLD:			return "§6";
		case GRAY:			return "§7";
		case DARK_GLAY:		return "§8";
		case INDIGO:		return "§9";
		case GREEN:			return "§a";
		case AQUA:			return "§b";
		case RED:			return "§c";
		case PINK:			return "§d";
		case YELLOW:		return "§e";
		case WHITE:			return "§f";

		case OBFUSCATED:	return "§k";
		case BOLD:			return "§l";
		case STRIKETHROUGH:	return "§m";
		case UNDER:			return "§n";
		case ITALIC:		return "§o";

		case RESET:			return "§r";

		}
		return "";
	}

	public String toColorCode(){
		switch ( this ){
		case BLACK:			return "&0";
		case DARK_BLUE:		return "&1";
		case DARK_GREEN:	return "&2";
		case DARK_AQUA:		return "&3";
		case DARK_RED:		return "&4";
		case DARK_PURPLE:	return "&5";
		case GOLD:			return "&6";
		case GRAY:			return "&7";
		case DARK_GLAY:		return "&8";
		case INDIGO:		return "&9";
		case GREEN:			return "&a";
		case AQUA:			return "&b";
		case RED:			return "&c";
		case PINK:			return "&d";
		case YELLOW:		return "&e";
		case WHITE:			return "&f";

		case OBFUSCATED:	return "&k";
		case BOLD:			return "&l";
		case STRIKETHROUGH:	return "&m";
		case UNDER:			return "&n";
		case ITALIC:		return "&o";

		case RESET:			return "&r";

		}
		return "";
	}
}
