package jp.d77.plugin.tputils.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DBDataSets {
	public enum DBDataType{
		STRING,
		INTEGER,
		LONG,
		FLOAT,
		DOUBLE,
	}

	protected class DBDataItem{
		protected DBDataType		type;
		protected ArrayList<Object>	data;
		protected DBDataItem( DBDataType type ){
			this.type	= type;
			this.data	= new ArrayList<Object>();
		}
	}

	private	HashMap<String,DBDataItem>		define;
	private	int	line = 0;

	public DBDataSets(){
		this.define		= new HashMap<String,DBDataItem>();
		this.line = 0;
	}

	public void defItem( DBDataType type, String name ){
		this.define.put(name, new DBDataItem(type) );
	}

	public void setResultSet( ResultSet rs ) throws SQLException{
		line++;
		for ( String key: this.define.keySet() ){
			if ( this.define.get(key).type.equals( DBDataType.STRING )  ){
				this.define.get(key).data.add( rs.getString(key) );

			}else if ( this.define.get(key).type.equals( DBDataType.INTEGER )  ){
				this.define.get(key).data.add( rs.getInt(key) );

			}else if ( this.define.get(key).type.equals( DBDataType.LONG )  ){
				this.define.get(key).data.add( rs.getLong(key) );

			}else if ( this.define.get(key).type.equals( DBDataType.FLOAT )  ){
				this.define.get(key).data.add( rs.getFloat(key) );

			}else if ( this.define.get(key).type.equals( DBDataType.DOUBLE )  ){
				this.define.get(key).data.add( rs.getDouble(key) );

			}else{
				this.define.get(key).data.add( rs.getString(key) );
			}
		}
	}

	public int getLines(){
		return this.line;
	}

	public Object getData( String name, int line ){
		return this.define.get( name ).data.get( line );
	}

	public String getString( String name, int line ){
		return (String) this.define.get( name ).data.get( line );
	}

	public int getInteger( String name, int line ){
		return (int) this.define.get( name ).data.get( line );
	}

	public long getLong( String name, int line ){
		return (long) this.define.get( name ).data.get( line );
	}

	public double getDouble( String name, int line ){
		return (double) this.define.get( name ).data.get( line );
	}

	public float getFloat( String name, int line ){
		return (float) this.define.get( name ).data.get( line );
	}

	public DBDataType getType( String name ){
		return this.define.get( name ).type;
	}

}

