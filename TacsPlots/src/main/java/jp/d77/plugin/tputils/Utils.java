package jp.d77.plugin.tputils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.BrewingStand;
import org.bukkit.block.Chest;
import org.bukkit.block.Dispenser;
import org.bukkit.block.DoubleChest;
import org.bukkit.block.Dropper;
import org.bukkit.block.Furnace;
import org.bukkit.block.Hopper;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.util.BlockIterator;

public class Utils {
	// ---------------------------------------------------------------------------------------------------------------
	// Checker
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * 同じブロック位置か?
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean isEqualBlockLocation( Location a, Location b ){
		// 同一ワールドかを確認
		if ( ! a.getWorld().equals( b.getWorld() ) ) return false;
		if ( a.getBlockX() != b.getBlockX() )  return false;
		if ( a.getBlockY() != b.getBlockY() )  return false;
		if ( a.getBlockZ() != b.getBlockZ() )  return false;
		return true;
	}

	/**
	 * コマンド発行がコンソールか
	 * @param sender
	 * @return true - Console , false - Other
	 */
	public static boolean isConsole( CommandSender sender ){
		if ((sender instanceof ConsoleCommandSender)) {
			return true;
		}else{
			return false;
		}
	}

	/**
	 * コマンド発行がプレイヤーか
	 * @param sender
	 * @return true - Player , false - Other
	 */
	public static boolean isPlayer( CommandSender sender ){
		if ((sender instanceof Player)) {
			return true;
		}else{
			return false;
		}
	}

	public static boolean isNumber(String val) {
		try {
			Integer.parseInt(val);
			return true;
		} catch (NumberFormatException nfex) {
			return false;
		}
	}

	/**
	 * String null or blank check
	 * @param s
	 * @return
	 */
	public static boolean isBlank( String s ){
		if ( s == null ) return true;
		if ( s.equals("") ) return true;
		return false;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Getter
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 指定した名前のプレイヤーが居るか
	 * @param name
	 * @return
	 */
	public static Player getExistPlayer(String name) {
		for ( Player player : Bukkit.getOnlinePlayers() ) {
			if ( player.getName().equalsIgnoreCase(name) ) {
				return player;
			}
		}
		for ( OfflinePlayer player : Bukkit.getOfflinePlayers() ) {
			if ( player.getName().equalsIgnoreCase(name) ) {
				return player.getPlayer();
			}
		}
		return null;
	}

	/**
	 * 指定した名前のプレイヤーがオンラインか
	 * @param name
	 * @return
	 */
	public static Player getOnlinePlayer(String name) {
		for ( Player player : Bukkit.getOnlinePlayers() ) {
			if ( player.getName().equalsIgnoreCase(name) ) {
				return player;
			}
		}
		return null;
	}
	/**
	 * 指定したUUIDのプレイヤーが居るか
	 * @param name
	 * @return
	 */
	public static Player getExistUUID ( UUID uuid ) {
		for ( Player player : Bukkit.getOnlinePlayers() ) {
			if ( player.getUniqueId().equals( uuid ) ) {
				return player;
			}
		}
		for ( OfflinePlayer player : Bukkit.getOfflinePlayers() ) {
			if ( player.getUniqueId().equals( uuid ) ) {
				return player.getPlayer();
			}
		}
		return null;
	}

	/**
	 * 乱数を返す
	 * @param max
	 * @return 1～max値
	 */
	public static int getRandam( int max ){
		return (int)(Math.random() * max) + 1;	// 1～max
	}

	/**
	 * 視線の方角を取得<br>
	 * East=0 South=90 West=180 North=270
	 * @param ref
	 * @param target
	 * @return
	 */
	public static double getDirection( Location ref, Location target ){
		int sX,sZ;
		double r;

		sX = target.getBlockX() - ref.getBlockX();
		sZ = target.getBlockZ() - ref.getBlockZ();

		r = Math.atan2( sZ, sX ) * 180d / Math.PI;
		if ( r < 0 ) r= 360.0d + r;

		return r;
	}

	/**
	 * 視線の方向にあるブロックを取得
	 * @param p
	 * @param distance 距離
	 * @return Block or null
	 */
	public static Block getTargetBlock( Player p,int distance ) {
		BlockIterator it = new BlockIterator( p, distance);

		// 手前側から検証を行う。
		// Blockが取得できた時点でreturnして終了する。
		while ( it.hasNext() ) {

			Block block = it.next();

			if ( block.getType() != Material.AIR ) {
				// ブロックが見つかった
				return block;
			}
		}

		// 最後までブロックがみつからなかった
		return null;
	}

	/**
	 * 視線の方向にいるプレイヤーを取得
	 * @param p
	 * @param distance 距離
	 * @return Block or null
	 */
	public static Player getTargetPlayer(Player p,int distance ) {
		BlockIterator it = new BlockIterator( p.getPlayer(), distance);

		// 手前側から検証を行う。
		// Blockが取得できた時点でreturnして終了する。
		while ( it.hasNext() ) {
			Block block = it.next();
			if ( block.getType() == Material.AIR ) {
				for ( Player op : Bukkit.getOnlinePlayers() ){
					if ( op.getName().equals( p.getName() ) ) continue;
					if ( block.getLocation().getWorld().equals( op.getLocation().getWorld() ) ){
						// 同一ワールド
						if ( block.getLocation().getBlockX() == op.getLocation().getBlockX() && block.getLocation().getBlockZ() == op.getLocation().getBlockZ() ){
							if ( block.getLocation().getBlockY() - 1 == op.getLocation().getBlockY() ){
							// *見つめてる*
								return op;
							}
						}
					}
				}
			}
		}

		// 最後までブロックがみつからなかった
		return null;
	}

	/**
	 * 視線の方向にいるエンティティを取得
	 * @param p
	 * @param distance 距離
	 * @return Block or null
	 */
	public static LivingEntity getTargetEntity(Player p,int distance ) {
		BlockIterator it = new BlockIterator( p.getPlayer(), distance);

		// 手前側から検証を行う。
		// Blockが取得できた時点でreturnして終了する。
		while ( it.hasNext() ) {
			Block block = it.next();
			if ( block.getType() != Material.AIR ) continue;
			try {
				for ( LivingEntity e : block.getWorld().getLivingEntities() ){
					if ( e == null ) continue;
					if ( e.getType().equals( EntityType.PLAYER ) ){
						if ( ((Player)e).getName().equals( p.getName() ) ) continue;
					}
					if ( e.getWorld() == null ) continue;
					if ( block.getLocation().getWorld().equals( e.getWorld() ) ){
						// 同一ワールド
						if ( block.getLocation().getBlockX() == e.getLocation().getBlockX() && block.getLocation().getBlockZ() == e.getLocation().getBlockZ() ){
							if ( block.getLocation().getBlockY() - 1 == e.getLocation().getBlockY() ){
								return e;
							}
						}
					}
				}
			}catch ( NoSuchMethodError e ) {

			}

		}

		// 最後までブロックがみつからなかった
		return null;
	}

	/**
	 * Inventory情報からその場所を取得
	 * @param inv
	 * @return
	 */
	public static Block getBlockFromInventry( Inventory inv ){
		//core.msgs.debug( "TradeMain", "TradeChestOpen", "TradeChestOpen." );

		InventoryHolder invHolder = inv.getHolder();
		Location location = null;

		if (invHolder instanceof DoubleChest) {
			location = ((DoubleChest) invHolder).getLocation();

		} else if(invHolder instanceof Chest) {
			location = ((Chest) invHolder).getLocation();

		} else if(invHolder instanceof Hopper) {
			location = ((Hopper) invHolder).getLocation();

		} else if(invHolder instanceof Dropper) {
			location = ((Dropper) invHolder).getLocation();

		} else if(invHolder instanceof Dispenser) {
			location = ((Dispenser) invHolder).getLocation();

		} else if(invHolder instanceof Furnace) {
			location = ((Furnace) invHolder).getLocation();

		} else if(invHolder instanceof BrewingStand) {
			location = ((BrewingStand) invHolder).getLocation();

		} else {
			return null;
		}

		if(location == null) return null;

		return location.getBlock();
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Converter
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 数値を上限・下限に合わせる
	 * @param base
	 * @param max
	 * @param min
	 * @return
	 */
	public static int MinMax( int base, Integer max, Integer min ){
		int ret = base;
		if ( max != null ){
			if ( ret > max ) ret = max;
		}
		if ( max != null ){
			if ( ret > max ) ret = max;
		}
		if ( min != null ){
			if ( ret < min ) ret = min;
		}
		return ret;
	}

	/**
	 * Location -> String( world,X,Y,Z )
	 * @param loc
	 * @return
	 */
	public static String formatLocation( Location loc ){
		if ( loc == null ) return "null";
		return loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ();
	}

	public static String formatLocation( Chunk chunk ){
		if ( chunk == null ) return "null";
		return chunk.getWorld().getName() + " x=" + chunk.getX() + " z=" + chunk.getZ();
	}

	/**
	 * what time is it now!?
	 * @return ms
	 */
	public static long nowtime(){
		return new Date().getTime();
	}

	/**
	 * Long -> String (HH:mm:ss)
	 * @param d
	 * @return
	 */
	public static String formatDateTime( Long d ){
		return Utils.formatDateTime( d, "yyyy.MM.dd HH:mm:ss" );
	}

	/**
	 * Long -> String (HH:mm:ss)
	 * @param d
	 * @return
	 */
	public static String formatDate( Long d ){
		return Utils.formatDateTime( d, "yyyy.MM.dd" );
	}

	/**
	 * Long -> String (HH:mm:ss)
	 * @param d
	 * @return
	 */
	public static String formatTime( Long d ){
		return Utils.formatDateTime( d, "HH:mm:ss" );
	}

	/**
	 * Long -> String ( any format )
	 * @since 20160626
	 * @param d
	 * @return
	 */
	public static String formatDateTime( Long d, String format ){
		if ( d == null ) return "null";
		SimpleDateFormat sdf1 = new SimpleDateFormat(format);
		return sdf1.format( new Date( d ) );
	}

	/**
	 * Long -> String( null = "null" )
	 * @param d
	 * @return
	 */
	public static String LongToString( Long d ){
		if ( d == null ){
			return "null";
		}else{
			return d.toString();
		}
	}

	/**
	 * String -> Long
	 * @param d
	 * @return
	 */
	public static Long StringToLong( String d ){
		if ( d.equals("null") ){
			return null;
		}else if ( NumberUtils.isNumber( d ) ) {
			return Long.parseLong( d );
		}else{
			return null;
		}
	}

	/**
	 * Integer -> String( null = "null" )
	 * @param d
	 * @return
	 */
	public static String IntToString( Integer d ){
		if ( d == null ){
			return "null";
		}else{
			return d.toString();
		}
	}

	/**
	 * Integer -> String( null = "null" )
	 * @param d
	 * @return
	 */
	public static String BoolToString( Boolean d ){
		if ( d == null ){
			return "null";
		}else{
			return d.toString();
		}
	}

	/**
	 * String -> Int
	 * @param d
	 * @return
	 */
	public static Integer StringToInt( String d ){
		if ( d == null ) return null;
		if ( d.equals("null") ){
			return null;
		}else if ( NumberUtils.isNumber( d ) ) {
			return Integer.parseInt( d );
		}else{
			return null;
		}
	}

	public static String join( String Separator,String[] args ){
		String ret;
		if ( args.length > 0 ){
			ret = args[0];
		}else{
			return "";
		}
		for ( int i = 1 ; i < args.length ; i++ ){
			ret += Separator + args[i];
		}
		return ret;
	}

	public static String join( String Separator,Set<String> args ){
		return Utils.join( Separator, Utils.Set2Strings( args ) );
	}

	public static String join( String Separator,ArrayList<String> args ){
		return join( Separator, args.toArray( new String[0] ) );
	}

	public static String join( String Separator,List<String> args ){
		String ret;
		if ( args == null ){
			return null;
		}
		if ( args.size() > 0 ){
			ret = args.get(0);
		}else{
			return "";
		}
		for ( int i = 1 ; i < args.size() ; i++ ){
			if ( args.get(i) == null ) continue;
			ret += Separator + args.get(i);
		}
		return ret;
	}

	public static ArrayList<String> String2Set( String[] s ){
		return new ArrayList<String>( Arrays.asList(s) );
	}

	public static String[] Set2Strings( Set<String> a ){
		return a.toArray( new String[0] );
	}

	/**
	 * 指定座標からの相対位置を取得する
	 * @param loc
	 * @param X
	 * @param Y
	 * @param Z
	 * @return
	 */
	public static Location relativeLocation( Location loc, double X, double Y, double Z ){
		if ( loc == null ) return null;
		return new Location( loc.getWorld(), loc.getX() + X, loc.getY() + Y, loc.getZ() + Z );
	}

	public static String template( String msgs, String... args ){
		if ( args != null ){
			for ( int i = args.length - 1 ; i >= 0 ; i-- ){
				//core.msgs.debug("Messages", "template", msgs);
				if ( args[i] == null ){
					msgs = msgs.replace("$" + (i+1) , "null" );
				}else{
					msgs = msgs.replace("$" + (i+1) , args[i] );
				}
			}
		}
		return msgs;
	}

	public static void safeTeleport( Player p, Location to ){
		Location loc, loc_head;

		while ( to.getBlockY() < 255 ){
			loc = new Location( to.getWorld(), to.getBlockX(), to.getBlockY() ,to.getBlockZ() );
			loc_head = new Location( to.getWorld(), to.getBlockX(), to.getBlockY() + 1 ,to.getBlockZ() );
			if ( loc.getBlock().getType().isTransparent() == true && loc_head.getBlock().getType().isTransparent() == true ){
				break;
			}
			to.add( 0, 1, 0 );
		}
		p.teleport( to );
	}
}
