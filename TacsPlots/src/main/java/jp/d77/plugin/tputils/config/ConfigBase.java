package jp.d77.plugin.tputils.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Set;

import jp.d77.plugin.tputils.Utils;
import jp.d77.plugin.tputils.message.Message;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Tactica
 * @since 20160703
 *
 */
public class ConfigBase {
	//protected String				mFileName;		// ファイル名
	//protected String				mFullName;		// フルパス
	protected File					mFile;			// フルパス
	protected FileConfiguration		mConfig;		// ファイルオブジェクト
	protected boolean				mSave = true;	//
	protected boolean				mDebug = false;	// Debug

	protected JavaPlugin			mPlugin = null;	//
	protected Message				mMsg = null;	// メッセージ出力用

	// 自動バックアップ
	protected boolean				mAutoBackup = false;
	protected String				mBackupDir = "backup";
	protected File					mBackupPath;

	public ConfigBase( JavaPlugin plugin, String path, String FileName ) {
		this.mPlugin = plugin;
		if ( path == null ){
			this.setFileName( FileName );
		}else if ( path.equals( "" ) ) {
			this.setFileName( FileName );
		}else{
			this.setFileName( path + "/" + FileName );
		}
		this.mConfig = new YamlConfiguration();
		this.mConfig.options().copyDefaults(true);
	}

	public ConfigBase( JavaPlugin plugin, String FileName ) {
		this( plugin, plugin.getDataFolder().toString(), FileName );
	}

	// ---------------------------------------------------------------------------------------------------------------
	// 基本method
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 初期化処理
	 */
	public boolean init() {
		if ( this.mDebug ) this.debug("init","Loading: " + this.mFile.toString() );

		if ( ! this.load_resource( this.mFile.getName() ) ){
			return false;
		}
		if ( ! this.load_file( this.mFile.toString() ) ){
			return false;
		}
		this.setDefaults();
		this.save_file( this.mFile.toString() );

		if ( this.mDebug ) debug("init","Loaded: " + this.mFile.toString() );
		return true;
	}

	/**
	 * 破棄
	 */
	public void destroy() {
		this.clear();
		this.debug( "destroy","config:" + this.mFile.toString() );
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			this.destroy();
		}
	}

	public void setMessageClass( Message msg ){
		this.mMsg = msg;
	}

	public void setDebug( boolean debug ){
		this.mDebug = debug;
	}

	protected void debug( String method, String msg ){
		if ( ! this.mDebug ) return;

		if ( this.mMsg == null ){
			this.mPlugin.getLogger().info( "[DEBUG][" + this.getClass().getSimpleName() + "][" + method + "] " + msg );
		}else{
			this.mMsg.debug( this.getClass().getSimpleName(), method, msg );
		}
	}
	protected void warning( String msg ){
		if ( this.mMsg == null ){
			this.mPlugin.getLogger().warning( msg );
		}else{
			this.mMsg.warning( msg );
		}
	}
	protected void error( String msg ){
		if ( this.mMsg == null ){
			this.mPlugin.getLogger().info( "[ERROR] " + msg );
		}else{
			this.mMsg.error( msg );
		}
	}
	protected void info( String msg ){
		if ( this.mMsg == null ){
			this.mPlugin.getLogger().info( msg );
		}else{
			this.mMsg.info( msg );
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Load
	// ---------------------------------------------------------------------------------------------------------------

	public void save(){
		this.save_file( this.mFile.toString() );
	}

	public void load(){
		this.load_file( this.mFile.toString() );
	}

	/**
	 * 設定をリソースファイルから読み込み
	 */
	//@SuppressWarnings("deprecation")
	public boolean load_resource(String FileName){
		this.debug("load_resource","resource file: " + FileName);
		try {

			InputStream is = this.mPlugin.getResource(FileName);
			if ( is == null ) {
				this.debug( "load_resource","InputStream is null" );
				return true;
			}
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader( isr );
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n" );
			}
			mConfig.loadFromString( sb.toString() );

		} catch (IOException | InvalidConfigurationException e1) {
			this.warning( "Resource load error:" + FileName );
			e1.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 設定をファイルから読み込み
	 */
	public boolean load_file(String FileName){
		if ( this.mDebug ) debug("load_file","File=" + FileName);
		try {
			this.mConfig.load(FileName);
		} catch (java.io.FileNotFoundException e) {
			this.warning("File not found. set default value:" + FileName);
			return true;
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Save
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * 設定をファイルへ保存
	 */
	public boolean save_file(String FileName){
		if ( ! this.mSave ) return true;

		this.execBackup();

		if ( this.mDebug ) debug("save_file","File :" + FileName);
		try {
			mConfig.save(FileName);
		} catch (IOException e) {
			this.warning("Error occurred when saving files:" + FileName);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public void importConfig(String configString){
		try {
			mConfig.loadFromString(configString);
		} catch (InvalidConfigurationException e) {
			this.error("Default config load error:" + this.mFile.toString() );
			e.printStackTrace();
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Utils
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * データクリア
	 */
	public void clear() {
		if ( this.mDebug ) debug( "clear","Clear :" + this.mFile.toString() );
		mConfig = null;
		mConfig = new YamlConfiguration();
		try {
			mConfig.loadFromString("");
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * ファイル名を格納
	 * @param FileName
	 */
	public void setFileName( String FileName ){
		this.mFile = new File( FileName );
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Setter
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * Set Value
	 * @param path
	 * @param value
	 */
	public void set ( String path, Object value ){
		mConfig.set(path, value);
	}

	/**
	 * Set Values
	 * @param path
	 * @param value
	 */
	public void sets ( String path, Object... value ){
		mConfig.set( path, Arrays.asList(value) );
	}

	/**
	 * Set Values
	 * @param path
	 * @param value
	 */
	public void setList ( String path, Object[] value ){
		mConfig.set( path, value );
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Getter
	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * Get Value(String)
	 * @param path
	 * @return
	 */
	public String getString( String path, String... def ){
		if ( def != null && def.length > 0 ){
			return mConfig.getString( path, def[0] );
		}else{
			return mConfig.getString( path );
		}
	}

	/**
	 * Get Value(String)
	 * @param path
	 * @return
	 */
	public String getStringJoin( String path ){
		String s;

		if ( mConfig.isList( path ) ){
			s = this.join( "\n", mConfig.getList(path).toArray( new String[0] ) );
		}else{
			s = mConfig.getString( path );
		}
		//if ( s != null ) s = s.trim();
		return s;
	}

	public String join( String Separator,String[] args ){
		String ret;
		if ( args.length > 0 ){
			ret = args[0];
		}else{
			return "";
		}
		for ( int i = 1 ; i < args.length ; i++ ){
			ret += Separator + args[i];
		}
		return ret;
	}

	/**
	 * Get Value(int)
	 * @param path
	 * @return
	 */
	public Integer getInteger( String path, Integer... def ){
		if ( def != null && def.length > 0 ){
			return mConfig.getInt( path, def[0] );
		}else{
			return mConfig.getInt( path );
		}
	}

	/**
	 * Get Value(long)
	 * @param path
	 * @return
	 */
	public Long getLong( String path, Long... def ){
		if ( def != null && def.length > 0 ){
			return mConfig.getLong( path, def[0] );
		}else{
			return mConfig.getLong( path );
		}
	}

	/**
	 * Get Value(double)
	 * @param path
	 * @return
	 */
	public Double getDouble( String path, Double... def ){
		if ( def != null && def.length > 0 ){
			return mConfig.getDouble( path, def[0] );
		}else{
			return mConfig.getDouble( path );
		}
	}

	/**
	 * Get Value(boolean)
	 * @param path
	 * @return
	 */
	public Boolean getBoolean( String path, Boolean... def ){
		if ( def != null && def.length > 0 ){
			return mConfig.getBoolean(path, def[0] );
		}else{
			return mConfig.getBoolean(path);
		}
	}

	/**
	 * Get Value List(String)
	 * @param path
	 * @return
	 */
	public String[] getStringList( String path, String... def ){
		return mConfig.getStringList(path).toArray( new String[0] );
	}

	/**
	 * 指定したパス階層のデータを取得する.
	 * @param path
	 * @return
	 */
	public String[] getKeyList( String path ){
		Set<String> st = mConfig.getConfigurationSection(path).getKeys(false);
		String[] lst = (String[])st.toArray(new String[0]);
		return lst;
	}

	/**
	 * 指定したデータが設定されているパスを取得する(複数の場合は最初の一つだけ)
	 * @param value
	 * @return path
	 */
	public String getPath( String key, String value ){
		//Set<String> st = mConfig.getRoot().getKeys(true);
		String[] arr = getKeyList( key );

		for ( String s : arr ){
			//core.msgs.debug("ConfigBase", "getPath", s);
			if ( s == null ) continue;
			if ( getString( key + "." + s ) == null ) continue;
			if ( getString( key + "." + s ).equalsIgnoreCase( value ) ){
				return key + "." + s;
			}
		}
		return null;
	}

	public FileConfiguration data(){
		return mConfig;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Checker
	// ---------------------------------------------------------------------------------------------------------------
	public boolean isDefined( String path ){
		if ( path == null ) return false;
		return mConfig.contains( path );
	}
	public boolean isBlank( String path ){
		if ( path == null ) return true;
		if ( ! mConfig.contains( path ) ) return true;

		String s;
		s = mConfig.getString( path );
		if ( s != null ) s = s.trim();
		if ( s.length() <= 0 ) return true;

		return false;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// set Default
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * デフォルト値を設定
	 */
	public void setDefaults(){

	}

	public void setDefaultStrings(String path,String... default_value){
		if ( !mConfig.contains(path) ){
			// 未定義
			sets( path, (Object[])default_value );
			this.info( "&bconfig default set (Strings)&r" + path + "=" + default_value );
			return;
		}

		if ( !mConfig.isList(path) ){
			sets( path, (Object[])default_value );
			this.info( "&bconfig default set (Strings)&r" + path + "=" + default_value );
			return;
		}

		if ( mConfig.getString(path) == null ){
			sets( path, (Object[])default_value );
			this.info( "&bconfig default set (Strings)&r" + path + "=" + default_value );
			return;
		}

		if ( mConfig.getString(path).isEmpty() ){
			sets( path, (Object[])default_value );
			this.info( "&bconfig default set (Strings)&r" + path + "=" + default_value );
			return;
		}
	}

	/**
	 * 設定値チェック(String)
	 * @param path
	 * @param default_value
	 */
	public void setDefault(String path,String default_value){
		boolean change = false;

		if ( !this.isDefined(path) )		change = true;	// 未定義
		if ( !this.mConfig.isString(path) )		change = true;	// 型違い
		if ( this.getString(path) == null )	change = true;	// null

		if ( change ){
			this.set( path , default_value );
			this.info( "&bconfig default set (String)&r" + path + "=" + this.getString(path) );
			return;
		}
	}

	/**
	 * 設定値チェック(int)
	 * @param path
	 * @param default_value
	 */
	public void setDefault(String path,int default_value){
		boolean change = false;

		if ( !this.isDefined(path) )			change = true;	// 未定義
		if ( !mConfig.isInt(path) )				change = true;	// 型違い
		if ( this.getInteger(path) == null )	change = true;	// null

		if ( change ){
			set( path , default_value );
			this.info( "&bconfig default set (int)&r" + path + "=" + default_value );
			return;
		}
	}

	/**
	 * 設定値チェック(double)
	 * @param path
	 * @param default_value
	 */
	public void setDefault(String path,double default_value){
		boolean change = false;

		if ( !this.isDefined(path) )		change = true;	// 未定義
		if ( !mConfig.isDouble(path) )		change = true;	// 型違い
		if ( this.getDouble(path) == null )	change = true;	// null

		if ( change ){
			set( path , default_value );
			this.info( "&bconfig default set (double)&r" + path + "=" + default_value );
			return;
		}
	}

	/**
	 * 設定値チェック(Bool)
	 * @param path
	 * @param default_value
	 */
	public void setDefault(String path,boolean default_value){
		boolean change = false;

		if ( !this.isDefined(path) )			change = true;	// 未定義
		if ( !mConfig.isBoolean(path) )			change = true;	// 型違い
		if ( this.getBoolean(path) == null )	change = true;	// null

		if ( change ){
			set( path , default_value );
			this.info( "&bconfig default set (boolean)&r" + path + "=" + default_value );
			return;
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// 自動バックアップ
	// ---------------------------------------------------------------------------------------------------------------
	/**
	 * 自動バックアップの有効/無効
	 * @param enable true=有効
	 * @since 20160626
	 */
	public void setAutoBackup( boolean enable ){
		this.mAutoBackup = enable;
	}


	/**
	 * バックアップ実行
	 * @since 20160702
	 */
	public void execBackup(){
		// 日ごとのバックアップ
		this.execBackup( "d","dd" );

		// 1時間毎のバックアップ
		this.execBackup( "h","HH" );
	}

	/**
	 * バックアップ実行
	 * @since 20160702
	 */
	public void execBackup( String fname, String formatDate ){
		if ( ! this.mAutoBackup ) return;
		File bkFile;

		this.mBackupPath = new File( this.mFile.getParent(), this.mBackupDir );
		if ( this.mDebug ) this.debug("init","Backup path= " + this.mBackupPath.toString() );

		if ( ! this.mBackupPath.exists() ) this.mBackupPath.mkdir();

		bkFile = new File( this.mBackupPath, this.mFile.getName() + "." + fname + Utils.formatDateTime( Utils.nowtime() , formatDate ) );
		if ( this.mDebug ) this.debug("init","Backup: " + this.mFile.toString() + "->" + bkFile.toString() );

		this.copyFile( this.mFile, bkFile );
	}

	/**
	 * ファイルのコピー
	 * @param from
	 * @param dest
	 * @return
	 */
	public boolean copyFile( File from, File dest ){
		try {
			Files.copy( from.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING );
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
}
