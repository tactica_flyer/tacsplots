package jp.d77.plugin.tputils.kvs;

import jp.d77.plugin.tputils.config.ConfigBase;

import org.bukkit.plugin.java.JavaPlugin;

public class KVStoreYaml extends ConfigBase implements KVStoreImpl {
	protected String table;

	public KVStoreYaml( JavaPlugin plugin, String file, String table ){
		super( plugin, file );
		this.table = table;
	}

	@Override
	public void save(){
		super.save();
	}

	@Override
	public void load(){
		super.load();
	}

	@Override
	public void clear(){
		super.clear();
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Set
	// ---------------------------------------------------------------------------------------------------------------

	@Override
	public void setTable(String table) {
		this.table = table;
	}

	@Override
	public void setString(String key, String value) {
		super.set( this.table + "." + key , value);
	}

	@Override
	public void setInteger(String key, Integer value) {
		super.set( this.table + "." + key , value);
	}

	@Override
	public void setLong(String key, Long value) {
		super.set( this.table + "." + key , value);
	}

	@Override
	public void setFloat(String key, Float value) {
		super.set( this.table + "." + key , value);
	}

	@Override
	public void setDouble(String key, Double value) {
		super.set( this.table + "." + key , value);
	}

	@Override
	public void setBoolean(String key, Boolean value) {
		super.set( this.table + "." + key , value);
	}

	@Override
	public void setMode( KVSModeType mode, Boolean data ){
		if ( mode.equals( KVSModeType.AUTO_BACKUP ) ){
			super.setAutoBackup( data );
		}
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Get
	// ---------------------------------------------------------------------------------------------------------------
	@Override
	public String getString( String key ) {
		if ( ! super.isDefined( this.table + "." + key ) ) return null;
		return super.getString( this.table + "." + key );
	}

	@Override
	public Integer getInteger( String key ) {
		if ( ! super.isDefined( this.table + "." + key ) ) return null;
		return super.getInteger( this.table + "." + key );
	}

	@Override
	public Long getLong( String key ) {
		if ( ! super.isDefined( this.table + "." + key ) ) return null;
		return super.getLong( this.table + "." + key );
	}

	@Override
	public Double getDouble( String key ){
		if ( ! super.isDefined( this.table + "." + key ) ) return null;
		return super.getDouble( this.table + "." + key );
	}

	@Override
	public Boolean getBoolean( String key ){
		if ( ! super.isDefined( this.table + "." + key ) ) return null;
		return super.getBoolean( this.table + "." + key );
	}

	@Override
	public String[] getKeys( String key ){
		if ( ! super.isDefined( this.table + "." + key ) ) return new String[0];
		return super.getKeyList( this.table + "." + key );
	}

}
