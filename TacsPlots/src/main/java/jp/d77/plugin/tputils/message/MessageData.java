package jp.d77.plugin.tputils.message;

import java.util.ArrayList;

import jp.d77.plugin.tputils.Utils;
import jp.d77.plugin.tputils.config.ConfigBase;
import jp.d77.plugin.tputils.message.Message.LOG_LEVEL;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class MessageData {
	private Player					mPlayer;
	private ConsoleCommandSender	mConsole;
	private LOG_LEVEL				mLogLevel;
	private String					mMsg;

	// Paging params
	private ArrayList<String>		mMsgList;
	private int						mPageLines = 5;
	private int						mNowPage;
	private int						mMaxPage;

	public MessageData( Player p, String Msg ){
		this.mPlayer = p;
		this.mMsg = Msg;
	}

	public MessageData( ConsoleCommandSender c, LOG_LEVEL LogLevel, String Msg ){
		this.mConsole = c;
		this.mLogLevel = LogLevel;
		this.mMsg = Msg;
	}

	public void sendMessage( Message msg ){
		if ( mPlayer != null ){
			mPlayer.sendMessage( this.mMsg );
		}else if ( mConsole != null ){
			msg.log( this.mLogLevel, this.mMsg );
		}
		return;
	}

	public void enablePaging(){
		mMsgList = new ArrayList<String>( Utils.String2Set( mMsg.split("\n") ) );
		mNowPage = 1;
		mMaxPage = (( mMsgList.size() + 1) / mPageLines ) + 1;
	}

	/**
	 * ページング処理/次のページへ
	 */
	public void nextPage(){
		setPage( mNowPage + 1 );
	}

	/**
	 * ページング処理/前のページへ
	 */
	public void previousPage(){
		setPage( mNowPage - 1 );
	}

	/**
	 * ページング処理/ページ指定
	 */
	public void setPage( int page ){
		mNowPage = page;

		if ( mNowPage < 1 ) mNowPage = 1;
		if ( mNowPage > mMaxPage ) mNowPage = mMaxPage;
	}

	/**
	 * 現在のページのメッセージを取得
	 * @return
	 */
	public void sendPageMessage( ConfigBase cfg ){
		int sline,eline;
		String msg = "";

		eline = (mNowPage * mPageLines) - 1;
		sline = eline - mPageLines + 1;

		if ( eline > (mMsgList.size()-1) ){
			eline = mMsgList.size();
		}

		for ( int i = sline; i < eline; i++ ){
			msg += mMsgList.get(i) + "\n";
		}
		if ( mNowPage != mMaxPage ){
			msg += cfg.getString( "MESSAGE.MAIN.PREFIX" ) + "- " + mNowPage + "/" + mMaxPage + " - "
				+ "(" + cfg.getString( "MESSAGE.PAGING.NEXT" ) + " or "
				+ cfg.getString( "MESSAGE.PAGING.PREVIOUS" ) + " or "
				+ cfg.getString( "MESSAGE.PAGING.NUMBER" ) + ")";
		}

		if ( ! msg.equals("") ) mPlayer.sendMessage( msg );
	}

	public String getMessage(){
		return this.mMsg;
	}

	public Player getPlayer(){
		return this.mPlayer;
	}

}
