package jp.d77.plugin.tputils.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBCore {
	protected String		mDbType = "sqlite";

	protected Connection	mDBConnect;
	protected Statement		mDBStatement;

	protected String		mConnectionStr = "";
	protected String		mDBClass = "org.sqlite.JDBC";

	private Boolean		mTransaction = false;

	protected boolean		mDebug = true;

	public DBCore(){
	}

	/**
	 * Initializer
	 */
	public boolean init(){
		return true;
	}

	/**
	 * Destractor
	 */
	public void destroy(){

	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	/**
	 * DBの種別を取得
	 * @return
	 */
	public String getDBType(){
		return mDbType;
	}

	/**
	 * DBとセッションを貼る
	 * @return
	 */
	public boolean dbopen( ){
		boolean ret = true;

		if ( this.mDBClass.length() == 0 ) return false;
		if ( this.mConnectionStr.length() == 0 ) return false;

		dbclose();

		try {
			Class.forName( this.mDBClass );
			mDBConnect = DriverManager.getConnection( this.mConnectionStr );
			mDBStatement = mDBConnect.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
			ret = false;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			ret = false;
		}

		return ret;
	}

	/**
	 * DBセッションを閉じる
	 */
	public void dbclose(){
		if ( mDBConnect == null ) return;
		try {
			if ( ! mDBConnect.isClosed() ) {
				commit();
				mDBStatement.close();
				mDBConnect.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void startTransaction(){
		try {
			mDBStatement.execute("begin transaction;");
			mTransaction = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void commit(){
		if ( ! this.mTransaction ) return;
		try {
			mTransaction = false;
			mDBStatement.execute("commit;");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void rollback(){
		if ( ! this.mTransaction ) return;
		try {
			mTransaction = false;
			mDBStatement.execute("rollback;");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 単純に応答を求めないクエリーの実行
	 * @param query
	 * @param enableMsg
	 * @return
	 */
	public boolean execQuery( String query ){
		boolean res = true;

		try {
			mDBStatement.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
			res = false;
		}

		return res;
	}

	public boolean getDBDatasSet( String query, DBDataSets ds ){
		boolean nullchk = false;

		if ( ! dbopen() ) return false;

		try {
			mDBStatement.execute(query);
			ResultSet rs = mDBStatement.executeQuery(query);
			while ( rs.next() ){
				ds.setResultSet(rs);
				nullchk = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		dbclose();

		if ( nullchk ){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * データベースに指定のテーブルが存在しているか確認します
	 * @return
	 */
	public boolean matchTable( String table ){
		boolean res = false;

		if ( ! dbopen() ) return false;
		try {
			// ステートメントオブジェクトを生成
			DatabaseMetaData dmd = mDBConnect.getMetaData();
			ResultSet rs = null;
			String types[] = { "TABLE" };
			rs = dmd.getTables(null, null,"%", types);
			try {
				while(rs.next()){
					if ( table.equalsIgnoreCase( rs.getString("TABLE_NAME") ) ){
						res = true;
						break;
					}
				}
			} finally {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		dbclose();
		return res;
	}


}
