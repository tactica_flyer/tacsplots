package jp.d77.plugin.tputils;

import jp.d77.plugin.tputils.message.Message;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class CmdData {
	private CommandSender	mSender;
	private Command			mCmd;
	private String			mLabel;
	private String[]		mArgs;

	private boolean			mYesNo		= false;
	private boolean			mConsole	= true;
	private boolean			mPlayer		= true;
	private String			mPermissions	= null;
	private int				mArgMinLen	= -1;
	private int				mArgMaxLen	= 99;

	/**
	 * コンストラクタ
	 * @param sender
	 * @param cmd
	 * @param label
	 * @param args
	 */
	public CmdData( CommandSender sender, Command cmd, String label, String[] args ){
		mSender = sender;
		mCmd = cmd;
		mLabel = label;
		mArgs = args;
	}
	public CmdData(){

	}

	// ---------------------------------------------------------------------------------------------------------------
	// Setter
	// ---------------------------------------------------------------------------------------------------------------
	public CommandSender getSender(){
		return mSender;
	}

	public Player getPlayer(){
		return (Player)mSender;
	}

	/**
	 * 発言プレイヤー名を取得
	 * @return 発言プレイヤー名。プレイヤーでなければnull
	 */
	public String getPlayerName(){
		if ( this.isConsole() ) return null;
		return ((Player)mSender).getName();
	}

	public boolean getYesNoFlag(){
		return this.mYesNo;
	}

	/**
	 * コマンド(スラッシュ直後)を取得
	 * @return
	 */
	public Command getCmd(){
		return mCmd;
	}

	/**
	 * コマンドの第一引数を取得
	 * @return
	 */
	public String getSubCmd(){
		if ( mArgs.length >= 1 ){
			return mArgs[0];
		}else{
			return null;
		}
	}

	/**
	 * get command alias name
	 * @return
	 */
	public String getLabel(){
		return mLabel;
	}

	/**
	 * get command arguments
	 * @return
	 */
	public String[] getArgs(){
		return mArgs;
	}

	public String getArgs( int i ){
		if ( mArgs.length < i+1 ) return null;
		return mArgs[i];
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Setter
	// ---------------------------------------------------------------------------------------------------------------
	public void setArgumentsLength( int min, int max ){
		this.mArgMinLen = min;
		this.mArgMaxLen = max;
	}

	public void setYesNoFlag( boolean yes_no ){
		this.mYesNo = yes_no;
	}

	public void setConsoleCommand( boolean console ){
		this.mConsole = console;
	}

	public void setPlayerCommand( boolean player ){
		this.mPlayer = player;
	}

	public void setPermission( String perm ){
		this.mPermissions = perm;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Checker
	// ---------------------------------------------------------------------------------------------------------------

	public void setDefault( Message msg ){
		msg.setDefault( "MESSAGE.COMMANDS.NOT_PLAYER", "このコマンドはプレイヤーから実行できません." );
		msg.setDefault( "MESSAGE.COMMANDS.NOT_CONCOLE", "このコマンドはコンソールから実行できません." );
		msg.setDefault( "MESSAGE.COMMANDS.FAIL", "コマンドを間違えています." );
		msg.setDefault( "MESSAGE.COMMANDS.FAIL_PERMISSION", "権限がありません." );
	}

	/**
	 * Basic Command Checker
	 * @return
	 */
	public boolean checkExec( Message msg ){
		String w;

		if ( ! this.mPlayer ){ // プレイヤーでの実行が許可されていない
			if ( isPlayer() ) {
				msg.addMessage( ((Player)mSender).getUniqueId(), "MESSAGE.COMMANDS.NOT_PLAYER" );
				return false;
			}
		}
		if ( ! this.mConsole ){ // コンソールでの実行が許可されていない
			if ( isConsole() ){
				msg.addMessage( ((Player)mSender).getUniqueId(), "MESSAGE.COMMANDS.NOT_CONCOLE" );
				return false;
			}
		}

		// オプション数チェック
		if ( this.getArgs().length < this.mArgMinLen || this.getArgs().length > this.mArgMaxLen ) {
			msg.addMessage( this.getSender() , "MESSAGE.COMMANDS.FAIL" );

			msg.debug( "CmdData", "checkExec", "args fail min=" + this.mArgMinLen + " max=" + this.mArgMaxLen + " now=" + this.getArgs().length);

			for ( int i = this.getArgs().length - 1; i >= 0; i-- ){
				w = "";
				for ( int j = 0; j <= i; j++ ){
					if ( ! w.equals("") ) w += "-";
					w += this.getArgs()[j].toUpperCase();
				}
				/*
				msg.debug("checkExec", "help check=" + w);
				if ( ! core.cfg.isBlank( "MESSAGE.COMMANDS." + w ) ){
					msg.addMessage( this.getSender(), "COMMANDS." + w );
					return false;
				}else{
					msg.debug("checkExec", "help check=COMMANDS." + w + " is Blank");
				}
				*/
			}
			return false;
		}

		// コンソール実行なら権限確認は飛ばす
		if ( isConsole() ) return true;

		// Permissionの指定が無いならOKと見なす
		if ( this.mPermissions == null ) return true;
		if ( this.mPermissions.equals("") ) return true;

		msg.debug( "CmdData", "checkExec", "check permn=" + this.mPermissions );

		if ( ! getPlayer().hasPermission( this.mPermissions ) ){
			msg.addMessage( ((Player)mSender).getUniqueId(), "MESSAGE.COMMANDS.FAIL_PERMISSION" );
			return false;
		}

		return true;
	}

	/**
	 * 途中までの入力を許可
	 * @param base
	 * @param cmd
	 * @return
	 */
	public boolean matchCommandString( String base, String cmd ){
		return matchCommandString( base, cmd, 0 );
	}

	/**
	 * 途中までの入力を許可(最小文字数制限付き)
	 * @param base
	 * @param cmd
	 * @return
	 */
	public boolean matchCommandString( String base, String cmd, int minLength ){
		if ( cmd.length() < minLength ) return false;
		if ( base.equalsIgnoreCase( cmd ) ) return true;
		if ( base.substring( 0, cmd.length() ).equalsIgnoreCase( cmd ) ) return true;
		return false;
	}

	/**
	 * コマンド発行がコンソールか
	 * @param sender
	 * @return true - Console , false - Other
	 */
	public boolean isConsole(){
		if ((mSender instanceof ConsoleCommandSender)) {
			return true;
		}else{
			return false;
		}
	}

	/**
	 * コマンド発行がプレイヤーか
	 * @param sender
	 * @return true - Player , false - Other
	 */
	public boolean isPlayer(){
		if ((mSender instanceof Player)) {
			return true;
		}else{
			return false;
		}
	}

}
