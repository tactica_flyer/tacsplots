package jp.d77.plugin.tputils.kvs;

import org.bukkit.plugin.java.JavaPlugin;



public class KeyValue implements KVStoreImpl{
	public enum DB_TYPE{
		MYSQL,YAML,SQLITE
	}

	private DB_TYPE	dbType = DB_TYPE.YAML;
	private String	dbName = "config.yml";
	private String	tableName = "";
	private KVStoreImpl	store;


	public KeyValue( DB_TYPE dbType, String dbname, String tablename ){
		this.dbType = dbType;
		this.dbName = dbname;
		this.tableName = tablename;
	}

	public boolean init( JavaPlugin plugin ){
		if ( this.dbType == DB_TYPE.YAML ){
			this.store = new KVStoreYaml( plugin, this.dbName, this.tableName );
		}else{
			this.store = new KVStoreYaml( plugin, this.dbName, this.tableName );
		}
		return true;
	}

	public boolean destroy(){
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			super.finalize();
		} finally {
			destroy();
		}
	}

	@Override
	public void save(){
		this.store.save();
	}

	@Override
	public void load(){
		this.store.load();
	}

	@Override
	public void clear(){
		this.store.clear();
	}
	// ---------------------------------------------------------------------------------------------------------------
	// Gets
	// ---------------------------------------------------------------------------------------------------------------
	@Override
	public Integer getInteger(String key) {
		return this.store.getInteger(key);
	}

	@Override
	public Long getLong(String key) {
		return this.store.getLong(key);
	}

	@Override
	public Double getDouble(String key) {
		return this.store.getDouble(key);
	}

	@Override
	public Boolean getBoolean(String key) {
		return this.store.getBoolean(key);
	}

	@Override
	public String[] getKeys( String key ){
		return this.store.getKeys(key);
	}

	@Override
	public String getString(String key) {
		return this.store.getString(key);
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Sets
	// ---------------------------------------------------------------------------------------------------------------

	@Override
	public void setDebug( boolean b ){
		this.store.setDebug( b );
	}

	@Override
	public void setTable(String table) {
		this.store.setTable(table);
	}

	@Override
	public void setString(String key, String value) {
		this.store.setString(key, value);
	}

	@Override
	public void setInteger(String key, Integer value) {
		this.store.setInteger(key, value);
	}

	@Override
	public void setLong(String key, Long value) {
		this.store.setLong(key, value);
	}

	@Override
	public void setFloat(String key, Float value) {
		this.store.setFloat(key, value);
	}

	@Override
	public void setDouble(String key, Double value) {
		this.store.setDouble(key, value);
	}

	@Override
	public void setBoolean(String key, Boolean value) {
		this.store.setBoolean(key, value);
	}

	@Override
	public void setMode( KVSModeType mode, Boolean data ){
		this.store.setMode(mode, data);
	}

}
